import countries from '../constants/countries'

export const HANDLE_COUNTRY_NAME_INPUT_CHANGE = 'HANDLE_COUNTRY_NAME_INPUT_CHANGE'
export const ADD_NEW_COUNTRY = 'ADD_NEW_COUNTRY'
export const DELETE_COUNTRY = 'DELETE_COUNTRY'
export const EDIT_COUNTRY = 'EDITING_COUNTRY'
export const CANCEL_EDIT_COUNTRY = 'CANCEL_EDIT_COUNTRY'
export const HANDLE_STATUS_INPUT_CHANGE = 'HANDLE_STATUS_INPUT_CHANGE'
export const FETCH_REQUEST_COUNTRIES_SUCCEEDED = 'FETCH_REQUEST_COUNTRIES_SUCCEEDED'
export const SAVE_NEW_COUNTRY = 'SAVE_NEW_COUNTRY'
export const UPDATE_COUNTRY = 'UPDATE_COUNTRY'

export function handleNameInputChange(event,id){
	return {
		type: HANDLE_COUNTRY_NAME_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}

export function updateCountry(country){
	return{
		type: UPDATE_COUNTRY,
		country: country
	}
}

export function saveNewCountry(country,id){
	return{
		type: SAVE_NEW_COUNTRY,
		country: country,
		new_country_id:id,
	}
}

export function handleStatusInputChange(event,id){
	return {
		type: HANDLE_STATUS_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}

export function addNewCountry(){
	return {
		type: ADD_NEW_COUNTRY,
	}
}

export function deleteCountry(id){
	return {
		type: DELETE_COUNTRY,
		id: id
	}
}

export function editCountry(id){
	return {
		type: EDIT_COUNTRY,
		id: id
	}
}

export function cancelEditCountry(text,id){
	return{
		type: CANCEL_EDIT_COUNTRY,
		text: text,
		id:id
	}
}

export function fetchRequestCountriesSucceeded(countries){
	return{ 
		type: FETCH_REQUEST_COUNTRIES_SUCCEEDED,
		countries: countries
	}
}

export function fetchCountries(){
	
	return function (dispatch,getState) {	
		dispatch(fetchRequestCountriesSucceeded(countries))
		/*return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"countries",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			//'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });

			if (response.status === 403)
				return response.json().then(err => { throw err; });
			
			},
			
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(fetchRequestCountriesSucceeded(json.countries))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar los paises')
		})*/
		
	}
}

export function fetchCreateCountry(country){
	let newCountry = {
			name : country.name,
			status: country.status      
	}
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/countries",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(newCountry)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(saveNewCountry(json.country,country.id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchDeleteCountry(id){
	return function (dispatch,getState) {
		dispatch(deleteCountry(id))
		/*const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"countries/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("País eliminado exitosamente")
				
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		})*/
		
	}
}

export function fetchUpdateCountry(country){
	let country_to_update = {
			name : country.name,
			status: country.status      
	}
	
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		dispatch(updateCountry(country))
		/*return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"countries/"+country.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(country_to_update)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateCountry(country))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la modificación')
		})*/
		
	}
}
