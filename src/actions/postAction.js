import history from '../history'
import {detResponseStatusErr} from '../functions'

export const HANDLE_POST_INPUT_CHANGE = 'HANDLE_POST_INPUT_CHANGE'
export const SHOW_POST = 'SHOW_POST'
export const RESET_FETCH = 'RESET_FETCH'
export const IS_FETCHING = 'IS_FETCHING'
export const CANCEL_POST_POST = "CANCEL_POST_POST"
export const DELETE_POST_PICTURES = 'DELETE_POST_PICTURES'

export const FETCH_POSTS_SUCCEEDED = 'FETCH_POSTS_SUCCEEDED'
export const FETCH_SAVE_NEW_POST_SUCCEEDED = 'FETCH_SAVE_NEW_POST_SUCCEEDED'

export const FETCH_POST_FAILED = "FETCH_POST_FAILED"

export function fetchSaveNewPostSucceeded(post,fetch){
	return {
		type: FETCH_SAVE_NEW_POST_SUCCEEDED,
		post,
		fetch
	}
}

export function fetchPostFailed(message,fetch){
	return {
		type: FETCH_POST_FAILED,
		message,
		fetch
	}
}

export function fetchPostsSucceeded(posts){
	return{
		type: FETCH_POSTS_SUCCEEDED,
		posts
	}
}

export function resetFetchPost(){
    return{
        type: RESET_FETCH
    }
}

export function isFetching(fetch){
    return{
		type: IS_FETCHING,
		fetch
    }
}


export function showPost(post){
    return{
        type: SHOW_POST,
        post
    }
}

export function onPostHandleInputChange(event){
    return {
        type: HANDLE_POST_INPUT_CHANGE,
        event
    }
}

export function deletePostPictures(index){
    return {
        type: DELETE_POST_PICTURES,
        index
    }
}

export function cancelPostPost(){
    return {
        type: CANCEL_POST_POST
    }
}

export function downloadFile(fileName){
	const _FETCH = "downloadFile"
	return function (dispatch,getState) {
		dispatch(isFetching(_FETCH))
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_URL_SERVER+"/posts/upload/"+fileName+"/download",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
            }
            if (detResponseStatusErr(response.status))
				return response.json().then(err => { throw err; });
			},
			error => {
				dispatch(fetchPostFailed("No se pudo descargar el archivo",_FETCH));
				console.log('An error occurred.', error)
			}
		)
		.then(json => {
			if (json) {
                console.log("hola")
				
			}		
		}
		).catch((errors) => {
			dispatch(fetchPostFailed("No se pudo descargar el archivo",_FETCH));
			console.log('No se pudiero descargar el archivo',errors)
		})
		
	}
}

export function fetchPosts(path){
	console.log(path)
	const _FETCH = "fetchPosts"
	return function (dispatch,getState) {
		dispatch(isFetching(_FETCH))
        const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_URL_SERVER+path,{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
            }
            if (detResponseStatusErr(response.status))
				return response.json().then(err => { throw err; });
			},
			error => {
				dispatch(fetchPostFailed("No se pudieron cargar los post",_FETCH));
				console.log('An error occurred.', error)
			}
		)
		.then(json => {
			if (json) {
                if(json.code==200){
					dispatch(fetchPostsSucceeded(json.posts))
				}
				else {
					dispatch(fetchPostFailed("No se pudieron cargar los post",_FETCH));
				}
				
			}		
		}
		).catch((errors) => {
			dispatch(fetchPostFailed("No se pudieron cargar los post",_FETCH));
			console.log('No se pudieron cargar los posts',errors)
		})
		
	}
}

export function fetchSaveNewPost(post){
	const _FETCH = "fetchSaveNewPost"
	return function (dispatch,getState) {
		
    	const dataWithFile = new FormData();
		dataWithFile.append('title', JSON.stringify(post.title));
		dataWithFile.append('content', JSON.stringify(post.description));
    	//for (var i=0; i < post.pictures.length; i++) {
		dataWithFile.append('file', post.files[0])
    	//}
		const token = getState().main.globalData.token;
		dispatch(isFetching(_FETCH))	
		return fetch(process.env.REACT_APP_URL_SERVER+"/posts",{
				method: "POST",
				mode: "cors",
				headers: {
          		'Authorization': 'Bearer ' + token
				},
				body: dataWithFile
			})
  		.then(			
  			response => {
  			if (response.ok){
  				return response.json()
  			}	
  			if (detResponseStatusErr(response.status))
				return response.json().then(err => { throw err; });
    		},
			error => {
				dispatch(fetchPostFailed("No se pudo guardar el post",_FETCH));
				console.log('An error occurred.', error)
			}
			)
		.then(json => { 		
				if (json != null) {
					if(json.code && json.code == 200){
						dispatch(fetchSaveNewPostSucceeded(json.post,_FETCH))
						/*history.push("/detail-publication")
						window.location.reload(true)*/
					}
				}else{											
					dispatch(fetchPostFailed("No se pudo guardar el post",_FETCH));
          			console.log('No se pudo guardar el post ')
        		}			
		}
		).catch((errors) => {
		    console.log("errors:")
		    console.log(errors)
			console.log('Error: No se pudo guardar el post')
			dispatch(fetchPostFailed("No se pudo guardar el post",_FETCH));
      })
	}
}