import userTransform from '../transforms/userTransform'

export const HANDLE_EMAIL_INPUT_CHANGE = 'HANDLE_EMAIL_INPUT_CHANGE'
export const FETCH_REFERRALS_SUCCEEDED = 'FETCH_REFERRALS_SUCCEEDED'
export const SHOW_REFERRED = 'SHOW_REFERRED'
export const EDIT_REFERRED = 'EDIT_REFERRED'
export const CANCEL_EDIT_REFERRED = 'CANCEL_EDIT_REFERRED'
export const RESTORE_REFERRED_DATA = 'RESTORE_REFERRED_DATA'
export const RESTORE_REFERRED = 'RESTORE_REFERRED'
export const DELETE_REFERRED = 'DELETE_REFERRED'
export const HANDLE_REFERRED_INPUT_CHANGE = 'HANDLE_REFERRED_INPUT_CHANGE'
export const HANDLE_REFERRED_EMAIL_INPUT_CHANGE = 'HANDLE_REFERRED_EMAIL_INPUT_CHANGE'
export const HANDLE_REFERRED_PASSWORD_INPUT_CHANGE = 'HANDLE_REFERRED_PASSWORD_INPUT_CHANGE'
export const HANDLE_REFERRED_CONFIRM_PASSWORD_INPUT_CHANGE = 'HANDLE_REFERRED_CONFIRM_PASSWORD_INPUT_CHANGE'
export const HANDLE_REFERRED_REFERREDNAME_INPUT_CHANGE = 'HANDLE_REFERRED_REFERREDNAME_INPUT_CHANGE'
export const HANDLE_REFERRED_ROLE_INPUT_CHANGE = 'HANDLE_REFERRED_ROLE_INPUT_CHANGE'
export const FETCH_REQUEST_REFERRALS_SUCCEEDED = 'FETCH_REQUEST_REFERRALS_SUCCEEDED'
export const UPDATE_REFERRED = 'UPDATE_REFERRED'
export const SAVE_NEW_REFERRED= 'SAVE_NEW_REFERRED'
export const HANDLE_REFERRED_ROLE_CHANGE ='HANDLE_REFERRED_ROLE_CHANGE'
export const HANDLE_REFERRED_COUNTRY_CHANGE ='HANDLE_REFERRED_COUNTRY_CHANGE'
export const SHOW_PUBLIC_REFERRED_PROFILE_SUCCEEDED = 'SHOW_PUBLIC_REFERRED_PROFILE_SUCCEEDED'

export function showPublicReferredProfileSucceeded(referred){
	return{
		type: SHOW_PUBLIC_REFERRED_PROFILE_SUCCEEDED,
		referred
	}
}

export function handleReferredInputChange(event){
	return{
		type:HANDLE_REFERRED_INPUT_CHANGE,
		event
	}
}
export function handleReferredRoleChange(event){
	return{
		type:HANDLE_REFERRED_ROLE_CHANGE,
		event
	}
}
export function handleReferredCountryChange(event){
	return{
		type:HANDLE_REFERRED_COUNTRY_CHANGE,
		event
	}
}

export function fetchReferralsSucceeded(referrals){
	return{
		type: FETCH_REFERRALS_SUCCEEDED,
		referrals:referrals
	}
}

export function showReferred(referred){
	return{
		type: SHOW_REFERRED,
		referred
	}
}

export function editReferred(referred){
	return{
		type: EDIT_REFERRED,
		referred
	}
}

export function updateReferred(referred){
	return{
		type: UPDATE_REFERRED,
		referred
	}
}

export function saveNewReferred(referred,id){
	return{
		type: SAVE_NEW_REFERRED,
		referred: referred,
		new_referred_id: id
	}
}

export function restoreReferred(){
	return{
		type: RESTORE_REFERRED
	}
}

export function deleteReferred(id){
	return{
		type: DELETE_REFERRED,
		id
	}
}


export function restoreReferredData(){
	return{
		type:RESTORE_REFERRED_DATA
	}
}

export function fetchRequestReferralsSucceeded(referrals){
	return{ 
		type: FETCH_REQUEST_REFERRALS_SUCCEEDED,
		referrals: referrals
	}
}

export function fetchReferrals(){
	return function (dispatch,getState) {
		
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/users/referrals/myReferrals",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log('Se pudieron cargar los usuarios')
				dispatch(fetchRequestReferralsSucceeded(userTransform(json.userReferreds)))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			console.log(errors)
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar los usuarios')
		})
		
	}
}

export function fetchDeleteReferred(id){
	return function (dispatch,getState) {
		dispatch(deleteReferred(id))
		/*const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"referrals/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("Usuario eliminado exitosamente")
				dispatch(deleteReferred(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		})*/
		
	}
}

 
export function fetchCreateReferred(referred){
		
		
	let newReferred = {
			email : referred.email,      
			password : referred.password,
			passwordConfirm: referred.confirmPassword,
			//telephone: "555",
			country: referred.country_id,
			role: referred.role_id,			
			/*referredname : "123",
			name: "Moises",
			surname: "Cam",
			birthdate: "2019-10-12",*/
			//gender:1,
			
	}
	let new_referred = {id: Math.random(),
        referredname: 'nuevo',
        email: referred.email,
        status: 1,
		role_id: 1,
		country:{
			id: 3,
			name: "Argentina", 
			status: 1
		},
        role:{
            id: 1,
            name: "Administrador",
            description: "Usuario encargado del manejo y control de la apliación y de la data", 
            status: 1
        },
    }
		
		
	return function (dispatch,getState) {
		dispatch(saveNewReferred(new_referred,referred.id))
		//dispatch(saveNewReferred(json.referred,referred.id))
		/*const token = getState().main.globalData.token		
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"referrals",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(newReferred), // body data type must match "Content-Type" header
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {			
			if (json) {
			if(json.email){			
				dispatch(saveNewReferred(json.referred,referred.id))
								
			}
			else{
				//dispatch(requestRegisterNewReferredFailed(json))							
			}
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})*/
		
	}
}
export function fetchUpdateReferred(referred){
	/*let update_Referred = {
		email : referred.email,      
		password : "123456789",
		passwordConfirm: "123456789",
		telephone: "555",
		country: referred.referred_profile.country.id,
		role: referred.role.id,			
		referredname : "123",
		name: "Moises",
		surname: "Cam",
		birthdate: "2019-10-12",
		gender:1,
		
	}*/
	console.log(referred)
	return function (dispatch,getState) {
		dispatch(updateReferred(referred))
		/*const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"referrals/"+referred.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(update_Referred)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });

			if (response.status === 500)
				return response.json().then(err => { throw err; });

			if (response.status === 403)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateReferred(referred))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log(errors)
			console.log('No se pudo completar la modificación')
		})
		*/
	}
}