import userApp from '../constants/userApp'
import userTransform from '../transforms/userTransform'
import emailTransform from '../transforms/emailTransform'
import {detResponseStatusErr} from '../functions'

export const HANDLE_MAIN_USER_INPUT_CHANGE = 'HANDLE_MAIN_USER_INPUT_CHANGE'
export const HANDLE_USER_PROFILE_INPUT_CHANGE = 'HANDLE_USER_PROFILE_INPUT_CHANGE'
export const HANDLE_AUTH_STATE_CHANGE = 'HANDLE_AUTH_STATE_CHANGE'
export const AUTH_USER_SUCCEEDED = 'AUTH_USER_SUCCEEDED'
export const REQUEST_FAILED = "REQUEST_FAILED"
export const RESET_USER_DATA = 'RESET_USER_DATA'
export const CANCEL_EDIT_PROFILE = 'CANCEL_EDIT_PROFILE'
export const SAVE_USER_PROFILE = 'SAVE_USER_PROFILE'
export const UPDATE_USER_PROFILE = 'UPDATE_USER_PROFILE'
export const RESET_MAIN_FETCH = 'RESET_MAIN_FETCH'
export const HANDLE_INPUT_FILE_CHANGE = 'HANDLE_INPUT_FILE_CHANGE'
export const HANDLE_MAIN_USER_MULTI_SELECT = "HANDLE_MAIN_USER_MULTI_SELECT"
export const DELETE_ORDER_PICTURES = 'DELETE_ORDER_PICTURES'

export const FETCH_EMAIL_SINGLE_SUCCEEDED = "FETCH_EMAIL_SINGLE_SUCCEEDED"
export const REQUEST_FINISH_PURCHASE_SUCCEDED = 'REQUEST_FINISH_PURCHASE_SUCCEDED'
export const REQUEST_FINISH_PURCHASE_FAILED = 'REQUEST_FINISH_PURCHASE_FAILED'
export const HANDLE_CHANGE_PASSWORD_INPUT_CHANGE = 'HANDLE_CHANGE_PASSWORD_INPUT_CHANGE'
export const RESET_CHANGE_PASSWORD = 'RESET_CHANGE_PASSWORD'

export const FETCH_GET_EMAIL_SUCCEEDED = 'FETCH_GET_EMAIL_SUCCEEDED'
export const REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED = 'REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED'
export const REQUEST_CHANGE_PROFILE_IMAGE_FAILED = 'REQUEST_CHANGE_PROFILE_IMAGE_FAILED'

export const REGISTER_USER_SUCCEEDED = 'REGISTER_USER_SUCCEEDED'

export const REQUEST_IN_PROCESS = "REQUEST_IN_PROCESS"

export const SET_EMAIL = 'SET_EMAIL'

export function setEmail(email){
	return{
		type: SET_EMAIL,
		email
	}
}
export function requestInProcess(fetch){
	return {
		type: REQUEST_IN_PROCESS,
		fetch
	}
}

export function resetChancePassword(){
	return{
		type: RESET_CHANGE_PASSWORD
	}
}

export function handleUserMultiSelect(array,state){
	return{
		type: HANDLE_MAIN_USER_MULTI_SELECT,
		array,
		state
	}
}

export function handleChangePasswordInputChange(event){
	return{
		type: HANDLE_CHANGE_PASSWORD_INPUT_CHANGE,
		event
	}
}

function requestFinishPurchaseSucceded(purchase){
	return {
		type: REQUEST_FINISH_PURCHASE_SUCCEDED,
		purchase
	}
}

function requestFinishPurchaseFailed(){
	return {
		type: REQUEST_FINISH_PURCHASE_FAILED
	}
}

export function deleteOrderImage(index){
	return {
		type: DELETE_ORDER_PICTURES,
		index
	}
}

export function handleInputFileChange(event){
	return {
		type: HANDLE_INPUT_FILE_CHANGE,
		event
	}
}


export function resetFetch(){
	return{
		type: RESET_MAIN_FETCH
	}
}

export function handleUserInputChange(event){
	return{
		type:HANDLE_MAIN_USER_INPUT_CHANGE,
		event
	}
}

export function handleUserProfileInputChange(event){
	return{
		type:HANDLE_USER_PROFILE_INPUT_CHANGE,
		event
	}
}

export function handleAuthStateChange(text){
	return{
		type: HANDLE_AUTH_STATE_CHANGE,
		text
	}
}

export function authUserSucceeded(user,token){
	return{
		type: AUTH_USER_SUCCEEDED,
		user: user,
		token
	}
}

function fetchGetEmailSucceeded(messages)
{
	return{
		type: FETCH_GET_EMAIL_SUCCEEDED,
		emails:messages
	}
}

function registerUserSucceeded(message){
	return{
		type: REGISTER_USER_SUCCEEDED,
		message
	}
}

function fetchEmailSingleSucceeded(content){
	return{
		type: FETCH_EMAIL_SINGLE_SUCCEEDED,
		content
	}
}

export function requestFailed(message,fetch){
	return{
		type: REQUEST_FAILED,
		message,
		fetch:fetch?fetch:""
	}
}

export function resetUserData(){
	return{
		type: RESET_USER_DATA
	}
}

export function cancelEditProfile(){
	return{
		type: CANCEL_EDIT_PROFILE
	}
}

export function saveUserProfile(){
	return{
		type: SAVE_USER_PROFILE
	}
}

export function updateUserProfile(user){
	return{
		type: UPDATE_USER_PROFILE,
		user
	}
}

function requestChangeProfileImageSucceded(image){
	return {
		type: REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED,
		image
	}
}

function requestChangeProfileImageFailed(){
	return {
		type: REQUEST_CHANGE_PROFILE_IMAGE_FAILED
	}
}

export function fetchSaveUserProfile(user){
	const _FETCH = "fetchSaveUserProfile"
	let update_userProfile = {	
		//email: user.email,	
		//telephone: user.phone,
		//country: user.country.id,	
		name: user.name,
		lastName: user.lastname,
		//birthdate: user.birthdate,
		//gender: user.sex,
		phone: user.telephone,
		socialReason: user.social_reason
		
	}
	return function (dispatch,getState) {
		dispatch(requestInProcess(_FETCH))
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_URL_SERVER+"/users/"+user.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(update_userProfile)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 400)
			return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {			
				if(json.statusCode==200){
					dispatch(updateUserProfile(user))
				}
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			dispatch(requestFailed("No se pudo completar la modificación",_FETCH))
			console.log('No se pudo completar la modificación')
		})
		
	}
}


	export function fetchCreateUser(user){
		const _FETCH="fetchCreateUser"
		let pathologiesId
		 
		if(user.user_type==1){
			pathologiesId = user.pathologies.map(pathology =>{ return pathology.id})
		}
		if(user.webSite==false){
			user.webSite="www.notengositioweb.com"
		}
		
		let newUser = {
			email: user.email,
			password: user.password,
			confirmPassword: user.passwordConfirm,
			name: user.name,
			lastName: user.lastname,
			phone: user.phone,
			userType: parseInt(user.user_type),
			birthdate: user.birthdate,
			instructionDegree: user.instruction,
			jobType: user.job_type,
			jobName: user.job,
			currentStudy: user.current_study,
			country: user.country,
			pathologies:  pathologiesId,
			socialReason: user.socialReason,
			clasificationId: parseInt(user.classification_id),
			sex: user.sex,
			facebook: user.facebook,
			twitter: user.twitter,
			instagram: user.instagram,
			referralCode: user.referralCode,
			webSite: user.webSite,
			address: user.address	
	
		}

		return dispatch => {
			dispatch(requestInProcess(_FETCH))
			return fetch(process.env.REACT_APP_URL_SERVER+"/auth/register",{
			method: "POST", // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			//credentials: "with-credentials", // include, same-origin, *omit
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				// "Content-Type": "application/x-www-form-urlencoded",
				//"X-Email": email,
				//"X-Token": token
			},
			body: JSON.stringify(newUser)
			})
			.then(
				response => {
					if (response.ok) {
						return response.json()
					}
					if (detResponseStatusErr(response.status))
						return response.json().then(err => { throw err; });
					},
					error => {
						dispatch(requestFailed("No se pudo registrar el usuario"));
						console.log('An error occurred.', error)
					}
			)
			.then(json => {
				if (json) {
					dispatch(registerUserSucceeded("Usuario registrado exitosamente"))
				}	
				else{
					dispatch(requestFailed("No se pudo realizar la autenticación"))
				}	
			}
			).catch((errors) => {
				//dispatch(requestAddCountryFailed(errors))
				console.log(errors)
				dispatch(requestFailed(errors.message))
				//dispatch(addErrorMessages(message, "danger"))
				console.log('No se pudo completar el registro')
			})
			
		}
	}

export function fetchAuthUser(user){
	let userJson = {
		email: user.email,
		password: user.password
	}
	
	return function (dispatch){
		dispatch(requestInProcess())
		return fetch(process.env.REACT_APP_URL_SERVER+"/auth/login",{
			method: "POST", // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			//credentials: "with-credentials", // include, same-origin, *omit
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				// "Content-Type": "application/x-www-form-urlencoded",
				//"X-Email": email,
				//"X-Token": token
			},
			body: JSON.stringify(userJson)
		})
		.then(
			response => {
				console.log(response)
			if (response.ok) {
				return response.json()
			}
			detResponseStatusErr(response.status)
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(authUserSucceeded(userTransform(json.decoded),json.accessToken))
			}	
			else{
				dispatch(requestFailed("No se pudo realizar la autenticación"))
			}	
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))

			console.log(errors)
			dispatch(requestFailed(errors.message))
			console.log('No se pudo completar el inicio de sesion')
		})
	}
}

export function fetchChangePassword(change_password){
	return function (dispatch,getState) {	
		let json = {
			password: change_password.current_password,
			newPassword: change_password.new_password,
			passwordConfirm: change_password.new_password_confirm,
			user_id: getState().main.userData.id,
		  }
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_URL_SERVER+"/users/change_password",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					if(json.status==200){
						console.log('Contraña cambiada con éxito ')
					} else{
						console.log('No se pudo cambiar la contraseña')
					}
				}else{										
                    console.log('No se pudo cambiar la contraseña')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                console.log('Error: No se pudo cambiar la contraseña')
		      })
	}
}



export function fetchUploadProfileImage(event){
	
	return function(dispatch, getState){
		const token = getState().main.globalData.token;
		const user = getState().main.userData;
		let allowed_extensions = ['jpg', 'jpeg', 'png', 'gif'];
		let file = event.target.files[0];
		let short_name = file.name.split('.')
    let extension = short_name[short_name.length -1]

		if(file){
			if(allowed_extensions.indexOf(extension) >= 0){
				const dataWithFile = new FormData()
				dataWithFile.append('image', file) 

				return fetch(process.env.REACT_APP_URL_SERVER+"/users/"+user.id+"/uploadProfileImage",{
		        method: "POST",
		        mode: "cors",
		        headers: {
							'Authorization': 'Bearer ' + token
		        },
		        body: dataWithFile
		    })
	      .then(
	        response => {
	    		if (response.ok){
						return response.json()
					}	
					if (response.status === 422)
						return response.json().then(err => { throw err; });
					},

	        error => console.log('An error occurred.', error)
	      )
	      .then(json => {
		      	if (json && json.status !== 500) {
							dispatch(requestChangeProfileImageSucceded(json.image))
							alert(json.message)
		      	}else{
							dispatch(requestChangeProfileImageFailed())
		      	}
		      }
	      )
	      .catch((errors) => {
					console.log(errors);
				});		  
			}else{
				alert('Formato de archivo no válido, las extensiones permitidas son ' + allowed_extensions.join(', '));
			}
		}else{
			alert('No se ha seleccionado ninguna imágen');
		}
	}
}

export function fetchGetEmail(){
	
	return function (dispatch,getState){
		const token = getState().main.globalData.token;
		console.log(token)
		dispatch(requestInProcess())
		return fetch(process.env.REACT_APP_URL_SERVER+"/auth/getInbox",{
			method: "GET", // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				'Authorization': 'Bearer ' + token
			},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			detResponseStatusErr(response.status)
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json && json.messages) {
				dispatch(fetchGetEmailSucceeded(emailTransform(json.messages)))
			}	
			else{
				dispatch(requestFailed("No se pudo cargar el correo"))
			}	
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))

			console.log(errors)
			dispatch(requestFailed(errors.message))
			console.log('No se pudo cargar el correo')
		})
	}
}

 export function fetchEmailSingle(id){
	const _FETCH = "fetchEmailSingle"
	return function (dispatch,getState){
		const token = getState().main.globalData.token;
		dispatch(requestInProcess(_FETCH))
		return fetch(process.env.REACT_APP_URL_SERVER+"/auth/getInbox/"+id,{
			method: "GET", // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			headers: {
				"Content-Type": "text/html; charset=utf-8",
				'Authorization': 'Bearer ' + token
			},
		})
		.then(
			response => {
			if (response.ok) {
				return response.text()
			}
			detResponseStatusErr(response.status)
			},
			error => console.log('An error occurred.', error)
		)
		.then(data => {
			 if (data) {
				dispatch(fetchEmailSingleSucceeded(data))
			}	
			else{
				dispatch(requestFailed("No se pudo cargar el contenido del correo"))
			}	 
		}
		).catch((errors) => {
			console.log(errors)
			dispatch(requestFailed('No se pudo cargar el contenido del correo'))
			console.log('No se pudo cargar el contenido del correo')
		})
	}
} 

