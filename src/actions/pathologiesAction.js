import pathologies from '../constants/pathologies'
import pathologiesTransforms from '../transforms/pathologiesTransform'
export const HANDLE_PATHOLOGY_INPUT_CHANGE = 'HANDLE_PATHOLOGY_INPUT_CHANGE'
export const ADD_NEW_PATHOLOGY = 'ADD_NEW_PATHOLOGY'
export const DELETE_PATHOLOGY = 'DELETE_PATHOLOGY'
export const EDIT_PATHOLOGY = 'EDITING_PATHOLOGY'
export const CANCEL_EDIT_PATHOLOGY = 'CANCEL_EDIT_PATHOLOGY'
export const HANDLE_STATUS_INPUT_CHANGE = 'HANDLE_STATUS_INPUT_CHANGE'
export const FETCH_REQUEST_PATHOLOGIES_SUCCEEDED = 'FETCH_REQUEST_PATHOLOGIES_SUCCEEDED'
export const SAVE_NEW_PATHOLOGY = 'SAVE_NEW_PATHOLOGY'
export const UPDATE_PATHOLOGY = 'UPDATE_PATHOLOGY'

export function handleInputChange(event,id){
	return {
		type: HANDLE_PATHOLOGY_INPUT_CHANGE,
		event,
		id:id,
	}
}

export function updatePathology(pathology){
	return{
		type: UPDATE_PATHOLOGY,
		pathology: pathology
	}
}

export function saveNewPathology(pathology,id){
	return{
		type: SAVE_NEW_PATHOLOGY,
		pathology: pathology,
		new_pathology_id:id,
	}
}

export function handleStatusInputChange(event,id){
	return {
		type: HANDLE_STATUS_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}

export function addNewPathology(){
	return {
		type: ADD_NEW_PATHOLOGY,
	}
}

export function deletePathology(id,cancelNew){
	return {
		type: DELETE_PATHOLOGY,
		id: id,
		cancelNew:cancelNew
	}
}

export function editPathology(id){
	return {
		type: EDIT_PATHOLOGY,
		id: id
	}
}

export function cancelEditPathology(text,id){
	return{
		type: CANCEL_EDIT_PATHOLOGY,
		text: text,
		id:id
	}
}

export function fetchRequestPathologiesSucceeded(pathologies){
	return{ 
		type: FETCH_REQUEST_PATHOLOGIES_SUCCEEDED,
		pathologies: pathologies
	}
}

export function fetchPathologies(){
	
	return function (dispatch,getState) {	
		//dispatch(fetchRequestPathologiesSucceeded(pathologies))
		return fetch(process.env.REACT_APP_URL_SERVER+"/pathologies",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			//'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });

			if (response.status === 403)
				return response.json().then(err => { throw err; });
			
			},
			
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(fetchRequestPathologiesSucceeded(pathologiesTransforms(json.pathologies)))
			}
			
		}
		).catch((errors) => {
			console.log(errors)
			console.log('No se pudieron cargar las patologías')
		})
		
	}
}

export function fetchCreatePathology(pathology){
	let newPathology = {
			name: pathology.name,
			description: pathology.description?pathology.description:"-",
			sector: parseInt(pathology.sector),
			userType: parseInt(pathology.userType)		     
	}
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/pathologies",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
		},
		body: JSON.stringify(newPathology)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(saveNewPathology(pathologiesTransforms(json.pathology),pathology.id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddPathologyFailed(errors))
			console.log(errors)
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchDeletePathology(id){
	return function (dispatch,getState) {
		 const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/pathologies/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("País eliminado exitosamente")
				dispatch(deletePathology(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddPathologyFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		}) 
		
	}
}

export function fetchUpdatePathology(pathology){
	let pathology_to_update = {		
			name: pathology.name,
			description: pathology.description,
			sector: parseInt(pathology.sector),
			userType: parseInt(pathology.userType)		      
	}
	console.log(pathology_to_update)
	return function (dispatch,getState) {
		
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/pathologies/"+pathology.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(pathology_to_update)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updatePathology(pathology))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddPathologyFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la modificación')
		}) 
		
	}
}
