import classifications from '../constants/classifications'
import classificationsTransforms from '../transforms/classificationsTransform'
export const HANDLE_CLASSIFICATION_INPUT_CHANGE = 'HANDLE_CLASSIFICATION_INPUT_CHANGE'
export const ADD_NEW_CLASSIFICATION = 'ADD_NEW_CLASSIFICATION'
export const DELETE_CLASSIFICATION = 'DELETE_CLASSIFICATION'
export const EDIT_CLASSIFICATION = 'EDITING_CLASSIFICATION'
export const CANCEL_EDIT_CLASSIFICATION = 'CANCEL_EDIT_CLASSIFICATION'
export const HANDLE_STATUS_INPUT_CHANGE = 'HANDLE_STATUS_INPUT_CHANGE'
export const FETCH_REQUEST_CLASSIFICATIONS_SUCCEEDED = 'FETCH_REQUEST_CLASSIFICATIONS_SUCCEEDED'
export const SAVE_NEW_CLASSIFICATION = 'SAVE_NEW_CLASSIFICATION'
export const UPDATE_CLASSIFICATION = 'UPDATE_CLASSIFICATION'

export function handleInputChange(event,id){
	return {
		type: HANDLE_CLASSIFICATION_INPUT_CHANGE,
		event,
		id:id,
	}
}

export function updateClassification(classification){
	return{
		type: UPDATE_CLASSIFICATION,
		classification: classification
	}
}

export function saveNewClassification(classification,id){
	return{
		type: SAVE_NEW_CLASSIFICATION,
		classification: classification,
		new_classification_id:id,
	}
}

export function handleStatusInputChange(event,id){
	return {
		type: HANDLE_STATUS_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}

export function addNewClassification(){
	return {
		type: ADD_NEW_CLASSIFICATION,
	}
}

export function deleteClassification(id,cancelNew){
	return {
		type: DELETE_CLASSIFICATION,
		id: id,
		cancelNew:cancelNew
	}
}

export function editClassification(id){
	return {
		type: EDIT_CLASSIFICATION,
		id: id
	}
}

export function cancelEditClassification(text,id){
	return{
		type: CANCEL_EDIT_CLASSIFICATION,
		text: text,
		id:id
	}
}

export function fetchRequestClassificationsSucceeded(classifications){
	return{ 
		type: FETCH_REQUEST_CLASSIFICATIONS_SUCCEEDED,
		classifications: classifications
	}
}

export function fetchClassifications(){
	
	return function (dispatch,getState) {	
		//dispatch(fetchRequestClassificationsSucceeded(classifications))
		return fetch(process.env.REACT_APP_URL_SERVER+"/clasifications",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			//'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });

			if (response.status === 403)
				return response.json().then(err => { throw err; });
			
			},
			
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(fetchRequestClassificationsSucceeded(classificationsTransforms(json.clasifications)))
			}
			
		}
		).catch((errors) => {
			console.log(errors)
			console.log('No se pudieron cargar las clasificaciones')
		})
		
	}
}

export function fetchCreateClassification(classification){
	let newClassification = {
			name: classification.name,
			description: classification.description?classification.description:"-",
			sector: parseInt(classification.sector),
			userType: parseInt(classification.userType)		     
	}
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/clasifications",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
		},
		body: JSON.stringify(newClassification)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(saveNewClassification(classificationsTransforms(json.clasification),classification.id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddClassificationFailed(errors))
			console.log(errors)
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchDeleteClassification(id){
	return function (dispatch,getState) {
		 const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/clasifications/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("País eliminado exitosamente")
				dispatch(deleteClassification(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddClassificationFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		}) 
		
	}
}

export function fetchUpdateClassification(classification){
	let classification_to_update = {		
			name: classification.name,
			description: classification.description,
			sector: parseInt(classification.sector),
			userType: parseInt(classification.userType)		      
	}
	console.log(classification_to_update)
	return function (dispatch,getState) {
		
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/clasifications/"+classification.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(classification_to_update)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateClassification(classification))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddClassificationFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la modificación')
		}) 
		
	}
}
