import userList from "../constants/users"
import userTransform from '../transforms/userTransform'

export const HANDLE_EMAIL_INPUT_CHANGE = 'HANDLE_EMAIL_INPUT_CHANGE'
export const FETCH_USERS_SUCCEEDED = 'FETCH_USERS_SUCCEEDED'
export const SHOW_USER = 'SHOW_USER'
export const EDIT_USER = 'EDIT_USER'
export const CANCEL_EDIT_USER = 'CANCEL_EDIT_USER'
export const RESTORE_USER_DATA = 'RESTORE_USER_DATA'
export const RESTORE_USER = 'RESTORE_USER'
export const DELETE_USER = 'DELETE_USER'
export const HANDLE_USER_INPUT_CHANGE = 'HANDLE_USER_INPUT_CHANGE'
export const HANDLE_USER_EMAIL_INPUT_CHANGE = 'HANDLE_USER_EMAIL_INPUT_CHANGE'
export const HANDLE_USER_PASSWORD_INPUT_CHANGE = 'HANDLE_USER_PASSWORD_INPUT_CHANGE'
export const HANDLE_USER_CONFIRM_PASSWORD_INPUT_CHANGE = 'HANDLE_USER_CONFIRM_PASSWORD_INPUT_CHANGE'
export const HANDLE_USER_USERNAME_INPUT_CHANGE = 'HANDLE_USER_USERNAME_INPUT_CHANGE'
export const HANDLE_USER_ROLE_INPUT_CHANGE = 'HANDLE_USER_ROLE_INPUT_CHANGE'
export const FETCH_REQUEST_USERS_SUCCEEDED = 'FETCH_REQUEST_USERS_SUCCEEDED'
export const UPDATE_USER = 'UPDATE_USER'
export const SAVE_NEW_USER= 'SAVE_NEW_USER'
export const HANDLE_USER_ROLE_CHANGE ='HANDLE_USER_ROLE_CHANGE'
export const HANDLE_USER_COUNTRY_CHANGE ='HANDLE_USER_COUNTRY_CHANGE'
export const SHOW_PUBLIC_USER_PROFILE_SUCCEEDED = 'SHOW_PUBLIC_USER_PROFILE_SUCCEEDED'
export const HANDLE_USER_MULTI_SELECT_INPUT = "HANDLE_USER_MULTI_SELECT_INPUT"
export const SET_FILTERED_USERS = "SET_FILTERED_USERS"

export function setFilteresUsers(users){
	return{
		type: SET_FILTERED_USERS,
		users
	}
}

export function showPublicUserProfileSucceeded(user){
	return{
		type: SHOW_PUBLIC_USER_PROFILE_SUCCEEDED,
		user
	}
}

export function handleUserInputChange(event){
	return{
		type:HANDLE_USER_INPUT_CHANGE,
		event
	}
}
export function handleUserMultiSelectInput(array,state){
	return{
		type: HANDLE_USER_MULTI_SELECT_INPUT,
		array,
		state
	}
}
export function handleUserRoleChange(event){
	return{
		type:HANDLE_USER_ROLE_CHANGE,
		event
	}
}
export function handleUserCountryChange(event){
	return{
		type:HANDLE_USER_COUNTRY_CHANGE,
		event
	}
}

export function fetchUsersSucceeded(users){
	return{
		type: FETCH_USERS_SUCCEEDED,
		users:users
	}
}

export function showUser(user){
	return{
		type: SHOW_USER,
		user
	}
}

export function editUser(user){
	return{
		type: EDIT_USER,
		user
	}
}

export function updateUser(user){
	return{
		type: UPDATE_USER,
		user
	}
}

export function saveNewUser(user,id){
	return{
		type: SAVE_NEW_USER,
		user: user,
		new_user_id: id
	}
}

export function restoreUser(){
	return{
		type: RESTORE_USER
	}
}

export function deleteUser(id){
	return{
		type: DELETE_USER,
		id
	}
}


export function restoreUserData(){
	return{
		type:RESTORE_USER_DATA
	}
}

export function fetchRequestUsersSucceeded(users){
	return{ 
		type: FETCH_REQUEST_USERS_SUCCEEDED,
		users: users
	}
}

export function fetchUsers(){
	return function (dispatch,getState) {
		
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/users",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
				console.log('Se pudieron cargar los usuarios')
				dispatch(fetchRequestUsersSucceeded(userTransform(json.users)))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			
			}
			console.log(errors)
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar los usuarios')
		})
		
	}
}

export function fetchDeleteUser(id){
	return function (dispatch,getState) {
		
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/users/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("Usuario eliminado exitosamente")
				dispatch(deleteUser(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		})
		
	}
}

 
export function fetchCreateUser(user){
		
		
	/*let newUser = {
			email : user.email,      
			password : user.password,
			passwordConfirm: user.confirmPassword,
			//telephone: "555",
			country: user.country_id,
			role: user.role_id,			
			/*username : "123",
			name: "Moises",
			surname: "Cam",
			birthdate: "2019-10-12",
			//gender:1,
			
	}*/
	let new_user = {
        email: user.email,
		password: user.password,
		confirmPassword: user.confirmPassword,
		name: user.name,
		lastName: user.lastName,
		phone: user.phone,
		userType: 1,
		birthdate: user.birthdate,
		instructionDegree: user.instruction_degree,
		jobType: user.job_type,
		jobName: user.job,
		currentStudy: user.current_study,
		country: "Venezuela",
		pathologies:  [1],
		socialReason: user.socialReason,
		clasificationId: 1,
		sex: user.sex,
		facebook: user.facebook,
		twitter: user.twitter,
		instagram: user.instagram,
		referralCode: user.referralCode,
		webSite: user.webSite,
		address: user.address	
    }
		
		
	return function (dispatch,getState) {
		
		//dispatch(saveNewUser(json.user,user.id))
		const token = getState().main.globalData.token		
		return fetch(process.env.REACT_APP_URL_SERVER+"/auth/register",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(new_user), // body data type must match "Content-Type" header
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {			
			if (json) {
			if(json.email){			
				dispatch(saveNewUser(json.user,user.id))
								
			}
			else{
				//dispatch(requestRegisterNewUserFailed(json))							
			}
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}
export function fetchUpdateUser(user){
	let update_user = {
        //email: user.email,
		name: user.name?user.name:"",
		lastName: user.lastname?user.lastname:"",
		phone: user.telephone?user.telephone:"",
		userType: user.user_type,
		birthdate: user.birthdate?user.birthdate:"",
		instructionDegree: user.instruction_degree?user.instruction_degree:"",
		jobType: user.job_type?user.job_type:"",
		jobName: user.job?user.job:"",
		currentStudy: user.current_study?user.current_study:"",
		//country: ,
		//pathologies:  [1],
		socialReason: user.social_reason?user.social_reason:"",
		clasificationId: user.classification?user.classification.id:"",
		//sex: user.sex,
		facebook: user.facebook?user.facebook:"",
		twitter: user.twitter?user.twitter:"",
		instagram: user.instagram?user.instagram:"",
		referralCode: user.referralCode?user.referralCode:"",
		webSite: user.webSite?user.webSite:"",
		address: user.address?user.address:""	
    }

	return function (dispatch,getState) {
		
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_URL_SERVER+"/users/"+user.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(update_user)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });

			if (response.status === 500)
				return response.json().then(err => { throw err; });

			if (response.status === 403)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateUser(user))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log(errors)
			console.log('No se pudo completar la modificación')
		})
		
	}
}
