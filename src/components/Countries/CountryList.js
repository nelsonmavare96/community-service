import React from 'react'
import { Table } from 'reactstrap';
import {
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
} from 'reactstrap';

import Country from './Country'
import CountryEdit from './CountryEdit'
import CountryNew from './CountryNew'

class CountryList extends React.Component {

	constructor(props){
		super(props)
		this.hasSomeCountryEditing = this.hasSomeCountryEditing.bind(this)
	}

	hasSomeCountryEditing(countries){
		let _is_some = false
		if(countries.length>0) 
			countries.map((country) => {
				if(country.isEditing){
					_is_some=true
				}
			})
		return _is_some
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	render(){
		const {countries,onCancelNewCountry,onUpdateCountry,onSaveNewCountry, onHandleNameInputChange, onHandleStatusInputChange, onAddNewCountry,onDeleteCountry,onEditCountry,onCancelEditCountry} = this.props
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Paises 
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>Nro</th>
			            <th>Nombre</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomeCountryEditing(countries))?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(countries && countries.length > 0)? 
			        		countries.map((country,index) => (
			        			(country.isEditing)?
									<CountryEdit 
										key={index} 
										index={index} 
										country={country}
										onUpdateCountry={(c)=>onUpdateCountry(c)} 
										onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,country.id)} 
										onCancelEditCountry={(text)=>onCancelEditCountry(text,country.id)}
										onEditCountry = {()=>onEditCountry(country.id)}
										onHandleNameInputChange={(e)=>onHandleNameInputChange(e,country.id)} 
									/>
			        			:
			        			(country.isNew)?
									<CountryNew key={index} index={index} countries={countries} country={country} 
												onHandleNameInputChange={(e)=>onHandleNameInputChange(e,country.id)}
												onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,country.id)} 
												onCancelNewCountry = {()=>onCancelNewCountry(country.id)}
												onSaveNewCountry = {(c)=>onSaveNewCountry(c)}
									/>
			        			:
									<Country 
										key={index}
										index={index} 
										country={country} 
										statusAsString={this.statusAsString} 
										onDeleteCountry = {()=>onDeleteCountry(country.id)}
										onEditCountry = {()=>onEditCountry(country.id)}
									/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={5}> No existen paises registrados </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={5}>
					        <Button color="primary" onClick={() => {onAddNewCountry()}} >
					        	Agregar País
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default CountryList;