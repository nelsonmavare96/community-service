import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Country extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {index,country, statusAsString, onDeleteCountry, onEditCountry} = this.props

		return(
			<>
			<tr key={country.id}>
				<td> {index+1} </td>		
				<td> {country.name} </td>
				<td> {statusAsString(country.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditCountry(country.id)}> 
						Modificar
					</Button> 
				</td>
				<td>
					{country.status==1 && 
						<Button color="secondary" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button>
					} 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={country} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={()=> {onDeleteCountry(country.id);this.openHandleModal()}} />
			</>
		);
	}
}

export default Country;