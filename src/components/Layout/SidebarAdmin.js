import logo200Image from 'assets/img/logo/logo_200.png';
import sidebarBgImage from 'assets/img/sidebar/sidebar-4.jpg';
import SourceLink from 'components/SourceLink';
import React from 'react';
import { FaGithub } from 'react-icons/fa';
import {
  MdAccountCircle,
  MdVerifiedUser,
  MdExitToApp,
  MdPermContactCalendar,
  MdLocationOn,
  MdClass,
  MdEmail
} from 'react-icons/md';
import {FaMicroscope} from "react-icons/fa"
//import { AiFillTwitterSquare, AiFillFacebook, AiFillInstagram } from "react-icons/ai";
import { NavLink, Link } from 'react-router-dom';
import {
  // UncontrolledTooltip,
  Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Button,
} from 'reactstrap';
import bn from 'utils/bemnames';
import {connect} from 'react-redux'
import {
  resetUserData,
  resetFetch
} from '../../actions/mainAppAction'
import { bindActionCreators } from 'redux';

const sidebarBackground = {
  backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const navItems = [
 // { to: '/', name: 'Inicio', exact: true, Icon: MdDashboard },
  { to: '/users', name: 'Usuarios', exact: false, Icon: MdPermContactCalendar }, 
  //{ to: '/countries', name: 'Paises', exact: false, Icon: MdLocationOn },
  { to: '/pathologies', name: 'Patologías', exact: false, Icon: FaMicroscope},
  { to: '/classifications', name: 'Clasificaciones', exact: false, Icon: MdClass},
  { to: '/email', name: 'Correo', exact: false, Icon: MdEmail},
  { to: '/posts', name: 'Publicaciones', exact: false, Icon: MdEmail},
  { to: '/userProfile', name: 'Perfil', exact: false, Icon: MdAccountCircle },

];


const bem = bn.create('sidebar');

class SidebarAdmin extends React.Component {
  state = {
    isOpenReports: false,
    isOpenRankings:false,
    modal_validation: false,
    modal_social_networks:false,

  };

  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  clickModalValidation = (e) =>{
    return this.setState({
        modal_validation: !this.state.modal_validation,
    }); 
}

  clickModalSocialNetworks = (e) =>{
    return this.setState({
      modal_social_networks: !this.state.modal_social_networks,
    }); 
  }


  render() {
    const {resetUserData, user, resetFetch} = this.props
    return (
      <aside id="aside-custom" className={bem.b()} data-image={sidebarBgImage}>
        <div className={bem.e('background')} style={sidebarBackground} />
        <div className={bem.e('content')}>
          <Navbar className="justify-content-center">
            <label style={{marginBottom: "0"}}>
              <img src={user.image?user.image:require('../../assets/img/users/default.jpg')} className="rounded-circle mb-2 sidebar-img" />
            </label>
            <div className="card-title" style={{fontSize: "larger", marginBottom:0, marginLeft:10}}>{user.name} {user.lastname}</div>
          </Navbar>
          <Nav vertical className="sidebar-options-container">
            {navItems.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  className="text-uppercase"
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}
            {/* <NavItem className={bem.e('nav-item')}>
              <BSNavLink
                className="text-uppercase"
                tag={NavLink}
                to={'#'}
                activeClassName="active"
                //exact={exact}
                onClick={()=> {this.clickModalSocialNetworks()}}
              >
                <MdVerifiedUser className={bem.e('nav-item-icon')} />
                <span className="">{'Conectar con redes sociales'}</span>
              </BSNavLink>
            </NavItem> */}
            <NavItem className={bem.e('nav-item')}>
              <div
                className="text-uppercase"
                onClick={()=> {this.clickModalValidation(); resetFetch()}}
                style={{padding: '0.5rem 1rem', fontWeight:400, cursor:'pointer'}}
              >
                <MdExitToApp className={bem.e('nav-item-icon')} />
                <span className="">{'cerrar sesión'}</span>
              </div>
            </NavItem>

            {/* <Modal
                isOpen={this.state.modal_social_networks}>
                    <ModalHeader>Conectar con red social</ModalHeader>
                <ModalBody>
                    <FormGroup className="justify-content-center">
                        <AiFillFacebook className="social-network-icon"/>
                        <AiFillInstagram className="social-network-icon"/>
                        <AiFillTwitterSquare className="social-network-icon"/>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>

                    <Button color="danger" onClick={()=> {this.clickModalSocialNetworks()}}>
                        Cancelar
                    </Button>                      
                </ModalFooter>
            </Modal> */}

            <Modal
                isOpen={this.state.modal_validation}>
                    <ModalHeader>Cerrar Sesión</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label for="email">¿Está seguro que desea cerrar sesión?</Label>{' '}
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Link to="/">
                        <Button color="success" onClick={()=> resetUserData()}>
                            Aceptar
                        </Button>
                    </Link>
                        <Button color="danger" onClick={()=> {this.clickModalValidation()}}>
                            Cancelar
                        </Button>                      
                </ModalFooter>
            </Modal>
          </Nav>
        </div>
      </aside>
    );
  }
}

const mapStateToProps = state => {
  return {
    globalData: state.main.globalData,
    user: state.main.userData,

  }
}


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    resetUserData,
    resetFetch
  }, dispatch)
);

export default connect(mapStateToProps,mapDispatchToProps)(SidebarAdmin)
