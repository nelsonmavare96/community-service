import React from 'react'
import {Button, Input} from 'reactstrap'

class RoleEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {role, onUpdateRole, index, onCancelEditRole, onHandleDescriptionInputChange,onHandleNameInputChange,onHandleStatusInputChange,onUpdateRol} = this.props

		return(
			<tr key={role.id}>
				<td> {index} </td>
				<td> 
					<Input 
						data-id={role.id}
						name="name"
						defaultValue={role.name}
						onChange={(e) => onHandleNameInputChange(e,role.id)}
					/>
				</td>
				<td> 
					<Input 
						data-id={role.id}
						name="description"
						defaultValue={role.description}
						onChange={(e) => onHandleDescriptionInputChange(e,role.id)}
					/>
				</td>		
				<td>  
		          <Input data-id={role.id} defaultValue={role.status} onChange={(e) => onHandleStatusInputChange(e,role.id)} type="select" name="status">
		            <option value={-1}>Selecciona una opción</option>
		            <option value={1}>Activo</option>
		            <option value={2}>Inactivo</option>
		          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdateRol(role)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditRole(role.originalName)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default RoleEdit;