import React from 'react'
import {Button, Input} from 'reactstrap'

class RoleNew extends React.Component {
	
	render(){
		const {
			rol, 
			onHandleNameInputChange, 
			onHandleDescriptionInputChange, 
			onHandleStatusInputChange, 
			onCancelAddNewRol,
			onSaveNewRol
		} = this.props

		return(
			<tr key={rol.id}>
				<td> {rol.id} </td>
				<td> 
					<Input 
						data-id={rol.id}
						name="name"
						defaultValue={rol.name}
						onChange={(e) => onHandleNameInputChange(e)}
					/>
				</td>
				<td> 
					<Input 
						data-id={rol.id}
						name="description"
						defaultValue={rol.description}
						onChange={(e) => onHandleDescriptionInputChange(e)}
					/>
				</td>						
				<td>  
		          <Input data-id={rol.id} defaultValue={rol.status} onChange={(e) => onHandleStatusInputChange(e)} type="select" name="status">
		            <option value={-1}>Selecciona una opción</option>
		            <option value={1}>Activo</option>
		            <option value={2}>Inactivo</option>
		          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onSaveNewRol(rol)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelAddNewRol(rol.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default RoleNew;