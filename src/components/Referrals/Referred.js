import React, {Text} from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
	FormText,
} from 'reactstrap';
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'


class Referred extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}


	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {referred, onEditReferred, onDeleteReferred, rolIdAsString, onGoBackToList,onChangeView} = this.props

		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12}>
	          <Card>
	            <CardHeader>Información Usuario</CardHeader>
	            <CardBody>
	              <Form>
	                <FormGroup row>
										<Label sm={6}>
	                    <Text style={{fontWeight:"bold"}}>Nombre: </Text> {" "+referred.Name}
	                  </Label>
										{referred.UserType==1&&
											<Label sm={6}>
												<Text style={{fontWeight:"bold"}}>Apellido: </Text> {" "+referred.Lastname}
											</Label>
										}
										<Label sm={6}>
											<Text style={{fontWeight:"bold"}}>Razón social: </Text> {" "+referred.SocialReason}
	                  </Label>
										<Label sm={6}>
											<Text style={{fontWeight:"bold"}}>Teléfono: </Text> {" "+referred.Phone}
	                  </Label>
										<Label sm={6}>
											<Text style={{fontWeight:"bold"}}>Clasificación: </Text> {" "+referred.clasification.Name}
	                  </Label>
										<Label sm={6}>
											<Text style={{fontWeight:"bold"}}>Tipo de usuario: </Text> {" "+referred.UserType}
	                  </Label>
	                </FormGroup>
									
	                { referred.referred_type=="institution"?
											<FormGroup row>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Sitio Web: </Text> {" "+referred.website}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Facebook: </Text> {" "+referred.facebook}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Twitter: </Text> {" "+referred.twitter}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Instagram: </Text> {" "+referred.instagram}
												</Label>
												<Label sm={12}>
													<Text style={{fontWeight:"bold"}}>Ubicación: </Text> {" "+referred.address}
												</Label>
											</FormGroup>
										:
											<FormGroup row>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Fecha de nacimiento: </Text> {" "+referred.birthdate}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Grado de institucuión: </Text> {" "+referred.instruction_degree}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Trabajo: </Text> {" "+referred.job}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Tipo de trabajo: </Text> {" "+referred.type_job}
												</Label>
												<Label sm={6}>
													<Text style={{fontWeight:"bold"}}>Estudio actual: </Text> {" "+referred.current_study}
												</Label>
											</FormGroup>
									}
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button color="info" style={{float: "right"}} onClick={()=> onGoBackToList()}>Volver</Button>
	                  </Col>
	                </FormGroup>
	              </Form>
	            </CardBody>
	          </Card>
	        </Col>
	        <ModalConfirmDeleteItem item={referred} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteReferred} onChangeView={onChangeView}/>
			</div>
		);
	}
}

export default Referred;