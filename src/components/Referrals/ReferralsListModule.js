import React from 'react'
import ReferredList from './ReferredList'
import ReferredView from './ReferredView'

class ReferralsListModule extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			view: "list",
		}

		this.statusAsString=this.statusAsString.bind(this)
		this.onViewReferred=this.onViewReferred.bind(this)
		this.onEditClick=this.onEditClick.bind(this)
		this.onCancelEditClick=this.onCancelEditClick.bind(this)
		this.onUpdateReferredClick=this.onUpdateReferredClick.bind(this)
		this.onAddNewReferredClick=this.onAddNewReferredClick.bind(this)
		this.onSaveClick=this.onSaveClick.bind(this)
		this.onCancelAddNewReferredClick=this.onCancelAddNewReferredClick.bind(this)
		this.onHandleInputChange=this.onHandleInputChange.bind(this)
		this.rolIdAsString=this.rolIdAsString.bind(this)
		this.onGoBackToList=this.onGoBackToList.bind(this)
		this.onChangeView = this.onChangeView.bind(this)
	}

	onHandleInputChange(event){
		let referred = Object.assign({}, this.state.referred, {
			[event.target.name]: event.target.value
		})

		this.setState({referred})
	}

	onGoBackToList(){
		this.setState({view:"list"})
	}

	rolIdAsString(rol_id){
		let rol_string = ""
		switch(parseInt(rol_id,10)){
			case 1: 
				rol_string = "Administrador"; break;
			case 2: 
				rol_string = "Usuario"; break;
			default:
				rol_string = "Otro"; break;
		}
		return rol_string
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	onViewReferred(reason){
		switch(reason.toString()){
			case "show":
				this.setState({view: "show"}); break;
			case "edit":
				this.setState({view: "edit"}); break; 
			case "new":
				this.setState({view: "new"}); break;
		}
	}

	onEditClick(id){
		this.setState({view: "edit"})
	}

	onCancelEditClick(id){
		this.setState({view: "list"})
	}

	onChangeView(type){
		//type is"edit","show" or "list"
		this.setState({view: type})
	}

	onUpdateReferredClick(referred){
		this.setState({view: "show"})

	}

	onAddNewReferredClick(){
		this.setState({view: "new"})		
	}

	onSaveClick(referred){
		this.setState({view: "list"})
	}

	onCancelAddNewReferredClick(){
		this.setState({referred:{}, view: "list"})
	}

	render(){
		const {view} = this.state
		const {
			referrals,
			countries,
			roles,
			referred,
			classifications,
			pathologies,
			onShowReferred,
			onEditReferred,
			onRestoreReferred,
			onDeleteReferred,
			onHandleReferredInputChange,
			onUpdateReferred,
			onHandleReferredRoleChange,
			onHandleReferredCountryChange,
			onSaveNewReferred
		} = this.props
		return(
			<div> 
			{
				(view && view == "list")?
					<ReferredList
						countries={countries}
						roles={roles}
						pathologies = {pathologies}
						referrals={referrals}
						classifications={classifications}
						rolIdAsString={this.rolIdAsString}
						statusAsString={this.statusAsString}
						onShowReferred={onShowReferred} 
						onEditReferred={onEditReferred}
						onOpenDeleteReferredModal={this.onOpenDelete} 
						onAddNewReferredClick={this.onAddNewReferredClick}
						onChangeView={this.onChangeView}
						onRestoreReferred={onRestoreReferred}
						onDeleteReferred = {onDeleteReferred} 
					/>
				:
					<ReferredView 
						view={view}
						countries={countries}
						roles={roles}
						onHandleReferredRoleChange={onHandleReferredRoleChange}
						onHandleReferredCountryChange={onHandleReferredCountryChange} 
						referred={referred} 
						onGoBackToList={this.onGoBackToList}
						rolIdAsString={this.rolIdAsString}
						onHandleReferredInputChange={onHandleReferredInputChange}
						onEditReferred={onEditReferred}
						onCancelEditClick={this.onCancelEditClick}
						onSaveClick={this.onSaveClick}
						onOpenDeleteReferredModal={this.onOpenDelete}
						onDeleteReferred={onDeleteReferred}
						onCancelAddNewReferredClick={this.onCancelAddNewReferredClick}
						onUpdateReferredClick={this.onUpdateReferredClick}
						onChangeView={this.onChangeView}
						onUpdateReferred={onUpdateReferred}
						onSaveNewReferred={onSaveNewReferred}
					/>
			}
			</div>
		);
	}
}

export default ReferralsListModule;