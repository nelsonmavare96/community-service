import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Col,
  Card,
  CardHeader,
  CardBody,
} from 'reactstrap';


class ReferredEditable extends React.Component {

	rolIdAsString(rol_id, roles){
		let rol_string = ""
		roles.map((rol)=>{
			if(rol.id ==rol_id){
				rol_string=rol.name
			}	
		}) 
		
		return rol_string
	}	

	render(){
		const {
					referred,
					roles,
					countries, 
					onCancelEditClick, 
					onUpdateReferredClick, 
					onUpdateReferred,
					onHandleReferredRoleChange,
					onHandleReferredInputChange,
					onHandleReferredCountryChange,
				} = this.props
		
		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12}>
	          <Card>
	            <CardHeader>Editar información de usuario</CardHeader>
	            <CardBody>
	              <Form>
	                <FormGroup row>
	                  <Label for="email" sm={2}>
	                    Email
	                  </Label>
	                  <Col sm={10}>
	                    <Input
	                      type="email"
	                      name="email"
	                      defaultValue={referred.email}
	                      onChange={(e) => onHandleReferredInputChange(e)}
	                      placeholder="Ingrese el email"
	                    />
	                  </Col>
	                </FormGroup>
	                <FormGroup row>
	                  <Label for="exampleSelect" sm={2}>
	                    Rol
	                  </Label>
	                  <Col sm={10}>
	                    <Input type="select" name="id" value={(referred && referred.role.id)? referred.role.id : -1 } onChange={(e) => onHandleReferredRoleChange(e)}>
									{
										(roles && roles.length > 0)? 
										roles.map((rol) => (
											<option value={rol.id}> {rol.name} </option>
										)):
										<option value={-1}> No existen roles </option>
									}
	                  	</Input>
	                  </Col>
	                </FormGroup>
					<FormGroup row>
	                  <Label for="exampleSelect" sm={2}>
	                    Rol
	                  </Label>
	                  <Col sm={10}>
	                    <Input type="select" name="id" value={(referred && referred.country.id)? referred.country.id: -1 } onChange={(e) => onHandleReferredCountryChange(e)}>
									{
										(countries && countries.length > 0)? 
										countries.map((country) => (
											<option value={country.id}> {country.name} </option>
										)):
										<option value={-1}> No existen Paises </option>
									}
	                  	</Input>
	                  </Col>
	                </FormGroup>
					
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button style={{float: "right"}} onClick={()=> {onCancelEditClick();}}>Cancelar</Button>
	                    <Button color="success" style={{marginRight: "20px",float: "right"}} onClick={() => {onUpdateReferredClick(referred); onUpdateReferred(referred)}}>Guardar</Button>
	                  </Col>
	                </FormGroup>
	              </Form>
	            </CardBody>
	          </Card>
	        </Col>
			</div>
		);
	}
}

export default ReferredEditable;