import React from 'react'
import ReferredNew from './ReferredNew'
import Referred from './Referred'
import ReferredEditable from './ReferredEditable'

class ReferredView extends React.Component {

	render(){
		
		const {
			view,
			countries,
			roles,
			referred,
			onCancelEditClick,
			onSaveClick,
			onOpenDeleteReferredModal,
			onDeleteReferred,
			onCancelAddNewReferredClick,
			onUpdateReferredClick,
			onEditReferred,
			rolIdAsString,
			onGoBackToList,
			onChangeView,
			onHandleReferredRoleChange,
			onHandleReferredInputChange,
			onHandleReferredCountryChange,
			onHandleEmailInputChange,
			onHandleReferredEmailInputChange,
			onHandleReferredPasswordInputChange,
			onHandleReferredPassworConfirmdInputChange,
			onHandleReferredReferrednameInputChange,
			onHandleReferredRoleInputChange,
			onUpdateReferred,
			onSaveNewReferred
		} = this.props

		return(
			<div>
			{
				(view && view.toString() === "new")?
					<ReferredNew 
						referred={referred}
						roles={roles}
						countries={countries} 	
						onCancelAddNewReferredClick={onCancelAddNewReferredClick} 
						onSaveClick={onSaveClick}
						onHandleReferredInputChange={onHandleReferredInputChange}
						onSaveNewReferred={onSaveNewReferred} 
						/>
				:
				(view && view.toString() === "show")?
					<Referred referred={referred} onChangeView={onChangeView} onEditReferred={onEditReferred} onDeleteReferred={onDeleteReferred} rolIdAsString={rolIdAsString} onGoBackToList={onGoBackToList} />
				:
					<ReferredEditable 
						referred={referred}
						roles={roles}
						countries={countries} 
						onChangeView={onChangeView}
						onHandleReferredRoleChange={onHandleReferredRoleChange}
						onHandleReferredCountryChange={onHandleReferredCountryChange} 
						onCancelEditClick={onCancelEditClick}
						onUpdateReferredClick={onUpdateReferredClick}
						onHandleReferredInputChange={onHandleReferredInputChange}
						onUpdateReferred={onUpdateReferred} />
			}
			</div>
		);
	}
}

export default ReferredView;