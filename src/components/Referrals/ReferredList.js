import React from 'react'
import { Table } from 'reactstrap';
import {filterUsers} from '../../functions'
import {
  Row,
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
  Input
} from 'reactstrap';
import Select from 'react-select';
import ReferredRow from './ReferredRow'

class ReferredList extends React.Component {

	constructor(props){
		super(props)
		this.state={
			filtered_users:this.props.referrals,
			filter_by_classifications: false,
			filter_by_pathologies: false,
			array_selected_pathologies: [],
			selected_classification:""
		}
	}

	setFilteresUsers=(users)=>{
		this.setState({filtered_users:users})
	}

	filterByPathologies = (users,array) =>{
		if(array && array.length>0){
			this.setFilteresUsers(filterUsers(users,array));
			this.setState({filter_by_pathologies: true})

		}
		else{
			if(this.state.selected_classification)
				{this.setFilteresUsers(users.filter(user => user.classification.id==this.state.selected_classification));
				this.setState({filter_by_pathologies: false});
			}
			else{
				this.setFilteresUsers(users)
				this.setState({filter_by_pathologies: false});
			}
			
		}
	}

	filterByClassifications = (value,users) => {
		if(value){
			this.setFilteresUsers(users.filter(user => user.classification.id==value));
			this.setState({filter_by_classifications: true});
		}
		else{
			if(this.state.array_selected_pathologies && this.state.array_selected_pathologies.length>0)
				{this.setFilteresUsers(filterUsers(users,this.state.array_selected_pathologies));
				this.setState({filter_by_classifications: false});
			}
			else{
				this.setFilteresUsers(users);
				this.setState({filter_by_classifications: false});
			}
			
		}
	}

	render(){
		const {
			referrals,
			countries, 
			pathologies,
			classifications,
			onShowReferred, 
			onEditReferred, 
			onOpenDeleteReferredModal, 
			statusAsString,
			rolIdAsString,
			onChangeView,
			onDeleteReferred
		} = this.props
		const {filtered_users} = this.state;

		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Referidos 
					</CardHeader> 
					<CardBody>
						<Row style={{marginBottom:15}}>
							<Col sm={6}>
								<Select
									placeholder="Filtrar por patologías"
									isMulti
									isDisabled = {this.state.filter_by_classifications}
									className="basic-multi-select"
									classNamePrefix="select"
									onChange={(array) => {this.filterByPathologies(referrals,array);this.setState({array_selected_pathologies:array})}}
									options={pathologies} 
								/>
							</Col>
							<Col sm={2}>
								<Button 
									disabled={!this.state.filter_by_classifications} 
									onClick={()=>{
										this.setState({filter_by_classifications:false, filter_by_pathologies:true});
										this.setFilteresUsers(filterUsers(referrals,this.state.array_selected_pathologies));
								}}
								>
									Habilitar
								</Button>
							</Col>
						</Row> 
						<Row style={{marginBottom:15}}>
							<Col sm={6}>
								<Input type="select" disabled={this.state.filter_by_pathologies} name="classification_id" onChange={(e) => {
									this.filterByClassifications(e.target.value,referrals);
									this.setState({selected_classification:e.target.value})
								}}>
								<option value="">Filtrar por clasificación</option>
								{classifications && classifications.length>0 &&
								classifications.map((classification,index)=>(	
									//classification.status == 1 &&
									<option key={index} value={classification.id}>{classification.name}</option>
								))}     
								</Input>
							</Col> 
							<Col sm={2}>
								<Button 
									disabled={!this.state.filter_by_pathologies} 
									onClick={()=>{
										this.setState({filter_by_classifications:true, filter_by_pathologies:false});
										this.state.selected_classification?
												this.setFilteresUsers(referrals.filter(user => user.classification.id==this.state.selected_classification))
											:
												this.setFilteresUsers(referrals)
								}}
								>
									Habilitar
								</Button>
							</Col>
						</Row>
						<Table responsive>
					        <thead>
					          <tr>
							  	<th>Email</th>  
					            <th>Razón social</th>
					            <th>nombre</th>
								<th>teléfono</th>
								<th>Clasificación</th>
								<th>tipo de usuario</th>
								<th>Status</th>
					            
					          </tr>
					        </thead>
					        <tbody>
					        {
					        	(referrals && referrals.length > 0 && filtered_users.length>0)? 
					        		filtered_users.map((referred) => (
										<ReferredRow
											countries={countries} 
					        				key={referred.id} 
					        				referred={referred} 
					        				onShowReferred={onShowReferred} 
					        				onEditReferred={onEditReferred} 
					        				statusAsString={statusAsString} 
					        				onOpenDeleteReferredModal={onOpenDeleteReferredModal} 
											rolIdAsString={rolIdAsString}
											onChangeView={onChangeView}
											onDeleteReferred={onDeleteReferred}  
					        			/>
					        		))
					        	:
					        	<tr> 
					        		<td colSpan={5}> No existen referidos registrados </td>
					        	</tr>
					        }
					        <tr>
					        	{/* <td colSpan={5}>
							        <Button color="primary" onClick={() => {onAddNewReferredClick(); onRestoreReferred()}} >
							        	Agregar Referido
							        </Button>
						      	</td> */}
					      	</tr>
				        </tbody>
				      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default ReferredList;