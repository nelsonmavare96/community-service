import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'
import {transformUserType} from '../../functions'

class ReferredRow extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {
			referred, 
			onShowReferred, 
			onEditReferred, 
			statusAsString,
			rolIdAsString,
			onChangeView,
			onDeleteReferred
		} = this.props

		return(
			<>
				<tr key={referred.id}>
					<td> {referred.email} </td>	
					<td> {referred.social_reason} </td>
					<td> {referred.name} </td>
					<td> {referred.telephone} </td>
					  <td> {referred.classification.name} </td> 
					 <td> {transformUserType(referred.user_type)} </td>
					<td> {statusAsString(referred.status)} </td> 
					{/* <td> 
						<Button color="info" onClick={() => {onShowReferred(referred); onChangeView("show")}}> 
							Ver
						</Button> 
					</td> */}
				</tr>
				<ModalConfirmDeleteItem item={referred} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteReferred} onChangeView={onChangeView}/>
			</>
		);
	}
}

export default ReferredRow;