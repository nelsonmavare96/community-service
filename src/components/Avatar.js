import React from 'react';
import PropTypes from 'utils/propTypes';

import classNames from 'classnames';

import userImage from 'assets/img/users/user_example.jpg';

import {connect} from 'react-redux'

const Avatar = ({
  rounded,
  circle,
  src,
  size,
  tag: Tag,
  className,
  style,
  user,
  ...restProps
}) => {
  const classes = classNames({ 'rounded-circle': circle, rounded }, className);
  return (
    <Tag
      src={user.image?user.image:require('../assets/img/users/default.jpg')}
      style={{ width: size, height: size, ...style }}
      className={classes}
      {...restProps}
    />
  );
};

Avatar.propTypes = {
  tag: PropTypes.component,
  rounded: PropTypes.bool,
  circle: PropTypes.bool,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  src: PropTypes.string,
  style: PropTypes.object,
};

Avatar.defaultProps = {
  tag: 'img',
  rounded: false,
  circle: true,
  size: 40,
  src: userImage,
  style: {},
};

const mapStateToProps = state => {
  return {
    user: state.main.userData
  }
}

export default connect(mapStateToProps)(Avatar);
