import logo200Image from 'assets/img/logo/logo_200.png';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, Form, FormGroup, Input, Label,Modal, ModalBody, ModalFooter,ModalHeader, Col} from 'reactstrap';
import {Redirect, Link} from 'react-router-dom';
import {validateEmptyFields} from '../../functions'

class AuthForm extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      user: {
        email: "",
        password: "",
        confirmPassword: "",
        remember: false
      },
      message:null,
      modal: false,
    }
  }


  clickModal = (e) =>{
    return this.setState({
        modal: !this.state.modal,
    }); 
  }

  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      confirmPasswordLabel,
      confirmPasswordInputProps,
      onLogoClick,

      authState,
      user,
      fetch,
      countries,
      globalData,
      onHandleAuthStateChange,
      onHandleUserInputChange,
      fetchCreateUser,
      onFetchAuthUser,
      onHandleUserProfileInputChange,
      onResetFetch,
      onResetUserData,
    } = this.props;
    return (
      <div style={{width:"100%",height:"100%",display: 'flex',justifyContent: 'center',alignItems: 'center', backgroundColor:"#e0e4e8"}}>
        <div style={{padding:10}}>
          <Form>
            {showLogo && (
              <div className="text-center pb-4">
                <img
                  src={logo200Image}
                  className="rounded"
                  style={{ width: 60, height: 60, cursor: 'pointer' }}
                  alt="logo"
                  onClick={onLogoClick}
                />
              </div>
            )}
              <div>
                <FormGroup className="login-form-style justify-content-center">
                  <div style={{width:'100%'}}>
                    <Col sm={12} className="col-input">
                      <Label className="label" for={usernameLabel}>{usernameLabel}</Label>
                      <Input 
                        {...usernameInputProps} 
                        name="email"
                        value={((user.email) ? user.email : "")} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={12} className="col-input">
                      <Label className="label">{passwordLabel}</Label>
                      <Input 
                        {...passwordInputProps} 
                        name="password"
                        value={((user.password) ? user.password : "")} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                  </div>
                </FormGroup>                    
                <div className="pt-1">
                    <h6>
                        <Link to="/recover-password" onClick={()=> {onResetUserData()}}>
                        ¿Ha olvidado su contraseña?
                        </Link>
                    </h6>
                </div>           
              </div>      
            <hr />
            <Button
              size="lg"
              className="bg-gradient-theme-left border-0"
              block
              onClick={() => {
                !fetch.is_fetching&&
                    validateEmptyFields([user.email,user.password])?
                      onFetchAuthUser(user)
                    :
                      this.clickModal()
                }}>
              {!fetch.is_fetching?"Iniciar sesión":"Procesando..."}
            </Button>
              <div className="text-center pt-1">
                <h6>o</h6>
                <h6>
                    <Link to="/register" onClick={()=> {onResetUserData();onResetFetch()}}>Registrate</Link>
                </h6>
              </div>
              <Modal
                  isOpen={this.state.modal}>
                  <ModalHeader>Debe llenar los campos solicitados</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {this.clickModal()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
               <Modal
                  isOpen={fetch.status==2 || fetch.status==1 || (fetch._fetch==="fetchCreateUser" && fetch.status==1)}>
                  <ModalHeader>{fetch.status==2?"Fallo de autenticación":"¡Éxito!"}</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">{fetch.message?fetch.message:this.state.message&&this.state.message}</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {onResetFetch(); onResetUserData(); this.setState({message:null})}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal> 
          </Form>
        </div>
      </div>
    );
  }
}


AuthForm.propTypes = {
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  showLogo: true,
  usernameLabel: 'Correo',
  usernameInputProps: {
    type: 'email',
    placeholder: 'your@email.com',
  },
  passwordLabel: 'Contraseña',
  passwordInputProps: {
    type: 'password',
    placeholder: 'contraseña',
  },
  confirmPasswordLabel: 'Confirmar contraseña',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirmación de contraseña',
  },
  onLogoClick: () => {},
};

export default AuthForm
