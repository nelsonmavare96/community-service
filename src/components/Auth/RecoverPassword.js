import logo200Image from 'assets/img/logo/logo_200.png';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, Form, FormGroup, Input, Label,Modal, ModalBody, ModalFooter,ModalHeader, Col} from 'reactstrap';
import {Redirect, Link} from 'react-router-dom';

class AuthForm extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      user: {
        email: "",
        password: "",
        confirmPassword: "",
        remember: false
      },
      modal: false,
    }
  }


  clickModal = (e) =>{
    return this.setState({
        modal: !this.state.modal,
    }); 
  }

  userLoggedRedirect = data =>{
    if(data.userLogged){
      if(data.roleUser==1){
        this.props.history.push("/countries")
      }
      else
        this.props.history.push("/home")
    }
  };

  renderButtonText(authState) {
    const { buttonText } = this.props;

    if (!buttonText && authState=="STATE_LOGIN") {
      return 'Iniciar sesión';
    }

    if (!buttonText && authState=="STATE_SIGNUP") {
      return 'Registrarse';
    }

    if (!buttonText && authState=="RECOVER_PASSWORD") {
      return 'Obtener contraseña';
    }

    return buttonText;
  }

  validateFields = (authState) =>{
    const {user} = this.props
    let empty_field = true
    if(authState=="STATE_LOGIN"){
      if(user.email.length!=0 && user.password.length!=0)
       empty_field = false
    }
    else if(authState=="STATE_SIGNUP"){
      if(user.email.length!=0 && user.password.length!=0 && user.passwordConfirm!=0 && user.country_id!="")
        empty_field = false
    }
    else if(authState=="RECOVER_PASSWORD"){
      if(user.email.length!=0){
        empty_field = false
      }
    }
    return empty_field
  }

  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      confirmPasswordLabel,
      confirmPasswordInputProps,
      onLogoClick,

      authState,
      user,
      fetch,
      countries,
      globalData,
      onHandleAuthStateChange,
      onHandleUserInputChange,
      fetchCreateUser,
      onFetchAuthUser,
      onHandleUserProfileInputChange,
      onResetFetch,
      onResetUserData,
    } = this.props;
    return (
      <div style={{width:"100%",height:"100%",display: 'flex',justifyContent: 'center',alignItems: 'center', backgroundColor:"#e0e4e8"}}>
        <div style={{padding:10,width:'50%'}}>
          <Form>
            {(globalData.isLogged) && ( 
              (globalData.roleUser=="1") ?
              <Redirect to="/countries"/>
              :
              <Redirect to="/home"/>
            )
            }
            {showLogo && (
              <div className="text-center pb-4">
                <img
                  src={logo200Image}
                  className="rounded"
                  style={{ width: 60, height: 60, cursor: 'pointer' }}
                  alt="logo"
                  onClick={onLogoClick}
                />
              </div>
            )}
              <div>
                <h6>Se le enviará un mensaje a su correo con una contraseña provisional de acceso, por favor ingrese su correo</h6>
                <FormGroup>
                  <Label for={usernameLabel}>{usernameLabel}</Label>
                  <Input {...usernameInputProps}
                  name="email"
                  defaultValue={((user.email) ? user.email : "")} 
                  onChange={(e) => onHandleUserInputChange(e)} />
                </FormGroup>
              </div>
            <hr />
            <Button
              size="lg"
              className="bg-gradient-theme-left border-0"
              block
              onClick={()=>{}}>
              Obtener contraseña
            </Button>

              <div className="text-center pt-1">
                <h6>o</h6>
                <h6>
                  <Link to="/" onClick={()=> {onHandleAuthStateChange("STATE_LOGIN");onResetUserData()}}>
                    Iniciar sesión
                  </Link>

                </h6>
              </div>
              <Modal
                  isOpen={this.state.modal}>
                  <ModalHeader>Debe llenar los campos solicitados</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {this.clickModal()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
              <Modal
                  isOpen={fetch.result_succeeded == false}>
                  <ModalHeader>Fallo de autenticación</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">{fetch.message}</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {onResetFetch()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
          </Form>
        </div>
      </div>
    );
  }
}

export const STATE_LOGIN = 'STATE_LOGIN';
export const STATE_SIGNUP = 'STATE_SIGNUP';
export const RECOVER_PASSWORD = 'RECOVER_PASSWORD';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP,RECOVER_PASSWORD]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'STATE_LOGIN',
  showLogo: true,
  usernameLabel: 'Correo',
  usernameInputProps: {
    type: 'email',
    placeholder: 'your@email.com',
  },
  passwordLabel: 'Contraseña',
  passwordInputProps: {
    type: 'password',
    placeholder: 'contraseña',
  },
  confirmPasswordLabel: 'Confirmar contraseña',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirmación de contraseña',
  },
  onLogoClick: () => {},
};

export default AuthForm
