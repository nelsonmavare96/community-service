import logo200Image from 'assets/img/logo/logo_200.png';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, Form, FormGroup, Input, Label,Modal, ModalBody, ModalFooter,ModalHeader, Col, Row} from 'reactstrap';
import {Redirect, Link} from 'react-router-dom';
import Select from 'react-select';
import sectors from "../../constants/sector"
import sexs from "../../constants/sex"

class AuthForm extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      user: {
        email: "",
        password: "",
        confirmPassword: "",
        remember: false
      },
      type_person: "",
      modal: false,
      _classifications: this.props.classifications
    }
  }


  clickModal = (e) =>{
    return this.setState({
        modal: !this.state.modal,
    }); 
  }
  filterClasifications(sectorId){
    var clasifications = this.props.classifications

    var filtered = clasifications.filter(c=>{
      
      if(c.sector == sectorId )
          return c
  
  })
  console.log(filtered)
  this.setState({_classifications: filtered})

  }
  handleClassifications(sectorId){
    this.setState({_classifications: this.filter(sectorId)})
    console.log(this.state._classisfications)
}
  
  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      confirmPasswordLabel,
      confirmPasswordInputProps,
      onLogoClick,
      onHandleUserMultiSelect,
      pathologies,

      authState,
      user,
      fetch,
      countries,
      classifications,
      globalData,
      onHandleAuthStateChange,
      onHandleUserInputChange,
      fetchCreateUser,
      onFetchAuthUser,
      onHandleUserProfileInputChange,
      onResetFetch,
      onResetUserData,
    } = this.props;
    function filter(sectorId){
      const {classifications} = this.props;
      this.state._classifications = classifications.map(c=>{
          if(c.sector == sectorId )
              return c
      })
      return this.state._classifications
  }

    if(!fetch.is_fetching && fetch.status==1){
      return(
      <Redirect to="/"/>)
    }
    else
    return (
      <div style={{width:"100%",height:"100%",display: 'flex',justifyContent: 'center',alignItems: 'center', backgroundColor:"#e0e4e8"}}>
        <div style={{padding:10,width:'50%'}}>
          <Form>
            {(globalData.isLogged) && ( 
              (globalData.roleUser=="1") ?
              <Redirect to="/countries"/>
              :
              <Redirect to="/home"/>
            )
            }
            {showLogo && (
              <div className="text-center pb-4">
                <img
                  src={logo200Image}
                  className="rounded"
                  style={{ width: 60, height: 60, cursor: 'pointer' }}
                  alt="logo"
                  onClick={onLogoClick}
                />
              </div>
            )}
              <div>
                <FormGroup row style={{backgroundColor:"white", borderRadius:6, paddingTop:15, paddingBottom:15}}>
                  <Col sm={6}>
                    <Label for={usernameLabel}>{usernameLabel}</Label>
                    <Input 
                      {...usernameInputProps} 
                      name="email"
                      value={((user.email) ? user.email : "")} 
                      onChange={(e) => onHandleUserInputChange(e)}
                    />
                  </Col>
                  <Col sm={6}>
                    <Label for={passwordLabel}>{passwordLabel}</Label>
                    <Input 
                      {...passwordInputProps} 
                      name="password"
                      value={((user.password) ? user.password : "")} 
                      onChange={(e) => onHandleUserInputChange(e)}
                    />
                  </Col>
                  <Col sm={6}>
                    <Label for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
                    <Input 
                      {...confirmPasswordInputProps} 
                      name="passwordConfirm"
                      value={((user.passwordConfirm) ? user.passwordConfirm : "")} 
                      onChange={(e) => onHandleUserInputChange(e)}
                    />
                  </Col>
                  <Col xl={6} lg={12} md={12}>
                    <Label >Tipo de usuario</Label>
                    <Form style={{height:'2.5rem', display:'flex'}} onChange={(e)=>{this.setState({type_person:e.target.id});onHandleUserInputChange(e)}}>
                      <FormGroup check inline>
                        <Label check>
                          <Input type="radio" name="user_type" id="person" value={1}/> Persona
                        </Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Label check>
                          <Input type="radio" name="user_type" id="institution" value={2}/> Institución
                        </Label>
                      </FormGroup>
                    </Form>
                  </Col>
                </FormGroup>
                {this.state.type_person==="person" ? 
                  <FormGroup row style={{backgroundColor:"white", borderRadius:6, paddingTop:15, paddingBottom:15}}>
                    <Col sm={6}>
                      <Label >Razón Social</Label>
                      <Input 
                        type="text" 
                        name="socialReason"
                        placeholder="Razon Social"
                        value={user.socialReason} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label >Nombre</Label>
                      <Input 
                        type="text" 
                        name="name"
                        placeholder="Nombre"
                        value={user.name} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label >Apellido</Label>
                      <Input 
                        type="text" 
                        name="lastname"
                        placeholder="Apellido"
                        value={user.lastname} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label >Teléfono (formato 584120000000)</Label>
                      <Input 
                        type="text" 
                        name="phone"
                        placeholder="Teléfono"
                        value={user.phone} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Fecha de nacimiento</Label>
                      <Input
                        type="date"
                        name="birthdate"
                        id="birthdate"
                        placeholder="Fecha de nacimiento"
                        value={user.birthdate} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Grado de Institución</Label>
                      <Input 
                        name="instruction"
                        placeholder="Grado de Institución"
                        value={user.instruction} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Tipo de trabajo</Label>
                      <Input 
                        type="text"  
                        name="job_type"
                        placeholder="Tipo de trabajo"
                        value={user.job_type} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Nombre del trabajo</Label>
                      <Input 
                        type="text" 
                        name="job"
                        placeholder="Nombre del trabajo"
                        value={user.job} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Estudio actual</Label>
                      <Input 
                        type="text" 
                        name="current_study"
                        placeholder="Estudio actual"
                        value={user.current_study} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Facebook</Label>
                      <Input 
                        type="text" 
                        name="facebook"
                        placeholder="Facebook"
                        value={user.facebook} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Twitter</Label>
                      <Input 
                        type="text" 
                        name="twitter"
                        placeholder="Twitter"
                        value={user.twitter} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Instagram</Label>
                      <Input 
                        type="text" 
                        name="instagram"
                        placeholder="Instagram"
                        value={user.instagram} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Codigo de Referido(Opcional)</Label>
                      <Input 
                        type="text" 
                        name="referralCode"
                        placeholder="Codigo de referido"
                        value={user.referralCode} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Sitio web: www.{user.webSite?user.webSite:"ejemplo"}.com</Label>
                      <Input 
                        type="text" 
                        name="webSite"
                        placeholder="Sitio web"
                        value={user.webSite} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label for={usernameLabel}>Dirección</Label>
                      <Input 
                        type="text" 
                        name="address"
                        placeholder="Direccion"
                        value={user.address} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>País</Label>
                      <Input type="select" name="country" value={user.country} onChange={(e) => onHandleUserInputChange(e)}>
                        <option value="">Seleccione un país</option>
                        {countries && countries.length>0 &&
                          countries.map((country,index)=>(
                          country.status==1&&
                          <option key={index} value={country.name}>{country.name}</option>
                        ))}
                      </Input>
                    </Col>
                    <Col sm={6}>
                      <Label>Patologías</Label>
                      <Select
                        placeholder="Seleccione la(s) patologia(s)"
                        isMulti
                        className="basic-multi-select"
                        classNamePrefix="select"
                        onChange={(array) => onHandleUserMultiSelect(array,'pathologies')}
                        options={pathologies}

                      />
                    </Col>
                    <Col sm={6}>
                        <Label>Sector</Label>
                        <Input type="select" name="sector_id" value={user.sector_id} onChange={(e) => {onHandleUserInputChange(e);  this.filterClasifications(e.target.value)}}>
                        <option value="">Seleccione un sector</option>
                        {sectors && sectors.length>0 &&
                          sectors.map((sector,index)=>(
                          sector.status==1&&
                          <option key={index} value={sector.id}>{sector.name}</option>
                          ))}     
                        </Input>
                      </Col>
                      <Col sm={6}>
                        <Label>Género</Label>
                        <Input type="select" name="sex" value={user.sex} onChange={(e) => onHandleUserInputChange(e)}>
                        <option value="">Seleccione un Género</option>
                        {sexs && sexs.length>0 &&
                          sexs.map((sex,index)=>(
                          sex.status==1&&
                          <option key={index} value={sex.name}>{sex.name}</option>
                          ))}     
                        </Input>
                      </Col>
                      <Col sm={6}>
                        <Label>Clasificaciones</Label>
                        <Input type="select" name="classification_id" value={user.classification_id} onChange={(e) => onHandleUserInputChange(e)}>
                        <option value="">Seleccione una clasificación</option>
                        {this.state._classifications && this.state._classifications.length>0 &&
                          this.state._classifications.map((classification,index)=>(
                            
                            classification.status==1&& classification.userType== 1 && 
                          <option key={index} value={classification.id}>{classification.name}</option>
                          ))}     
                        </Input>
                      </Col>
                      <Col sm={12}>
                        <Label >Los campos donde no aparece "(Opcional)" son requeridos</Label>
                        <Label >La contraseña debe tener minimo 8 caracteres</Label>
                      </Col> 
                  </FormGroup>
                  
                :
                   this.state.type_person==="institution" &&
                    <FormGroup row style={{backgroundColor:"white", borderRadius:6, paddingTop:15, paddingBottom:15}}>
                      <Col sm={6}>
                      <Label >Razón Social</Label>
                      <Input 
                        type="text" 
                        name="socialReason"
                        placeholder="Razon Social"
                        value={user.socialReason} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label >Nombre</Label>
                      <Input 
                        type="text" 
                        name="name"
                        placeholder="Nombre"
                        value={user.name} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label >Teléfono</Label>
                      <Input 
                        type="text" 
                        name="phone"
                        placeholder="Teléfono"
                        value={user.phone} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                      <Col sm={6}>
                        <Label >Ubicación</Label>
                        <Input 
                          type="text" 
                          name="address"
                          placeholder="Ubicación"
                          value={user.address} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Sitio web</Label>
                        <Input
                          type="text"
                          name="webSite"
                          id="webSite"
                          placeholder="Sitio web"
                          value={user.webSite} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label >Facebook</Label>
                        <Input 
                          name="facebook"
                          value={user.facebook} 
                          placeholder="Facebook"
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label >Twitter</Label>
                        <Input 
                          type="text"  
                          name="twitter"
                          placeholder="Twitter"
                          value={user.twitter} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label >Instagram</Label>
                        <Input 
                          type="text" 
                          name="instagram"
                          placeholder="Instagram"
                          value={user.instagram} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Sector</Label>
                        <Input type="select" name="sector_id" value={user.sector_id} onChange={(e) => {onHandleUserInputChange(e); this.filterClasifications(e.target.value)}}>
                        <option value="">Seleccione un sector</option>
                        {sectors && sectors.length>0 &&
                          sectors.map((sector,index)=>(
                          sector.status==1&&
                          <option key={index} value={sector.id}>{sector.name}</option>
                          ))}     
                        </Input>
                      </Col>
                      
                      <Col sm={6}>
                        <Label>Clasificaciones</Label>
                        <Input type="select" name="classification_id" value={user.classification_id} onChange={(e) => onHandleUserInputChange(e)}>
                        <option value="">Seleccione una clasificación</option>
                        {this.state._classifications && this.state._classifications.length>0 &&
                          this.state._classifications.map((classification,index)=>(
                            
                            classification.status==1&& classification.userType== 2 && 
                          <option key={index} value={classification.id}>{classification.name}</option>
                          ))}     
                        </Input>
                      </Col> 
                      <Col sm={6}>
                      <Label>País</Label>
                      <Input type="select" name="country" value={user.country} onChange={(e) => onHandleUserInputChange(e)}>
                        <option value="">Seleccione un país</option>
                        {countries && countries.length>0 &&
                          countries.map((country,index)=>(
                          country.status==1&&
                          <option key={index} value={country.name}>{country.name}</option>
                        ))}
                      </Input>
                      </Col>
                      <Col sm={6}>
                      <Label for={usernameLabel}>Codigo de Referido(Opcional)</Label>
                      <Input 
                        type="text" 
                        name="referralCode"
                        placeholder="Codigo de referido"
                        value={user.referralCode} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                      <Col sm={12}>
                        <Label >Los campos donde no aparece "(Opcional)" son requeridos</Label>
                        <Label >La contraseña debe tener minimo 8 caracteres</Label>
                        
                      </Col>
                      
                    </FormGroup>
                  
                }
              </div>         
            <hr />
            <Button
              size="lg"
              className="bg-gradient-theme-left border-0"
              block
              onClick={() => !fetch.is_fetching&&fetchCreateUser(user) }>
              {!fetch.is_fetching?"Registrarse":"Procesando..."}
            </Button>
              <div className="text-center pt-1">
                <h6>o</h6>
                <h6>
                  <Link to="/" onClick={()=> {onResetUserData();onResetFetch()}}>
                    Iniciar sesión
                  </Link>
                </h6>
              </div>
              <Modal
                  isOpen={this.state.modal}>
                  <ModalHeader>Debe llenar los campos solicitados</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {this.clickModal()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
              <Modal
                  isOpen={fetch.status==2}>
                  <ModalHeader>Fallo de registro</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">{fetch.message}</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {onResetFetch()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
          </Form>
        </div>
      </div>
    );
  }
}

export const STATE_LOGIN = 'STATE_LOGIN';
export const STATE_SIGNUP = 'STATE_SIGNUP';
export const RECOVER_PASSWORD = 'RECOVER_PASSWORD';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP,RECOVER_PASSWORD]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'STATE_LOGIN',
  showLogo: true,
  usernameLabel: 'Correo',
  usernameInputProps: {
    type: 'email',
    placeholder: 'your@email.com',
  },
  passwordLabel: 'Contraseña',
  passwordInputProps: {
    type: 'password',
    placeholder: 'contraseña',
  },
  confirmPasswordLabel: 'Confirmar contraseña',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirmación de contraseña',
  },
  onLogoClick: () => {},
};

export default AuthForm
