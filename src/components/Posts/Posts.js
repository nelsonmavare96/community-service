import Page from '../../components/Page';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import React from 'react';
import { Link, withRouter } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  Col,
  Row,
  Badge,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  CardTitle,
  Input,
  Label,
} from 'reactstrap';
import Avatar from 'components/Avatar';

class Posts extends React.Component {
    state = {
        modal: false,
    };
    componentWillMount(){
        const {fetchPosts} = this.props
        const {pathname} = this.props.location;
        console.log(pathname)
		pathname=="/posts"?
            fetchPosts("/posts")
        :
            fetchPosts("/posts/myReferral")
	}
    render(){
        const {
            userData,
            posts,
            onShowPost,
            onShowPublicUserProfile
            } = this.props;
            
        return (
            <Page className="custom-container">
                <Row className="justify-content-center" style={{paddingTop:10}}>
                    {userData.user_type== 3 &&
                        <Col md={12}>
                            <Link to="newPost">
                                <Button color="primary" onClick={() => {}} >
                                    Agregar nueva publicación
                                </Button>
                            </Link>
                        </Col>
                    }
                    {(fetch.is_fetching && fetch._fetch==="fetchPosts") ?
                        <Col className="justify-content-center" md={8}>
                            <label >Cargando...</label>
                        </Col>
                    :
                        <Col className="justify-content-center" md={8}>
                            {(posts && posts.length>0 ) ?
                                <div>
                                    {posts.map((post, index) => (
                                        <div key={index}>
                                            {post &&
                                                <Card inverse style={{marginBottom:20}}>
                                                    <CardBody className="d-flex justify-content-center align-items-center flex-column">
                                                        {/* <Link to='publicUserProfile' style={{width:'100%'}}>
                                                            <div className="user-header" style={{zIndex: 10,top: 0}} onClick={()=>onShowPublicUserProfile(post.user)}>
                                                                <li className="nav-item" style={{listStyle: "none"}}>
                                                                    <div id="Popover2" className="nav-link">
                                                                        <img src={(post.user && post.user && post.image)? process.env.REACT_APP_URL_SERVER + 'uploads/users/' + post.user.image : process.env.REACT_APP_URL_SERVER + 'assets/user_default.png' + post.user.image} className="rounded-circle can-click flipthis-highlight user-image-cardsells"/>
                                                                        <label style={{marginLeft: "2%", color: "white", marginTop:"2%"}}>
                                                                        {(post.user.name!="" && post.user.lastname!="") ? <>{post.user.name} {post.user.lastname}</> : post.user.email}
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                            </div>
                                                        </Link>  */}
                                                        <Row className="m-0 row-standard-card-text" >
                                                            <CardText>
                                                                <b>{post.Title}</b>
                                                            </CardText>
                                                        </Row>
                                                        <Row className="m-0 row-standard-card-text" >
                                                            <CardText style={{textAlign:'justify'}}>
                                                                <small>{post.Content.slice(0,230)}{post.Content.legth>230?"...":""}</small>
                                                            </CardText>
                                                        </Row>
                                                        <Row className="m-rl-0" style={{height:35,marginTop:15,marginBottom: 13}}>
                                                            <Link to="/detail-post">
                                                                <Button onClick={()=>{onShowPost(post)}} title="Ver detalles" className="btn btn-success button-succes-custom btn-show"><FontAwesomeIcon icon={faEye}/></Button>
                                                            </Link>
                                                        </Row>
                                                    </CardBody>
                                                </Card>
                                            }
                                        </div>
                                    ))}
                                </div>
                                :
                                    <label >{fetch.status===2?"No se pudieron cargar las publicaciones":"No hay publicaciones para mostrar"} </label>
                            }
                        </Col>
                    }
                    
                </Row>
                {/* <Modal
                    isOpen={this.state.modal}>
                    <ModalHeader>Compartir publicación</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="email">Correo</Label>{' '}
                            <Input
                                type="email"
                                name="email"
                                placeholder="ejemplo@gmail.com"
                            />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=> this.clickModal()}>
                        Enviar
                        </Button>{' '}
                        <Button
                        color="secondary" onClick={()=> this.clickModal()}>
                        Cancelar
                        </Button>
                    </ModalFooter>
                </Modal>  */}  
            </Page>
        );
    }
}

export default withRouter(Posts);
