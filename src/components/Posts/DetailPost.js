import React from 'react'
import {
	Card, 
	CardHeader, 
	CardBody, 
	CardTitle, 
	Row, 
	Col, 
	Label, 
	Form, 
	FormGroup, 
	FormText, 
	Input, 
	Button,
} from 'reactstrap' 
import { Link } from 'react-router-dom'
import moment from 'moment'

 
class DetailPost extends React.Component {
 
	constructor(props){ 
		super(props)
	}

	render(){
		const{post, onDownloadFile} = this.props
		return(	
			<Card className="new-post-card custom-container">
				<CardBody> 
					<Row>
						<Col xl={12} lg={12} md={12}>
							<Card> 
								<CardHeader style={{textTransform: "none"}}>Publicado el: {moment(post.DateUploaded).format('DD/MM/YYYY hh:mm a')}</CardHeader>
								<CardBody>
									<Form>
										<FormGroup row>
											<Label for="exampleText" sm={3}>
												Título:
											</Label>
											<Col sm={9}>
												<Input 
													name="title"
													onChange={()=>{}}
                                                    value={post.Title}
                                                    disabled
                                                    style={{backgroundColor:'white'}} 
												/>
											</Col>
										</FormGroup>
										{/* <FormGroup row>
											<Label for="exampleText" sm={3}>
												Autor:
											</Label>
											<Col sm={9}>
												<Input 
													name="author"
													onChange={()=>{}}
                                                    value={post.Title+" "+post.Title}
                                                    disabled
                                                    style={{backgroundColor:'white'}} 
												/>
											</Col>
										</FormGroup> */}
										<FormGroup row>
											<Label for="exampleText" sm={3}>
												Contenido:
											</Label>
											<Col sm={9}>
												<Input 
													type="textarea" 
													name="description"
													rows= {6}
													onChange={()=>{}} 
                                                    value={post.Content}
                                                    style={{backgroundColor:'white'}} 
                                                    disabled
												/>
											</Col>
										</FormGroup>
										<FormGroup row>
											<div style={{width: "100%", marginLeft: 19, background: "#dfdfdf", padding: 7, marginRight: 15, borderRadius: 4}}>
												{
													(post && post.FileName && post.FileName!="")?
                                                        <span>	
                                                            <label> {post.FileName} </label>
                                                        </span>	
													:
													<Label for="no_files">
														No hay archivos para mostrar
													</Label>
												}
												<a href={process.env.REACT_APP_URL_SERVER+"/posts/upload/"+post.FileName+"/download"} target="_blank" download>
													<Button color="info" style={{marginLeft:15}} onClick={()=>{}}>Descargar archivo</Button>
												</a>
											</div>
										</FormGroup>
										<FormGroup check row>
											<Col sm={{ size: 10, offset: 2 }}>
                                                <Link to="/posts">
                                                    <div>
                                                        <Button color="secondary" style={{float: "right"}} onClick={()=>{}}>Regresar</Button>
                                                    </div>
                                                </Link>
											</Col>
										</FormGroup>
									</Form>
								</CardBody>
							</Card>
						</Col>
					</Row>
				</CardBody>
			</Card>
			
		);
	}
}

export default DetailPost; 