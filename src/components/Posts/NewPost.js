import React from 'react'
import {
	Card, 
	CardHeader, 
	CardBody, 
	CardTitle, 
	Row, 
	Col, 
	Label, 
	Form, 
	FormGroup, 
	FormText, 
	Input, 
	Button,
	InputGroup,
	InputGroupAddon,
	Modal, ModalBody, ModalFooter,ModalHeader 
} from 'reactstrap' 
import { Link, Redirect } from 'react-router-dom'
 
class NewPost extends React.Component {
 
	constructor(props){ 
		super(props)
		this.state = {
			modal: false,
			savePostModal: false
		}
	}

	clickModal = (e) =>{
		return this.setState({
				modal: !this.state.modal,
		}); 
	}
	
	clickSavePostModal = (e) =>{
		return this.setState({
				savePostModal: !this.state.savePostModal,
		}); 
	}

	validateFields(post){	
		if(post.title && post.description && post.files && post.files.length>0)
			return this.setState({savePostModal: !this.state.savePostModal});
		return this.setState({modal: !this.state.modal}); 
	}

 
	render(){
		const{categories,onPostInputChange,onSaveNewPost, onDeletePostPicture, onCancelPostPost, restorePostValues,post, fetch} = this.props;
		if(!fetch.is_fetching && fetch.status==1 && fetch._fetch=="fetchSaveNewPost"){
			return(
				<Redirect to="/posts"/>)
		}
		return(	
			<Card className="new-post-card custom-container">
				<CardBody> 
					<Row>
						<Col xl={12} lg={12} md={12}>
							<Card> 
								<CardHeader style={{textTransform: "none"}}>Nueva publicación</CardHeader>
								<CardBody>
									<Form>
										<FormGroup row>
											<Label for="exampleText" sm={3}>
												Título:
											</Label>
											<Col sm={9}>
												<Input 
													name="title"
													onChange={onPostInputChange}
													value={post.title} 
												/>
											</Col>
										</FormGroup>
													{/* <FormGroup row>
														<Label for="category" sm={2}>
															Categoría
														</Label>
														<Col sm={10}>
															<Input type="select" name="category_id" value={(post && post.category_id)?  post.category_id : "-1"} onChange={onPostInputChange}>
																<option value="-1">Seleccione una categoría</option>
																{categories && categories.length>0 && categories.map((category,index)=>(
																	<option key={index} value={category.id}>{category.name}</option>
																))}
																
															</Input>
														</Col>
													</FormGroup>
													<Row className="m-0" >
															<Label for="exampleText">Condiciones:</Label>
															<FormGroup style={{width:'100%'}}>
																	<Row className="m-rl-0" style={{marginTop:10}}>
																			<Input 
																				type="textarea" 
																				name="conditions"
																				onChange={onPostInputChange}
																				value={post.conditions}
																			/>
																	</Row>
															</FormGroup>
													</Row> */}
										<FormGroup row>
											<Label for="exampleText" sm={3}>
												Contenido:
											</Label>
											<Col sm={9}>
												<Input 
													type="textarea" 
													name="description"
													rows= {6}
													onChange={onPostInputChange} 
													value={post.description}
												/>
											</Col>
										</FormGroup>
										<FormGroup row>
											<Label for="files" sm={4}>
												Selecciona el archivo
											</Label>
											<Col sm={8}>
												<Input type="file" name="files" style={{color: "transparent"}} onChange={onPostInputChange}  />
												{
													(post && post.files && post.files.length > 0)?
													<label>
														{"Se han seleccionado " + post.files.length + " archivo"}
													</label>
													:
													""
												}
											</Col>
										</FormGroup>
										<FormGroup row>
											<div style={{width: "100%", marginLeft: 19, background: "#dfdfdf", padding: 7, marginRight: 15, borderRadius: 4}}>
												{
													(post && post.files && post.files.length > 0)?
														post.files.map((file, index) => (
															<span key={index}>	
																<label className="justify-content-center" style={{height:25, width:25, background:'red', display:'inline-flex', marginRight:5, borderRadius:25, fontWeight:'bold'}} onClick={() => onDeletePostPicture(index)}> x </label>
																<label> {file.name} </label>
															</span>
														))
													:
													<Label for="no_files">
														No se ha seleccionado un archivo aún.
													</Label>
												}
											</div>
										</FormGroup>
										<FormGroup check row>
											<Col sm={{ size: 10, offset: 2 }}>
												<Link to="/posts">
													<div>
														<Button color="secondary" style={{float: "right"}} onClick={()=> onCancelPostPost()}>Cancelar</Button>
													</div>
												</Link>
												<div>
													{ fetch.is_fetching && fetch._fetch==="fetchSaveNewPost" ?
														<Button className="btn btn-success" color="success" onClick={()=>{}} style={{float: "right", marginRight: "21px"}}>Procesando...</Button>	
													:
														<Button className="btn btn-success" color="success" onClick={()=>{this.validateFields(post)}} style={{float: "right", marginRight: "21px"}}>Publicar</Button>
													}
													
												</div>
											</Col>
										</FormGroup>
									</Form>
								</CardBody>
									<Modal
										isOpen={this.state.modal}>
										<ModalHeader>Debe llenar los campos solicitados</ModalHeader>
										<ModalBody>
												<FormGroup>
														<Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
												</FormGroup>
										</ModalBody>
										<ModalFooter>
												<Button color="primary" onClick={()=> {this.clickModal()}}>
														Aceptar
												</Button>
												
										</ModalFooter>
									</Modal>
									<Modal
										isOpen={this.state.savePostModal}>
										<ModalHeader>Está seguro que desea publicar este post?</ModalHeader>
										<ModalBody>
												<FormGroup>
														<Label for="email">Se publicará la información suministrada como nuevo post</Label>{' '}
												</FormGroup>
										</ModalBody>
										<ModalFooter>
												<Button color="primary" onClick={()=> {this.clickSavePostModal();onSaveNewPost(post);}}>
														Aceptar
												</Button>
												<Button color="danger" onClick={()=> {this.clickSavePostModal();}}>
														Cancelar
												</Button>
												
										</ModalFooter>
									</Modal>
							</Card>
						</Col>
					</Row>
				</CardBody>
			</Card>
			
		);
	}
}

export default NewPost; 