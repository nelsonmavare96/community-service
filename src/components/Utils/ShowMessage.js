import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ShowMessage extends React.Component {
	
	constructor(props) {
    super(props);
  }

	render(){
		const{message, messageType, modal} = this.props

		return(
      <Modal isOpen={modal}>
        <div>
            <div className="modal-content">
                <div className="modal-header">
                    <h4 className="modal-title"><i className="fas fa-users"></i> {(messageType && messageType === 'success')? "Acción exitosa" : "Hubo un error" } </h4>
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true" data-dismiss="modal">×</button>
                </div>
                <div className="modal-body">
                    <form>
                        <div className="form-group text-center">
                            <label htmlFor="recipient-name" className="control-label">{message}</label>
                            
                        </div>
                    </form>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary waves-effect" data-dismiss="modal">Cancelar</button>
										<button type="button" className="btn btn-primary waves-effect waves-light" data-dismiss="modal">Está bien</button>
								</div>
            </div>
        </div>        
  		</Modal>
		)
	}
}

export default ShowMessage;