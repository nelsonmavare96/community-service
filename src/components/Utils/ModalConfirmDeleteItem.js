import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ModalConfirmDeleteItem extends React.Component {
	
	constructor(props) {
    super(props);
  }


	render(){
		const{item, onCloseModal, onSubmit, modal, onChangeView} = this.props

		return(
			<>
        <Modal isOpen={modal}>
          <div>
	            <div className="modal-content">
	                <div className="modal-header">
	                    <h4 className="modal-title"><i className="fas fa-users"></i> Eliminar elemento</h4>
	                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true" onClick={() => onCloseModal() }>×</button>
	                </div>
	                <div className="modal-body">
	                    <form>
	                        <div className="form-group text-center">
	                            <label htmlFor="recipient-name" className="control-label">¿Confirmas eliminar el elemento?</label>
	                            
	                        </div>
	                    </form>
	                </div>
	                <div className="modal-footer">
	                    <button type="button" className="btn btn-secondary waves-effect" data-dismiss="modal" onClick={() => onCloseModal() }>Cancelar</button>
						{onChangeView ?
	                    <button type="button" className="btn btn-danger waves-effect waves-light" data-dismiss="modal" onClick={() => {onSubmit(item.id);onChangeView("list");onCloseModal()} }>Confirmar</button>
						:
						<button type="button" className="btn btn-danger waves-effect waves-light" data-dismiss="modal" onClick={() => {onSubmit(item.id);onCloseModal()} }>Confirmar</button>
						}
					</div>
	            </div>
	        </div>        
    		</Modal>
      </>
		)
	}
}

export default ModalConfirmDeleteItem;