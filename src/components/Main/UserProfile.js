import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Col,
  Card,
  CardHeader,
  CardBody,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
	Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Nav,
  NavLink
} from 'reactstrap';
import {transformUserType} from '../../functions'

class UserProfile extends React.Component {

	constructor(props){
		super(props)
		this.toggle = this.toggle.bind(this);
		this.state = {
			modal: false,
			modal_validation: false,
			modal_validation_fields: false,
			toggleMenu: {
				changePassword: false,
				updateProfile: true
			}
		}
	}

	clickModal = (e) =>{
		return this.setState({
				modal: !this.state.modal,
		}); 
}

clickModalValidation = (e) =>{
	return this.setState({
			modal_validation: !this.state.modal_validation,
	}); 
}

clickModalValidationFields = (e) =>{
	return this.setState({
			modal_validation_fields: !this.state.modal_validation_fields,
	}); 
}

	toggle() {
	    this.setState(prevState => ({
	      dropdownOpen: !prevState.dropdownOpen
	    }));
	 }

	onHandleChangePage(event){
		let toggleMenu = {}
		let user = Object.assign({}, this.state.user,{
				newPassword: "",
				passwordConfirmation: ""
			})
		if(event.target.dataset.value === "Cambiar contraseña"){
			toggleMenu = Object.assign({}, this.state.toggleMenu, {
				updateProfile: false,
				changePassword: true
			})
		}else{
			toggleMenu = Object.assign({}, this.state.toggleMenu, {
				updateProfile: true,
				changePassword: false
			})
		}
		this.setState({toggleMenu, user})
	}

	componentWillMount(){
		const {onCancelEditProfile} = this.props
		onCancelEditProfile();
	}


	render(){
		const {user, 
			fetch,
			onHandleProfileImageChagne, 
			countries,
			change_password,
			onHandleUserProfileInputChange,
			onChangePassword,
			resetChancePassword, 
			onHandleUserInputChange,
			onCancelEditProfile,
			onSaveUserProfile,
			onHandleChangePasswordInputChange,
			onResetFetch
		} = this.props
		return(
			<div className="custom-container-without-space">
				<div className="d-flex justify-content-center align-items-center flex-column card-body" style={{background: "#e6ebec"}}>
					<label htmlFor="imageFile" style={{marginBottom: "0"}}>
						<img src={user.image} className="rounded-circle mb-2" style={{width: "125px", height: "125px"}} />
					</label>
					<div>
						<input type="file" name="imageFile" id="imageFile" className="change-image" data-multiple-caption="{count} files selected" onChange={onHandleProfileImageChagne}/>
						<label htmlFor="imageFile">
							<span className="label-change-image">Cambiar la foto de perfil</span>
						</label>
					</div>
					<div className="card-title" style={{fontSize: "larger"}}>{user.name + " " + user.lastname}</div>
					<div className="card-subtitle" style={{fontSize: "small", fontWeight: "bold"}}>{user.email}</div>
					<p className="card-text"><small>{ transformUserType(user.user_type)}</small></p>
				</div>
				<Card>
		            <CardHeader style={{textAlign: "center", fontWeight: "bold"}}>
		             <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} className="dropcustom">
				        <DropdownToggle className="toggle-custom toggle-hover" caret>
				          {(this.state.toggleMenu.updateProfile)? "Actualizar Datos de perfil" : "Cambiar contraseña"}
				        </DropdownToggle>
				        <DropdownMenu className="menu-custom">
				          <DropdownItem data-value={(this.state.toggleMenu.updateProfile)? "Cambiar contraseña" : "Actualizar Datos de perfil"} className={(this.state.toggleMenu.updateProfile)? "dropdown-item-custom" : "dropdown-item-custom-changed"} onClick={(e) => {this.onHandleChangePage(e);resetChancePassword()}}>{(this.state.toggleMenu.updateProfile)? "Cambiar contraseña" : "Actualizar Datos de perfil"}</DropdownItem>
				        </DropdownMenu>
				      </Dropdown>
		           	</CardHeader>
		            <CardBody>
		            {
		             (this.state.toggleMenu.updateProfile)?
		             <div>
			              <Form>
			                <FormGroup row>
								<Col sm={6} className="col-input">
									<Label htmlFor="name">Nombre</Label>
									<Input
										type="text"
										name="name"
										value={user.name?user.name:""}
										onChange={(e)=> onHandleUserProfileInputChange(e)}
										placeholder="Introduce un nombre"
									/>
								</Col>
								{/* {user.user_type==1 &&
									<Col sm={6} className="col-input">
										<Label htmlFor="lastname">Apellido </Label>
										<Input
											type="text"
											name="lastname"
											value={user.lastname?user.lastname:""}
											onChange={(e)=> onHandleUserProfileInputChange(e)}
											placeholder="Apellido"
										/>
									</Col>
								} */}
								<Col sm={6} className="col-input">
									<Label htmlFor="name">Razón social</Label>
									<Input
										type="text"
										name="social_reason"
										value={user.social_reason?user.social_reason:""}
										onChange={(e)=> onHandleUserProfileInputChange(e)}
										placeholder="Introduce la razón social"
									/>
								</Col>
								<Col sm={6} className="col-input">
									<Label htmlFor="phone">Teléfono</Label>
									<Input
										type="text"
										name="telephone"
										value={user.telephone?user.telephone:""}
										onChange={(e)=> onHandleUserProfileInputChange(e)}
										placeholder="Introduce el número de teléfono"
									/>
								</Col>
								
								<Col sm={6} className="col-input">
									<Label htmlFor="whatsapp">Enlace WhatsApp</Label>
									<Nav>
										<NavLink target="_black" href={"https://api.whatsapp.com/send?phone="+user.telephone}>{"https://api.whatsapp.com/send?phone="+user.telephone}</NavLink>
									</Nav>
								</Col>
								{/* <Col sm={6} className="col-input">
									<Label>País</Label>
									<Input type="select" name="country_id" value={user.country_id} onChange={(e) => onHandleUserProfileInputChange(e)}>
										<option value="">Seleccione una país</option>
										{countries.map((country,index)=>(
											<option key={index} value={country.id}>{country.name}</option>
										))}
									</Input>
								</Col> */}
								</FormGroup>
								{/* {user.user_type==1?
									<FormGroup row>
										<Col sm={6} className="col-input">
											<Label htmlFor="birthdate">Fecha de nacimiento</Label>
											<Input
												type="date"
												name="birthdate"
												format="dd-mm-yyyy"
												value={user.birthdate?user.birthdate:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Fecha de nacimiento"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Grado de Institución</Label>
											<Input
												type="text"
												name="instruction_degree"
												value={user.instruction_degree?user.instruction_degree:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el grado de institución"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Nombre del trabajo</Label>
											<Input
												type="text"
												name="job"
												value={user.job?user.job:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el nombre del trabajo"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Tipo de trabajo</Label>
											<Input
												type="text"
												name="job_type"
												value={user.job_type?user.job_type:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el tipo de trabajo"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Estudio actual</Label>
											<Input
												type="text"
												name="current_study"
												value={user.current_study?user.current_study:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el estudio actual"
											/>
										</Col>
									</FormGroup>	
								:
									user.user_type==2&&
									<FormGroup row>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Ubicación</Label>
											<Input
												type="text"
												name="address"
												value={user.address?user.address:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el sitio web"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Sitio web</Label>
											<Input
												type="text"
												name="website"
												value={user.website?user.website:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el sitio web"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Facebook</Label>
											<Input
												type="text"
												name="facebook"
												value={user.facebook?user.facebook:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el sitio web"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Twitter</Label>
											<Input
												type="text"
												name="twitter"
												value={user.twitter?user.twitter:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el sitio web"
											/>
										</Col>
										<Col sm={6} className="col-input">
											<Label htmlFor="name">Instagram</Label>
											<Input
												type="text"
												name="instagram"
												value={user.instagram?user.instagram:""}
												onChange={(e)=> onHandleUserProfileInputChange(e)}
												placeholder="Introduce el sitio web"
											/>
										</Col>
									</FormGroup>
								} */}
			              </Form>
			              <div>
			              	<Button color="danger" style={{float: "right"}} onClick={()=>onCancelEditProfile()}>Cancelar</Button>
			              </div>
			              <div>
			              	<Button className="btn btn-success " style={{float: "right", marginRight: "21px"}} onClick={()=> {onSaveUserProfile(user);this.clickModal()}}>Guardar</Button>
			              </div>
		             	</div>
		              :
		              <div>
		              	<Form>
			              <FormGroup>
			                  <Label htmlFor="current_password">Contraseña actual</Label>
			                  <Input
			                  	type="password"
			                  	name="current_password"
			                  	value={change_password.current_password}
			                  	onChange={(e)=> onHandleChangePasswordInputChange(e)}
			                  	placeholder="Introduce tu contraseña"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="new_password">Nueva contraseña </Label>
			                  <Input
			                    type="password"
			                    name="new_password"
			                    value={change_password.new_password}
			                  	onChange={(e)=> onHandleChangePasswordInputChange(e)}
			                    placeholder="Ingresa la nueva contraseña"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="new_password_confirm">Confirmación de contraseña</Label>
			                  <Input
			                    type="password"
			                    name="new_password_confirm"
			                    value={change_password.new_password_confirm}
			                  	onChange={(e)=> onHandleChangePasswordInputChange(e)}
			                    placeholder="Ingresa tu confirmación de contraseña"
			                  />
			                </FormGroup>
		                </Form>
		                <div>
			              <Button color="danger" style={{float: "right"}} onClick={()=>console.log("cancelar cambio de password")}>Cancelar</Button>
			            </div>
									{(change_password.current_password!="" && change_password.new_password!="" && change_password.new_password_confirm!="" && change_password.new_password==change_password.new_password_confirm) ?
										<div>
											<Button className="btn btn-success" style={{float: "right", marginRight: "21px"}} onClick={()=>{this.clickModalValidation()}}>Guardar</Button>
										</div>
									:
										<div>
											<Button className="btn btn-success" style={{float: "right", marginRight: "21px"}} onClick={()=>{this.clickModalValidationFields()}}>Guardar</Button>
										</div>
									}
			           </div>
		            }
		            </CardBody>
          		</Card>
							<Modal
									isOpen={(fetch.status==1 || fetch.status==2)&& fetch._fetch=="fetchSaveUserProfile"}>
									<ModalHeader>Perfil guardado con éxito!</ModalHeader>
									<ModalBody>
											<FormGroup>
													<Label for="email">{fetch.status==1?"Los datos de su perfil han sido actualizados con éxito":
													"Los datos de su perfil no han podido ser actualizados"
												}</Label>{' '}
											</FormGroup>
									</ModalBody>
									<ModalFooter>
										<Button color="primary" onClick={()=> {this.clickModal();onResetFetch();(fetch.status==2)&&onCancelEditProfile()}}>
												OK
										</Button>
									</ModalFooter>
							</Modal>


							<Modal
									isOpen={this.state.modal_validation_fields}>
									{(change_password.current_password=="" || change_password.new_password=="" || change_password.new_password_confirm=="") ?
										<>
										<ModalHeader>Campos faltantes</ModalHeader>
											<ModalBody>
													<FormGroup>
															<Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
													</FormGroup>
											</ModalBody>
											<ModalFooter>
															<Button color="primary" onClick={()=> {this.clickModalValidationFields()}}>
																	Aceptar
															</Button>                  
											</ModalFooter>
										</>
									:
										<>
											<ModalHeader>Los campos no coinciden</ModalHeader>
											<ModalBody>
													<FormGroup>
															<Label for="email">La nueva contraseña debe coincidir con la confirmación</Label>{' '}
													</FormGroup>
											</ModalBody>
											<ModalFooter>
															<Button color="primary" onClick={()=> {this.clickModalValidationFields()}}>
																	Aceptar
															</Button>                     
											</ModalFooter>
										</>				
									}
							</Modal>


							<Modal
									isOpen={this.state.modal_validation}>
											<ModalHeader>La contraseña será cambiada</ModalHeader>
									<ModalBody>
											<FormGroup>
													<Label for="email">¿Está seguro que desea realizar esta acción?</Label>{' '}
											</FormGroup>
									</ModalBody>
									<ModalFooter>
													<Button color="primary" onClick={()=> {this.clickModalValidation();onChangePassword(change_password)}}>
															Aceptar
													</Button> 
													<Button color="danger" onClick={()=> {this.clickModalValidation();resetChancePassword()}}>
															Cancelar
													</Button>                      
									</ModalFooter>
							</Modal>

			</div>
		);
	}
}

export default UserProfile;