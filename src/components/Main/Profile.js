import bg1Image from 'assets/img/users/user_example.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import yellowStar from 'assets/img/star_checked.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  NavLink as BtnNavLink
} from 'reactstrap';
import Avatar from 'components/Avatar';

class Profile extends React.Component {

	constructor(props){
		super(props)
  }

  render(){
    const {user,my_posts,onShowPost} = this.props
    return (
      <Page className="custom-container">

        <Row>
          <Col md={5}>
            <Card inverse className='bg-gradient-theme'>
              <CardBody className="d-flex justify-content-center align-items-center flex-column">
                <img style={{height: 150, width: 150}} src={(user && user && user.image)? process.env.REACT_APP_URL_SERVER + '/uploads/users/' + user.image : process.env.REACT_APP_URL_SERVER + '/assets/user_default.png' + user.image} size={80} className="mb-2" />
                <CardTitle>{user.name} {user.lastname}</CardTitle>
                <Row>
                  <Col className="col-12" sm="3" md="3" lg="3" xl="3">
                    <BtnNavLink
                      tag={NavLink}
                      to={'/comments'}
                      activeClassName="active"
                      exact={true}
                      style={{padding:0}}
                    >
                      <Row className="m-0" style={{display: 'flex', justifyContent: 'center'}}>
                        <img src={yellowStar} style={{height: 40, width: 40}} className="mb-2 not-rounded" />
                      </Row>
                      <Row className="m-0 justify-content-center">
                        <CardText style={{color:'white'}}>
                        {(user.averageScore && user.averageScore>0) ?
                          <div>
                            {user.averageScore} / 5
                          </div>
                        :
                          "0 / 5"
                        }
                          
                        </CardText>
                      </Row>
                    </BtnNavLink>
                  </Col>
                  <Col className="col-12" sm="9" md="9" lg="9" xl="9">
                    <CardText>
                      {(user.name && user.lastname) && <><small>{user.name} {user.lastname}</small><br/></>}
                      <small>Email: {user.email}</small> <br/>
                      
                      {(user.facebook) && <><small>Facebook: {user.facebook}</small><br/></>}
                      {(user.twitter) && <><small>Twitter: {user.twitter}</small><br/></>}
                      {(user.instagram) && <><small>Instagram: {user.instagram}</small><br/></>}
                    </CardText>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          </Row>
          <Row className="m-0">
            {(my_posts && my_posts.length>0) ?
              my_posts.map((post,index)=>(
                <Col key={index} style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="3" lg="2" xl="2">
                  <Card>
                    <Link to="/detail-publication">
                      <div className="position-relative" onClick={()=>{onShowPost(post)}}>
                        <CardImg src={"http://localhost:3000/uploads/posts/"+post.pictures[0]} />
                      </div>
                    </Link>
                  </Card>
                </Col>
              ))                        
            :
              <Col md={8} sm={6} xs={12} className="mb-3">
                <div style={{padding: 10, height: 42, background: "antiquewhite"}}>
                  <label style={{float: "center",fontSize:14,width:'100%',textAlign:'center'}}>No posee posts </label>
                </div>
              </Col>
            }
          </Row>      
        
      </Page>
    );
  }
}

export default Profile;
