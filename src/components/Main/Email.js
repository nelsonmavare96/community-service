import React from 'react'
import { Table } from 'reactstrap';
import {
	Row,
	Col,
	Input,
  Card,
  Button,
  CardHeader,
  CardBody,
  Pagination, PaginationItem, PaginationLink
} from 'reactstrap';
import { Link } from 'react-router-dom';

class EmailList extends React.Component {
	constructor(props){
		super(props)
		this.pageSize = 10;
		this.state = {
			currentPage: 0,
			subject_filter:"",
			filtered_emails:[],
			pagesCount:0
		  };

	}

	handleClick(e, index) {
    
		this.setState({
		  currentPage: index
		});
		
	  }
	
	pagesCount(emails){
		return Math.ceil(emails.length / this.pageSize);
	}

	onSubjectFilter = (emails,subject) =>{
		if(!subject)
			return emails
		else
			var _subject = new RegExp(subject.toLowerCase()+'.*');
			return emails.filter(email=>email.subject.toLowerCase().match(_subject))
	}

	render(){
		const {emails,fetch,onSetEmail} = this.props
		const { currentPage, pagesCount,subject_filter} = this.state;
		let filtered_emails = this.onSubjectFilter(emails,subject_filter)
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Correo
					</CardHeader> 
					<CardBody>
						<Row style={{marginBottom:15}}>
							<Col sm={6}>
								<Input name="subject_filter" placeholder="Buscar correo(s) según el asunto" onChange={(e) => {
									this.setState({subject_filter:e.target.value})
								}}/>
							</Col> 
						</Row>
						<Table responsive>
					        <thead>
					          <tr>
					            <th>De</th>
								<th>Asunto</th>
								<th>Fecha</th>
					            <th>Ver</th>
					          </tr>
					        </thead>
					        <tbody>
					        {
					        	(emails && emails.length > 0 )? 
										filtered_emails.slice(
										currentPage * this.pageSize,
										(currentPage + 1) * this.pageSize
									  ).map((email,index) => (
										<tr key={index}>	
											<td> {email.from} </td>
											<td> {email.subject?email.subject:"Sin asunto"} </td>
											<td> {email.date} </td>
											<td> 
												<Link to="/email/single">
													<Button color="info" onClick={() => {onSetEmail(email)}}> 
															Ver
													</Button> 
												</Link>
											</td>
										</tr>
					        		))
					        	:
					        	<tr> 
					        		<td colSpan={5}> {fetch.is_fetching?"Cargando...":'No hay correos para mostrar'} </td>
					        	</tr>
							}
								<tr>
									<td colSpan={5}>
										<Pagination aria-label="Page navigation example">
					
											<PaginationItem disabled={currentPage <= 0}>
											
											<PaginationLink
												onClick={e => this.handleClick(e, currentPage - 1)}
												previous
												href="#"
											/>
											
											</PaginationItem>

											{[...Array(this.pagesCount(filtered_emails))].map((page, i) => 
											<PaginationItem active={i === currentPage} key={i}>
												<PaginationLink onClick={e => this.handleClick(e, i)} href="#">
												{i + 1}
												</PaginationLink>
											</PaginationItem>
											)}

											<PaginationItem disabled={currentPage >= this.pagesCount(filtered_emails) - 1}>
											
												<PaginationLink
													onClick={e => this.handleClick(e, currentPage + 1)}
													next
													href="#"
												/>
											</PaginationItem>
										</Pagination>
									</td>
					      		</tr>
				        </tbody>
				      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default EmailList;