import React, {text} from 'react'
import { Form, FormGroup, Label } from 'reactstrap';
import {
  Col,
  Row,
  Card,
  CardHeader,
  CardBody,
} from 'reactstrap';

class EmailSingle extends React.Component {

    componentWillMount(){
        const {onFetchEmailSigle, email} = this.props;
        onFetchEmailSigle(email.id)
    }

	render(){
		const {email,fetch} = this.props
		return(
			<div className="custom-container-without-space">
                <Col xl={12} lg={12} md={12} style={{marginTop:10}}>
                    <Card>
                        <CardHeader>Detalle de correo</CardHeader>
                        <CardBody>
                        <Form>
                            <FormGroup row>
                                <Label sm={6}>
                                    <text style={{fontWeight:"bold"}}>De: </text> {email.from}
                                </Label>
                                <Label sm={6}>
                                    <text style={{fontWeight:"bold"}}>Para </text> {email.to}
                                </Label>
                                <Label sm={6}>
                                    <text style={{fontWeight:"bold"}}>Fecha: </text> {email.date}
                                </Label>
                                <Label sm={12}>
                                    <text style={{fontWeight:"bold"}}>Asunto: </text> {email.subject}
                                </Label>
                            </FormGroup>
                            <Card>
                                <CardBody>
                                    {
                                        fetch.is_fetching?
                                        <text>"Cargado..."</text>
                                        :
                                            fetch.status==1?
                                                
                                                <div style={{width:'75vw'}}>{email.content}</div>

                                            :
                                            <text>{fetch.message}</text>
                                    }
                                </CardBody>
                            </Card>        
                        </Form>
                        </CardBody>
                    </Card>
                </Col>
			</div>
		);
	}
}

export default EmailSingle;