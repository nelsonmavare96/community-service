import React from 'react'
import {Button, Input} from 'reactstrap'

class ClassificationEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {index, userTypes, sectors, classification, onUpdateClassification, onCancelEditClassification, onHandleClassificationInputChange, onHandleInputChange,onHandleStatusInputChange} = this.props
		return(
			<tr key={classification.id}>
				<td> {index+1}</td>
				<td> 
					<Input 
						data-id={classification.id}
						name="name"
						defaultValue={classification.name}
						value={classification.name}
						onChange={(e) => onHandleInputChange(e,classification.id)}
					/>
				</td>
				<td>  
					{/* <Input type="select" name="sector" data-id={classification.id} defaultValue={classification.sector} onChange={(e) => onHandleStatusInputChange(e,classification.id)} >
						<option value={-1}>Selecciona una opción</option>
						{(sectors && sectors.length>0) &&
							sectors.map(sector =>{
								return(
									<option value={1}>{sector.name}</option>
								)
							})
						}
					</Input> */}
					<Input type="select" name="sector" value={classification.sector} onChange={(e) => onHandleInputChange(e,classification.id)}>
						<option value="">Seleccione un sector</option>
						{sectors && sectors.length>0 &&
							sectors.map((sector,index)=>(
							sector.status==1&&
							<option key={index} value={sector.id}>{sector.name}</option>
							))}     
					</Input>
				</td>	
				<td>
					<Input type="select" name="userType" data-id={classification.id} value={classification.userType} onChange={(e) => onHandleInputChange(e,classification.id)}>
						<option value={-1}>Selecciona una opción</option>
						{(userTypes && userTypes.length>0) &&
							userTypes.map(userType =>{
								return(
									<option value={userType.id}>{userType.name}</option>
								)
							})
						}
					</Input>
				</td>		
				<td>  
					<Input data-id={classification.id} defaultValue={classification.status} onChange={(e) => onHandleStatusInputChange(e,classification.id)} type="select" name="status">
						<option value={-1}>Selecciona una opción</option>
						<option value={1}>Activo</option>
						<option value={2}>Inactivo</option>
					</Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdateClassification(classification)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditClassification(classification.originalName,classification.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default ClassificationEdit;