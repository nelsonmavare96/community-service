import React from 'react'
import {Button, Input} from 'reactstrap'

class ClassificationNew extends React.Component {
	
	render(){
		const {
			classification, 
			index,
			onHandleInputChange, 
			onCancelNewClassification,
			onHandleStatusInputChange,
			onSaveNewClassification,
			userTypes,
			sectors
		} = this.props

		return(
			<tr key={index}>
				<td>NUEVO</td>
				<td> 
					<Input
						//data-id={classification.id}
						name="name"
						value={classification.name}
						value={classification.name}
						onChange={(e) => onHandleInputChange(e,classification.id)}
					/>
				</td>	
				<td>  
					<Input type="select" name="sector" data-id={classification.id} value={classification.sector} onChange={(e) => onHandleInputChange(e,classification.id)}>
						<option value={-1}>Selecciona una opción</option>
						{(sectors && sectors.length>0) &&
							sectors.map(sector =>{
								return(
									<option value={sector.id}>{sector.name}</option>
								)
							})
						}
					</Input>
				</td>	
				<td>
					<Input type="select" name="userType" data-id={classification.id} value={classification.userType} onChange={(e) => onHandleInputChange(e,classification.id)}>
						<option value={-1}>Selecciona una opción</option>
						{(userTypes && userTypes.length>0) &&
							userTypes.map(userType =>{
								return(
									<option value={userType.id}>{userType.name}</option>
								)
							})
						}
					</Input>
				</td>
				<td>
					<Input data-id={classification.id} value={classification.status} onChange={(e) => onHandleStatusInputChange(e,classification.id)} type="select" name="status">
						<option value={-1}>Selecciona una opción</option>
						<option value={1}>Activo</option>
						<option value={2}>Inactivo</option>
					</Input>
				</td>	
				<td> 
					<Button color="success" onClick={() => onSaveNewClassification(classification)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelNewClassification(classification.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default ClassificationNew;