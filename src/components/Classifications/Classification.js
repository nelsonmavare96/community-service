import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Classification extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {index,classification, statusAsString, onDeleteClassification, onEditClassification, sectorAsString,userTypeAsString } = this.props

		return(
			<>
			<tr key={classification.id}>
				<td> {index+1} </td>		
				<td> {classification.name} </td>
				<td> {sectorAsString(classification.sector)} </td>
				<td> {userTypeAsString(classification.userType)} </td>
				<td> {statusAsString(classification.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditClassification(classification.id)}> 
						Modificar
					</Button> 
				</td>
				<td>
					{classification.status==1 && 
						<Button color="secondary" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button>
					} 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={classification} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={()=> {onDeleteClassification(classification.id);this.openHandleModal()}} />
			</>
		);
	}
}

export default Classification;