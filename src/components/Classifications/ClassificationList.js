import React from 'react'
import { Table } from 'reactstrap';
import {
  Card,
  Button,
  CardHeader,
  CardBody,
} from 'reactstrap';
import userTypes from '../../constants/userTypes'
import sectors from '../../constants/sector'

import Classification from './Classification'
import ClassificationEdit from './ClassificationEdit'
import ClassificationNew from './ClassificationNew'

class ClassificationList extends React.Component {

	constructor(props){
		super(props)
		this.hasSomeClassificationEditing = this.hasSomeClassificationEditing.bind(this)
	}

	hasSomeClassificationEditing(classifications){
		let _is_some = false
		if(classifications.length>0) 
			classifications.map((classification) => {
				if(classification.isEditing){
					_is_some=true
				}
			})
		return _is_some
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	sectorAsString(sectorId){
		for(let sector of sectors){
			if(sector.id == sectorId)
			return sector.name
		}
		return "-"
	}

	userTypeAsString(userTypeId){
		for(let userType of userTypes){
			if(userType.id == userTypeId)
			return userType.name
		}
		return "-"
	}

	render(){
		const {classifications,onCancelNewClassification,onUpdateClassification,onSaveNewClassification, onHandleInputChange, onHandleStatusInputChange, onAddNewClassification,onDeleteClassification,onEditClassification,onCancelEditClassification} = this.props
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Clasificaciones
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>Nro</th>
			            <th>Nombre</th>
						<th>Sector</th>
						<th>Tipo de usuario</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomeClassificationEditing(classifications))?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(classifications && classifications.length > 0)? 
			        		classifications.map((classification,index) => (
			        			(classification.isEditing)?
									<ClassificationEdit 
										key={index} 
										index={index} 
										sectors={sectors}
										userTypes = {userTypes}
										classification={classification}
										onUpdateClassification={(c)=>onUpdateClassification(c)} 
										onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,classification.id)} 
										onCancelEditClassification={(text)=>onCancelEditClassification(text,classification.id)}
										onEditClassification = {()=>onEditClassification(classification.id)}
										onHandleInputChange={(e)=>onHandleInputChange(e,classification.id)} 
									/>
			        			:
			        			(classification.isNew)?
									<ClassificationNew key={index} index={index} classifications={classifications} classification={classification} 
												sectors={sectors}
												userTypes = {userTypes}
												onHandleInputChange={(e)=>onHandleInputChange(e,classification.id)}
												onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,classification.id)} 
												onCancelNewClassification = {()=>onCancelNewClassification(classification.id)}
												onSaveNewClassification = {(c)=>onSaveNewClassification(c)}
									/>
			        			:
									<Classification 
										key={index}
										index={index} 
										classification={classification}
										sectorAsString={this.sectorAsString}
										userTypeAsString= {this.userTypeAsString} 
										statusAsString={this.statusAsString} 
										onDeleteClassification = {()=>onDeleteClassification(classification.id)}
										onEditClassification = {()=>onEditClassification(classification.id)}
									/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={5}> No existen paises registrados </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={5}>
					        <Button color="primary" onClick={() => {onAddNewClassification()}} >
					        	Agregar Clasificación
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default ClassificationList;