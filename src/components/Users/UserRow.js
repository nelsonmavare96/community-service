import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'
import {transformUserType} from '../../functions'

class UserRow extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {
			user, 
			onShowUser, 
			onEditUser, 
			statusAsString,
			rolIdAsString,
			onChangeView,
			onDeleteUser
		} = this.props

		return(
			<>
				<tr key={user.id}>	
					<td> {user.social_reason} </td>
					<td> {user.name} </td>
					<td> {user.lastname?user.lastname:"-"} </td>
					<td> {user.telephone} </td>
					<td> {user.classification.name} </td>
					<td> {transformUserType(user.user_type)} </td>
					<td> {statusAsString(user.status)} </td>
					<td> 
						<Button color="info" onClick={() => {onShowUser(user); onChangeView("show")}}> 
							Ver
						</Button> 
					</td>
					<td> 
						<Button color="success" onClick={() => {onEditUser(user); onChangeView("edit")}}> 
							Modificar
						</Button> 
					</td>
					<td> 
						<Button color="danger" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button> 
					</td>
				</tr>
				<ModalConfirmDeleteItem item={user} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteUser} onChangeView={onChangeView}/>
			</>
		);
	}
}

export default UserRow;