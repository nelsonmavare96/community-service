import React from 'react'
import { Table } from 'reactstrap';
import {filterUsers} from '../../functions'
import {
  Row,
  Col,
  Label,
  Card,
  Button,
  CardHeader,
  CardBody,
  Input,
  Pagination, PaginationItem, PaginationLink
} from 'reactstrap';
import Select from 'react-select';

import UserRow from './UserRow'

class UserList extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			filter_by_classifications: false,
			filter_by_pathologies: false,
			array_selected_pathologies: [],
			selected_classification:"",
			currentPage: 0,
		}
		this.pageSize = 10;
	}

	handleClick(e, index) {
    
		this.setState({
		  currentPage: index
		});
		
	  }
	
	pagesCount(users){
		if(users && users.length>0)
			return Math.ceil(users.length / this.pageSize);
		return 0
	}

	filterByPathologies = (users,array) =>{
		const {setFilteresUsers} = this.props;
		if(array && array.length>0){
			setFilteresUsers(filterUsers(users,array));
			this.setState({filter_by_pathologies: true})

		}
		else{
			if(this.state.selected_classification)
				{setFilteresUsers(users.filter(user => user.classification.id==this.state.selected_classification));
				this.setState({filter_by_pathologies: false});
			}
			else{
				setFilteresUsers(users)
				this.setState({filter_by_pathologies: false});
			}
			
		}
	}

	filterByClassifications = (value,users) => {
		const {setFilteresUsers} = this.props;
		if(value){
			setFilteresUsers(users.filter(user => user.classification.id==value));
			this.setState({filter_by_classifications: true});
		}
		else{
			if(this.state.array_selected_pathologies && this.state.array_selected_pathologies.length>0)
				{setFilteresUsers(filterUsers(users,this.state.array_selected_pathologies));
				this.setState({filter_by_classifications: false});
			}
			else{
				setFilteresUsers(users);
				this.setState({filter_by_classifications: false});
			}
			
		}
	}

	render(){
		const {
			users,
			user,
			pathologies,
			countries, 
			onShowUser, 
			onEditUser, 
			classifications,
			onOpenDeleteUserModal, 
			onAddNewUserClick, 
			statusAsString,
			rolIdAsString,
			onChangeView,
			onRestoreUser,
			onDeleteUser,
			filtered_users,
			setFilteresUsers
		} = this.props;
		const { currentPage } = this.state;
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Usuarios 
					</CardHeader> 
					<CardBody>
						<Row style={{marginBottom:15}}>
							<Col sm={6}>
								<Select
									placeholder="Filtrar por patologías"
									isMulti
									isDisabled = {this.state.filter_by_classifications}
									className="basic-multi-select"
									classNamePrefix="select"
									onChange={(array) => {this.filterByPathologies(users,array);this.setState({array_selected_pathologies:array})}}
									options={pathologies} 
								/>
							</Col>
							<Col sm={2}>
								<Button 
									disabled={!this.state.filter_by_classifications} 
									onClick={()=>{
										this.setState({filter_by_classifications:false, filter_by_pathologies:true});
										setFilteresUsers(filterUsers(users,this.state.array_selected_pathologies));
								}}
								>
									Habilitar
								</Button>
							</Col>
						</Row> 
						<Row style={{marginBottom:15}}>
							<Col sm={6}>
								<Input type="select" disabled={this.state.filter_by_pathologies} name="classification_id" onChange={(e) => {
									this.filterByClassifications(e.target.value,users);
									this.setState({selected_classification:e.target.value})
								}}>
								<option value="">Filtrar por clasificación</option>
								{classifications && classifications.length>0 &&
								classifications.map((classification,index)=>(	
									//classification.status == 1 &&
									<option key={index} value={classification.id}>{classification.name}</option>
								))}     
								</Input>
							</Col> 
							<Col sm={2}>
								<Button 
									disabled={!this.state.filter_by_pathologies} 
									onClick={()=>{
										this.setState({filter_by_classifications:true, filter_by_pathologies:false});
										this.state.selected_classification?
												setFilteresUsers(users.filter(user => user.classification.id==this.state.selected_classification))
											:
												setFilteresUsers(users)
								}}
								>
									Habilitar
								</Button>
							</Col>
						</Row>
						<Table responsive>
					        <thead>
					          <tr>
					            <th>Razón social</th>
					            <th>Nombre</th>
								<th>Apellido</th>
								<th>teléfono</th>
								<th>Clasificación</th>
								<th>Tipo de usuario</th>
								<th>Status</th>
					            <th>Ver</th>
					            <th>Modificar</th>
					            <th>Eliminar</th>
					          </tr>
					        </thead>
					        <tbody>
					        {
					        	(users && users.length > 0 && filtered_users.length>0)? 
									filtered_users.slice(
										currentPage * this.pageSize,
										(currentPage + 1) * this.pageSize
									  ).map((user) => (
										user.status=="1" &&
										<UserRow
											countries={countries} 
					        				key={user.id} 
					        				user={user} 
					        				onShowUser={onShowUser} 
					        				onEditUser={onEditUser} 
					        				statusAsString={statusAsString} 
					        				onOpenDeleteUserModal={onOpenDeleteUserModal} 
											rolIdAsString={rolIdAsString}
											onChangeView={onChangeView}
											onDeleteUser={onDeleteUser}  
					        			/>
					        		))
					        	:
					        	<tr> 
					        		<td colSpan={5}> No existen usuarios registrados </td>
					        	</tr>
							}
							<tr>
								<td colSpan={5}>
									<Pagination aria-label="Page navigation example">
				
										<PaginationItem disabled={currentPage <= 0}>
										
											<PaginationLink
												onClick={e => this.handleClick(e, currentPage - 1)}
												previous
												href="#"
											/>
										
										</PaginationItem>

										{[...Array(this.pagesCount(filtered_users))].map((page, i) => 
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={e => this.handleClick(e, i)} href="#">
											{i + 1}
											</PaginationLink>
										</PaginationItem>
										)}

										<PaginationItem disabled={currentPage >= this.pagesCount(filtered_users) - 1}>
										
											<PaginationLink
												onClick={e => this.handleClick(e, currentPage + 1)}
												next
												href="#"
											/>
										</PaginationItem>
									</Pagination>
								</td>
							</tr>
					        <tr>
					        	<td colSpan={5}>
							        <Button color="primary" onClick={() => {onAddNewUserClick(); onRestoreUser()}} >
							        	Agregar Usuario
							        </Button>
						      	</td>
					      	</tr>
				        </tbody>
				      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default UserList;