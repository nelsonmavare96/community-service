import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';


class UserNew extends React.Component {

	constructor(props){
		super(props)
	}

	render(){
		const {
			user,
			countries,
			roles, 
			onCancelAddNewUserClick, 
			onSaveClick,
			onHandleUserInputChange,
			onSaveNewUser
			} = this.props

		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12}>
	          <Card>
	            <CardHeader>Registrar nuevo Usuario</CardHeader>
	            <CardBody>
	              <Form>
	                <FormGroup row>
	                  <Label for="email" sm={2}>
	                    Email
	                  </Label>
	                  <Col sm={10}>
	                    <Input
	                      type="email"
	                      name="email"
	                      defaultValue={user.email}
	                      onChange={(e) => onHandleUserInputChange(e)}
	                      placeholder="Ingrese el email"
	                    />
	                  </Col>
	                </FormGroup>
	                <FormGroup row>
	                  <Label for="password" sm={2}>
	                    Contraseña
	                  </Label>
	                  <Col sm={10}>
	                    <Input
	                      type="password"
	                      name="password"
	                      defaultValue={user.password}
	                      onChange={(e) => onHandleUserInputChange(e)}
	                      placeholder="Ingrese la contraseña"
	                    />
	                  </Col>
	                </FormGroup>
	                <FormGroup row>
	                  <Label for="confirmPassword" sm={2}>
	                    Confirmación
	                  </Label>
	                  <Col sm={10}>
	                    <Input
	                      type="password"
	                      name="confirmPassword"
	                      defaultValue={user.confirmPassword}
	                      onChange={(e) => onHandleUserInputChange(e)}
	                      placeholder="Ingrese la contraseña de confirmación"
	                    />
	                  </Col>
	                </FormGroup>	                
	                <FormGroup row>
	                  <Label for="exampleSelect" sm={2}>
	                    Rol
	                  </Label>
	                  <Col sm={10}>
	                    <Input type="select" name="role_id" defaultValue={(user && user.role_id!="")? user.role_id : "" } onChange={(e) => onHandleUserInputChange(e)}>
									<option value="-1"> Seleccione un rol </option>
									{
										(roles && roles.length > 0)? 
										roles.map((rol) => (
											<option value={rol.id}> {rol.name} </option>
										)):
										<option value={-1}> No existen roles </option>
									}
	                  	</Input>
	                  </Col>
	                </FormGroup>
					<FormGroup row>
	                  <Label for="exampleSelect" sm={2}>
	                    Pais
	                  </Label>
	                  <Col sm={10}>
	                    <Input type="select" name="country_id" defaultValue={(user && user.country_id!="")? user.country_id : "" } onChange={(e) => onHandleUserInputChange(e)}>
									<option value="-1"> Seleccione un Pais </option>
									{
									(countries && countries.length > 0)? 
					        		countries.map((country) => (
										<option value={country.id}> {country.name} </option>
									)):
									<option value={-1}> No existen paises </option>
									}
						</Input>
	                  </Col>
	                </FormGroup>
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button style={{float: "right"}} onClick={()=> onCancelAddNewUserClick()}>Cancelar</Button>						
							<Button color="success" style={{marginRight: "20px",float: "right"}} onClick={() => {onSaveNewUser(user);onSaveClick()}}>Guardar</Button>						
	                  </Col>
	                </FormGroup>
	              </Form>
	            </CardBody>
	          </Card>
	        </Col>
			</div>
		);
	}
}

export default UserNew;