import React, {text} from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGrouptext,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
	Formtext
} from 'reactstrap';
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'


class User extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}


	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {user, onEditUser, onDeleteUser, rolIdAsString, onGoBackToList,onChangeView} = this.props

		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12} style={{marginTop:10}}>
	          <Card>
	            <CardHeader>Información Usuario</CardHeader>
	            <CardBody>
	              <Form>
	                <FormGroup row>
										<Label sm={6}>
	                    <text style={{fontWeight:"bold"}}>Nombre: </text> {user.name}
	                  </Label>
										{user.user_type==1&&
											<Label sm={6}>
												<text style={{fontWeight:"bold"}}>Apellido: </text> {user.lastname}
											</Label>
										}
										<Label sm={6}>
											<text style={{fontWeight:"bold"}}>Razón social: </text> {user.social_reason}
	                  </Label>
										<Label sm={6}>
											<text style={{fontWeight:"bold"}}>Teléfono: </text> {user.telephone}
	                  </Label>
										<Label sm={6}>
											<text style={{fontWeight:"bold"}}>Clasificación: </text> {user.classification.name}
	                  </Label>
										<Label sm={6}>
											<text style={{fontWeight:"bold"}}>Tipo de usuario: </text> {user.user_type}
	                  </Label>
	                </FormGroup>
									
	                { user.user_type==2?
											<FormGroup row>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Sitio Web: </text> {user.website}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Facebook: </text> {user.facebook}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Twitter: </text> {user.twitter}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Instagram: </text> {user.instagram}
												</Label>
												<Label sm={12}>
													<text style={{fontWeight:"bold"}}>Ubicación: </text> {user.address}
												</Label>
											</FormGroup>
										:
											<FormGroup row>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Fecha de nacimiento: </text> {user.birthdate}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Grado de institucuión: </text> {user.instruction_degree}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Trabajo: </text> {user.job}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Tipo de trabajo: </text> {user.type_job}
												</Label>
												<Label sm={6}>
													<text style={{fontWeight:"bold"}}>Estudio actual: </text> {user.current_study}
												</Label>
											</FormGroup>
									}
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button color="info" style={{float: "right"}} onClick={()=> onGoBackToList()}>Volver</Button>
	                    <Button color="danger" style={{float: "right", marginRight: 10, marginLeft: -10}} onClick={()=> this.openHandleModal()}>Eliminar</Button>
	                    <Button color="success" style={{float: "right", marginRight: 20, marginLeft: -20}} onClick={() => {onEditUser(user);onChangeView("edit")}}>Modificar</Button>
	                  </Col>
	                </FormGroup>
	              </Form>
	            </CardBody>
	          </Card>
	        </Col>
	        <ModalConfirmDeleteItem item={user} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteUser} onChangeView={onChangeView}/>
			</div>
		);
	}
}

export default User;