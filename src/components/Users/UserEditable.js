import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';
import Select from 'react-select';


class UserEditable extends React.Component {

	constructor(props){
		super(props)
	}
	rolIdAsString(rol_id, roles){
		let rol_string = ""
		roles.map((rol)=>{
			if(rol.id ==rol_id){
				rol_string=rol.name
			}	
		}) 
		
		return rol_string
	}	

	render(){
		const {
					user,
					roles,
					countries, 
					pathologies,
					onCancelEditClick, 
					onUpdateUserClick, 
					onUpdateUser,
					onHandleUserRoleChange,
					onHandleUserInputChange,
          onHandleUserProfileInputChange,
					onHandleUserCountryChange,
					onHandleUserMultiSelect
				} = this.props
		
		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12}>
	          <Card>
	            <CardHeader>Editar información de usuario</CardHeader>
	            <CardBody>
	              <Form>
                  {/* <FormGroup row style={{backgroundColor:"white", borderRadius:6, paddingTop:15, paddingBottom:15}}>
                    <Col xl={6} lg={12} md={12}>
                      <Label >Tipo de usuario</Label>
                      <Form style={{height:'2.5rem', display:'flex'}} onChange={(e)=>{this.setState({type_person:e.target.id})}}>
                        <FormGroup check inline>
                          <Label check>
                            <Input type="radio" name="user_type" id="person"/> Persona
	                  </Label>
                        </FormGroup>
                        <FormGroup check inline>
                          <Label check>
                            <Input type="radio" name="user_type" id="institution"/> Institución
                          </Label>
                        </FormGroup>
                      </Form>
                    </Col>
                  </FormGroup> */}
                {user.user_type==1 ?
                  <FormGroup row style={{backgroundColor:"white", borderRadius:6, paddingTop:15, paddingBottom:15}}>
                    <Col sm={6}>
                      <Label >Nombre</Label>
	                    <Input
                        type="text" 
                        name="name"
                        placeholder="Nombre"
                        value={user.name} 
	                      onChange={(e) => onHandleUserInputChange(e)}
	                    />
	                  </Col>
                    <Col sm={6}>
                      <Label>Apellido</Label>
                      <Input 
                        type="text" 
                        name="lastname"
                        placeholder="Apellido"
                        value={user.lastname} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Fecha de nacimiento</Label>
                      <Input
                        type="date"
                        name="birthdate"
                        id="birthdate"
                        placeholder="Fecha de nacimiento"
                        value={user.birthdate} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Razón social</Label>
                      <Input 
                        type="text" 
                        name="social_reason"
                        placeholder="Nombre"
                        value={user.social_reason} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Grado de Institución</Label>
                      <Input 
                        name="instruction_degree"
                        placeholder="Grado de Institución"
                        value={user.instruction_degree} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Tipo de trabajo</Label>
                      <Input 
                        type="text"  
                        name="job_type"
                        placeholder="Tipo de trabajo"
                        value={user.job_type} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Nombre del trabajo</Label>
                      <Input 
                        type="text" 
                        name="job"
                        placeholder="Nombre del trabajo"
                        value={user.job} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>Estudio actual</Label>
                      <Input 
                        type="text" 
                        name="current_study"
                        placeholder="Estudio actual"
                        value={user.current_study} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </Col>
                    <Col sm={6}>
                      <Label>País</Label>
                      <Input type="select" name="country_id" value={user.country_id} onChange={(e) => onHandleUserInputChange(e)}>
                        <option value="">Seleccione una país</option>
                        {countries.map((country,index)=>(
                          <option key={index} value={country.id}>{country.name}</option>
                        ))}
	                  	</Input>
	                  </Col>
                    <Col sm={6}>
                      <Label>Patologías</Label>
                      <Select
                        placeholder="Seleccione la(s) patologia(s)"
                        isMulti
                        className="basic-multi-select"
                        classNamePrefix="select"
                        value={user.pathologies}
                        onChange={(array) => onHandleUserMultiSelect(array,'pathologies')}
                        
                        options={pathologies} 
                      />
                    </Col> 
	                </FormGroup>
                :
                   user.user_type==2 &&
                    <FormGroup row style={{backgroundColor:"white", borderRadius:6, paddingTop:15, paddingBottom:15}}>
                      <Col sm={6}>
                        <Label>Nombre</Label>
                        <Input 
                          type="text" 
                          name="name"
                          placeholder="Nombre"
                          value={user.name} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Razón social</Label>
                        <Input 
                          type="text" 
                          name="social_reason"
                          placeholder="Nombre"
                          value={user.social_reason} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Ubicación</Label>
                        <Input 
                          type="text" 
                          name="address"
                          placeholder="Ubicación"
                          value={user.address} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Sitio web</Label>
                        <Input
                          type="text"
                          name="website"
                          id="website"
                          placeholder="Sitio web"
                          value={user.website} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Facebook</Label>
                        <Input 
                          name="facebook"
                          value={user.facebook} 
                          placeholder="Facebook"
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>Twitter</Label>
                        <Input 
                          type="text"  
                          name="twitter"
                          placeholder="Twitter"
                          value={user.twitter} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label >Instagram</Label>
                        <Input 
                          type="text" 
                          name="instagram"
                          placeholder="Instagram"
                          value={user.instagram} 
                          onChange={(e) => onHandleUserInputChange(e)}
                        />
                      </Col>
                      <Col sm={6}>
                        <Label>País</Label>
                        <Input type="select" name="country_id" value={user.country_id} onChange={(e) => onHandleUserInputChange(e)}>
                          <option value="">Seleccione una país</option>
                          {countries.map((country,index)=>(
                            <option key={index} value={country.id}>{country.name}</option>
                          ))}     
	                  	</Input>
	                  </Col>
	                </FormGroup>
					
                }
	              </Form>
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button style={{float: "right"}} onClick={()=> {onCancelEditClick();}}>Cancelar</Button>
	                    <Button color="success" style={{marginRight: "20px",float: "right"}} onClick={() => {onUpdateUserClick(user); onUpdateUser(user)}}>Guardar</Button>
	                  </Col>
	                </FormGroup>
	            </CardBody>
	          </Card>
	        </Col>
			</div>
		);
	}
}

export default UserEditable;