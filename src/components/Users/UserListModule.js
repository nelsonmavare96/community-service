import React from 'react'
import UserList from './UserList'
import UserView from './UserView'

class UserListModule extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			view: "list",			
		}

		this.statusAsString=this.statusAsString.bind(this)
		this.onViewUser=this.onViewUser.bind(this)
		this.onEditClick=this.onEditClick.bind(this)
		this.onCancelEditClick=this.onCancelEditClick.bind(this)
		this.onUpdateUserClick=this.onUpdateUserClick.bind(this)
		this.onAddNewUserClick=this.onAddNewUserClick.bind(this)
		this.onSaveClick=this.onSaveClick.bind(this)
		this.onCancelAddNewUserClick=this.onCancelAddNewUserClick.bind(this)
		this.onHandleInputChange=this.onHandleInputChange.bind(this)
		this.rolIdAsString=this.rolIdAsString.bind(this)
		this.onGoBackToList=this.onGoBackToList.bind(this)
		this.onChangeView = this.onChangeView.bind(this)
	}

	onHandleInputChange(event){
		let user = Object.assign({}, this.state.user, {
			[event.target.name]: event.target.value
		})

		this.setState({user})
	}

	onGoBackToList(){
		this.setState({view:"list"})
	}

	rolIdAsString(rol_id){
		let rol_string = ""
		switch(parseInt(rol_id,10)){
			case 1: 
				rol_string = "Administrador"; break;
			case 2: 
				rol_string = "Usuario"; break;
			default:
				rol_string = "Otro"; break;
		}
		return rol_string
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	onViewUser(reason){
		switch(reason.toString()){
			case "show":
				this.setState({view: "show"}); break;
			case "edit":
				this.setState({view: "edit"}); break; 
			case "new":
				this.setState({view: "new"}); break;
		}
	}

	onEditClick(id){
		this.setState({view: "edit"})
	}

	onCancelEditClick(id){
		this.setState({view: "list"})
	}

	onChangeView(type){
		//type is"edit","show" or "list"
		this.setState({view: type})
	}

	onUpdateUserClick(user){
		this.setState({view: "show"})

	}

	onAddNewUserClick(){
		this.setState({view: "new"})		
	}

	onSaveClick(user){
		this.setState({view: "list"})
	}

	onCancelAddNewUserClick(){
		this.setState({user:{}, view: "list"})
	}

	render(){
		const {view} = this.state
		const {
			users,
			countries,
			roles,
			pathologies,
			user,
			classifications,
			onShowUser,
			onEditUser,
			onRestoreUser,
			onDeleteUser,
			onHandleUserInputChange,
			onUpdateUser,
			onHandleUserRoleChange,
			onHandleUserCountryChange,
			onSaveNewUser,
			onHandleUserMultiSelect,
			setFilteresUsers,
			filtered_users
		} = this.props
		return(
			<div> 
			{
				(view && view == "list")?
					<UserList
						countries={countries}
						roles={roles}
						users={users}
						classifications = {classifications}
						pathologies= {pathologies}
						rolIdAsString={this.rolIdAsString}
						statusAsString={this.statusAsString}
						onShowUser={onShowUser} 
						onEditUser={onEditUser}
						onOpenDeleteUserModal={this.onOpenDelete} 
						onAddNewUserClick={this.onAddNewUserClick}
						onChangeView={this.onChangeView}
						onRestoreUser={onRestoreUser}
						onDeleteUser = {onDeleteUser} 
						setFilteresUsers = {setFilteresUsers}
						filtered_users = {filtered_users}
					/>
				:
					<UserView 
						view={view}
						countries={countries}
						roles={roles}
						pathologies={pathologies}
						onHandleUserRoleChange={onHandleUserRoleChange}
						onHandleUserCountryChange={onHandleUserCountryChange} 
						user={user} 
						onGoBackToList={this.onGoBackToList}
						rolIdAsString={this.rolIdAsString}
						onHandleUserInputChange={onHandleUserInputChange}
						onEditUser={onEditUser}
						onCancelEditClick={this.onCancelEditClick}
						onSaveClick={this.onSaveClick}
						onOpenDeleteUserModal={this.onOpenDelete}
						onDeleteUser={onDeleteUser}
						onCancelAddNewUserClick={this.onCancelAddNewUserClick}
						onUpdateUserClick={this.onUpdateUserClick}
						onChangeView={this.onChangeView}
						onUpdateUser={onUpdateUser}
						onSaveNewUser={onSaveNewUser}
						onHandleUserMultiSelect={onHandleUserMultiSelect}
					/>
			}
			</div>
		);
	}
}

export default UserListModule;