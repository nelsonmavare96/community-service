import React from 'react'
import UserNew from './UserNew'
import User from './User'
import UserEditable from './UserEditable'

class UserView extends React.Component {

	render(){
		
		const {
			view,
			countries,
			roles,
			user,
			pathologies,
			onCancelEditClick,
			onSaveClick,
			onOpenDeleteUserModal,
			onDeleteUser,
			onCancelAddNewUserClick,
			onUpdateUserClick,
			onEditUser,
			rolIdAsString,
			onGoBackToList,
			onChangeView,
			onHandleUserRoleChange,
			onHandleUserInputChange,
			onHandleUserCountryChange,
			onHandleEmailInputChange,
			onHandleUserEmailInputChange,
			onHandleUserPasswordInputChange,
			onHandleUserPassworConfirmdInputChange,
			onHandleUserUsernameInputChange,
			onHandleUserRoleInputChange,
			onHandleUserMultiSelect,
			onUpdateUser,
			onSaveNewUser
		} = this.props

		return(
			<div>
			{
				(view && view.toString() === "new")?
					<UserNew 
						user={user}
						roles={roles}
						pathologies={pathologies}
						countries={countries} 	
						onCancelAddNewUserClick={onCancelAddNewUserClick} 
						onSaveClick={onSaveClick}
						onHandleUserInputChange={onHandleUserInputChange}
						onSaveNewUser={onSaveNewUser} 
						/>
				:
				(view && view.toString() === "show")?
					<User user={user} onChangeView={onChangeView} onEditUser={onEditUser} onDeleteUser={onDeleteUser} rolIdAsString={rolIdAsString} onGoBackToList={onGoBackToList} />
				:
					<UserEditable 
						user={user}
						pathologies={pathologies}
						roles={roles}
						countries={countries} 
						onChangeView={onChangeView}
						onHandleUserMultiSelect = {onHandleUserMultiSelect}
						onHandleUserRoleChange={onHandleUserRoleChange}
						onHandleUserCountryChange={onHandleUserCountryChange} 
						onCancelEditClick={onCancelEditClick}
						onUpdateUserClick={onUpdateUserClick}
						onHandleUserInputChange={onHandleUserInputChange}
						onUpdateUser={onUpdateUser} />
			}
			</div>
		);
	}
}

export default UserView;