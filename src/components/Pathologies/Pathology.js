import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Pathology extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {index,pathology, statusAsString, onDeletePathology, onEditPathology} = this.props

		return(
			<>
			<tr key={pathology.id}>
				<td> {index+1} </td>		
				<td> {pathology.name} </td>
				<td> {statusAsString(pathology.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditPathology(pathology.id)}> 
						Modificar
					</Button> 
				</td>
				<td>
					{pathology.status==1 && 
						<Button color="secondary" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button>
					} 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={pathology} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={()=> {onDeletePathology(pathology.id);this.openHandleModal()}} />
			</>
		);
	}
}

export default Pathology;