import React from 'react'
import {Button, Input} from 'reactstrap'

class PathologyEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {index, userTypes, sectors, pathology, onUpdatePathology, onCancelEditPathology, onHandlePathologyInputChange, onHandleInputChange,onHandleStatusInputChange} = this.props
		return(
			<tr key={pathology.id}>
				<td> {index+1}</td>
				<td> 
					<Input 
						data-id={pathology.id}
						name="name"
						defaultValue={pathology.name}
						value={pathology.name}
						onChange={(e) => onHandleInputChange(e,pathology.id)}
					/>
				</td>		
				<td>  
					<Input data-id={pathology.id} defaultValue={pathology.status} onChange={(e) => onHandleStatusInputChange(e,pathology.id)} type="select" name="status">
						<option value={-1}>Selecciona una opción</option>
						<option value={1}>Activo</option>
						<option value={2}>Inactivo</option>
					</Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdatePathology(pathology)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditPathology(pathology.originalName,pathology.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default PathologyEdit;