import React from 'react'
import { Table } from 'reactstrap';
import {
  Card,
  Button,
  CardHeader,
  CardBody,
} from 'reactstrap';
import userTypes from '../../constants/userTypes'
import sectors from '../../constants/sector'

import Pathology from './Pathology'
import PathologyEdit from './PathologyEdit'
import PathologyNew from './PathologyNew'

class PathologyList extends React.Component {

	constructor(props){
		super(props)
		this.hasSomePathologyEditing = this.hasSomePathologyEditing.bind(this)
	}

	hasSomePathologyEditing(pathologies){
		let _is_some = false
		if(pathologies.length>0) 
			pathologies.map((pathology) => {
				if(pathology.isEditing){
					_is_some=true
				}
			})
		return _is_some
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	sectorAsString(sectorId){
		for(let sector of sectors){
			if(sector.id == sectorId)
			return sector.name
		}
		return "-"
	}

	userTypeAsString(userTypeId){
		for(let userType of userTypes){
			if(userType.id == userTypeId)
			return userType.name
		}
		return "-"
	}

	render(){
		const {pathologies,onCancelNewPathology,onUpdatePathology,onSaveNewPathology, onHandleInputChange, onHandleStatusInputChange, onAddNewPathology,onDeletePathology,onEditPathology,onCancelEditPathology} = this.props
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Patologías
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>Nro</th>
			            <th>Nombre</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomePathologyEditing(pathologies))?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(pathologies && pathologies.length > 0)? 
			        		pathologies.map((pathology,index) => (
			        			(pathology.isEditing)?
									<PathologyEdit 
										key={index} 
										index={index} 
										sectors={sectors}
										userTypes = {userTypes}
										pathology={pathology}
										onUpdatePathology={(c)=>onUpdatePathology(c)} 
										onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,pathology.id)} 
										onCancelEditPathology={(text)=>onCancelEditPathology(text,pathology.id)}
										onEditPathology = {()=>onEditPathology(pathology.id)}
										onHandleInputChange={(e)=>onHandleInputChange(e,pathology.id)} 
									/>
			        			:
			        			(pathology.isNew)?
									<PathologyNew key={index} index={index} pathologies={pathologies} pathology={pathology} 
												sectors={sectors}
												userTypes = {userTypes}
												onHandleInputChange={(e)=>onHandleInputChange(e,pathology.id)}
												onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,pathology.id)} 
												onCancelNewPathology = {()=>onCancelNewPathology(pathology.id)}
												onSaveNewPathology = {(c)=>onSaveNewPathology(c)}
									/>
			        			:
									<Pathology 
										key={index}
										index={index} 
										pathology={pathology}
										sectorAsString={this.sectorAsString}
										userTypeAsString= {this.userTypeAsString} 
										statusAsString={this.statusAsString} 
										onDeletePathology = {()=>onDeletePathology(pathology.id)}
										onEditPathology = {()=>onEditPathology(pathology.id)}
									/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={5}> No existen paises registrados </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={5}>
					        <Button color="primary" onClick={() => {onAddNewPathology()}} >
					        	Agregar Patología
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default PathologyList;