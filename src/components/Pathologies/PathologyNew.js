import React from 'react'
import {Button, Input} from 'reactstrap'

class PathologyNew extends React.Component {

	render(){
		const {
			pathology, 
			index,
			onHandleInputChange, 
			onCancelNewPathology,
			onHandleStatusInputChange,
			onSaveNewPathology,
			sectors
		} = this.props

		return(
			<tr key={index}>
				<td>NUEVO</td>
				<td> 
					<Input
						//data-id={pathology.id}
						name="name"
						value={pathology.name}
						value={pathology.name}
						onChange={(e) => onHandleInputChange(e,pathology.id)}
					/>
				</td>	
				<td>
					<Input data-id={pathology.id} value={pathology.status} onChange={(e) => onHandleStatusInputChange(e,pathology.id)} type="select" name="status">
						<option value={-1}>Selecciona una opción</option>
						<option value={1}>Activo</option>
						<option value={2}>Inactivo</option>
					</Input>
				</td>	
				<td> 
					<Button color="success" onClick={() => onSaveNewPathology(pathology)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelNewPathology(pathology.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default PathologyNew;