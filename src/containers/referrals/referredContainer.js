import { connect } from 'react-redux'
import ReferralsListModule from '../../components/Referrals/ReferralsListModule'
import * as referralsActions from '../../actions/referralsActions'
import * as countriesAction from '../../actions/countriesAction'
import * as rolesAction from '../../actions/rolesAction'
import * as pathologiesAction from '../../actions/pathologiesAction'
import rolList from "../../constants/roles"
const mapStateToProps = state => {
    return {
        roles: rolList,
        referrals: state.referrals.referrals,
        referred: state.referrals.referred,
        countries : state.countries.countries,  
        pathologies: state.pathologies.pathologies,
        classifications: state.classifications.classifications  
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(pathologiesAction.fetchPathologies)
    dispatch(referralsActions.fetchReferrals())
    dispatch(countriesAction.fetchCountries())
    dispatch(rolesAction.fetchRoles())    
    return {
        onHandleReferredCountryChange: (e) =>{dispatch(referralsActions.handleReferredCountryChange(e))},
        onHandleReferredRoleChange: (e) =>{dispatch(referralsActions.handleReferredRoleChange(e))},
        onShowReferred:(referred) => {dispatch(referralsActions.showReferred(referred))},
        onEditReferred:(referred) => {dispatch(referralsActions.editReferred(referred))},
        onRestoreReferred: () => {dispatch(referralsActions.restoreReferred())},
        onDeleteReferred: (id) => {dispatch(referralsActions.fetchDeleteReferred(id))},
        onUpdateReferred: (referred) => {dispatch(referralsActions.fetchUpdateReferred(referred))},
        onSaveNewReferred: (referred) => {dispatch(referralsActions.fetchCreateReferred(referred))},
        onHandleReferredInputChange: (e) => {dispatch(referralsActions.handleReferredInputChange(e))},
    }
}
const referredContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ReferralsListModule)

export default referredContainer