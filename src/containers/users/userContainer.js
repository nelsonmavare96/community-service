import { connect } from 'react-redux'
import UserListModule from '../../components/Users/UserListModule'
import * as userAction from '../../actions/userAction'
import * as countriesAction from '../../actions/countriesAction'
import * as rolesAction from '../../actions/rolesAction'

import rolList from "../../constants/roles"

const mapStateToProps = state => {
    return {
        userData: state.main.userData,
        roles: rolList,
        users: state.user.users,
        filtered_users: state.user.filtered_users,
        user: state.user.user,
        countries : state.countries.countries, 
        pathologies: state.pathologies.pathologies,
        classifications: state.classifications.classifications
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(userAction.fetchUsers())
    dispatch(countriesAction.fetchCountries())
    dispatch(rolesAction.fetchRoles())    
    return {
        onHandleUserCountryChange: (e) =>{dispatch(userAction.handleUserCountryChange(e))},
        onHandleUserRoleChange: (e) =>{dispatch(userAction.handleUserRoleChange(e))},
        onShowUser:(user) => {dispatch(userAction.showUser(user))},
        onEditUser:(user) => {dispatch(userAction.editUser(user))},
        onRestoreUser: () => {dispatch(userAction.restoreUser())},
        onDeleteUser: (id) => {dispatch(userAction.fetchDeleteUser(id))},
        onUpdateUser: (user) => {dispatch(userAction.fetchUpdateUser(user))},
        onSaveNewUser: (user) => {dispatch(userAction.fetchCreateUser(user))},
        onHandleUserInputChange: (e) => {dispatch(userAction.handleUserInputChange(e))},
        onHandleUserMultiSelect: (array,state) => {dispatch(userAction.handleUserMultiSelectInput(array,state))},
        setFilteresUsers:(users) =>{dispatch(userAction.setFilteresUsers(users))}
    }
}
const userContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserListModule)

export default userContainer