import { connect } from 'react-redux'
import pathologiesList from '../../components/Pathologies/PathologyList'
import * as pathologiesAction from '../../actions/pathologiesAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      pathologies: state.pathologies.pathologies
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(pathologiesAction.fetchPathologies())
    return {
        onHandleInputChange: (event,id) => {dispatch(pathologiesAction.handleInputChange(event,id))},
        onAddNewPathology: () => {dispatch(pathologiesAction.addNewPathology())},
        onCancelNewPathology: (id) => {dispatch(pathologiesAction.deletePathology(id,true))},
        onDeletePathology: (id) => {dispatch(pathologiesAction.fetchDeletePathology(id))},
        onEditPathology: (id) => {dispatch(pathologiesAction.editPathology(id))},
        onCancelEditPathology: (text,id) => {dispatch(pathologiesAction.cancelEditPathology(text,id))},
        onHandleStatusInputChange: (event,id) => {dispatch(pathologiesAction.handleStatusInputChange(event,id))},
        onUpdatePathology: (pathology) => {dispatch(pathologiesAction.fetchUpdatePathology(pathology))},
        onSaveNewPathology: (pathology) => {dispatch(pathologiesAction.fetchCreatePathology(pathology))},
    }
}

const pathologiesListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(pathologiesList)

export default pathologiesListContainer