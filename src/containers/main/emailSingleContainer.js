import { connect } from 'react-redux'
import emailSingle from '../../components/Main/EmailSingle'
import * as mainAppActions from '../../actions/mainAppAction'


const mapStateToProps = state => {
    return {
        globalData: state.main.globalData,
        user: state.main.userData,
        email: state.main.email,
        fetch: state.main.fetch
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchEmailSigle: (id) => {
            dispatch(mainAppActions.fetchEmailSingle(id))
        }
    }
}
const emailSingleContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(emailSingle)

export default emailSingleContainer