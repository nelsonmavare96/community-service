import { connect } from 'react-redux'
import email from '../../components/Main/Email'
import * as mainAppActions from '../../actions/mainAppAction'


const mapStateToProps = state => {
    return {
        globalData: state.main.globalData,
        user: state.main.userData,
        emails: state.main.emails,
        fetch: state.main.fetch
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(mainAppActions.fetchGetEmail())
    return {
        onSetEmail: (email) => {
            dispatch(mainAppActions.setEmail(email))
        }
    }
}
const emailContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(email)

export default emailContainer