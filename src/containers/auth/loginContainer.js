import { connect } from 'react-redux'
import authForm from '../../components/Auth/Login'
import * as mainAppActions from '../../actions/mainAppAction'
import * as countriesAction from '../../actions/countriesAction'
import * as roleActions from '../../actions/rolesAction'
import * as pathologiesActions from "../../actions/pathologiesAction"
import * as classificationsActions from '../../actions/classificationsAction'

const mapStateToProps = state => {
    return {
        globalData: state.main.globalData,
        user: state.main.userData,
        userLogged: state.main.userLogged,
        authState: state.main.globalData.authState,
        countries: state.countries.countries,
        fetch: state.main.fetch,
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(countriesAction.fetchCountries())
    dispatch(roleActions.fetchRoles())
    //dispatch(mainAppActions.resetFetch())
    dispatch(pathologiesActions.fetchPathologies())
    dispatch(classificationsActions.fetchClassifications())
    return {
        onHandleAuthStateChange: (text) => {dispatch(mainAppActions.handleAuthStateChange(text))},
        onHandleUserInputChange: (e) =>{dispatch(mainAppActions.handleUserInputChange(e))},
        onHandleUserProfileInputChange: (event,id) => {dispatch(mainAppActions.handleUserProfileInputChange(event))},
        onFetchAuthUser: (user) => {dispatch(mainAppActions.fetchAuthUser(user))},
        onResetFetch: () => {dispatch(mainAppActions.resetFetch())},
        onResetUserData: () => {dispatch(mainAppActions.resetUserData())},
    }
}
const authFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(authForm)

export default authFormContainer