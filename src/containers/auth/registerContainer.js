import { connect } from 'react-redux'
import authForm from '../../components/Auth/Register'
import * as mainAppActions from '../../actions/mainAppAction'
import * as countriesAction from '../../actions/countriesAction'
import * as roleActions from '../../actions/rolesAction'
import * as classificationsActions from '../../actions/classificationsAction'
const mapStateToProps = state => {
    return {
        globalData: state.main.globalData,
        user: state.main.userData,
        userLogged: state.main.userLogged,
        authState: state.main.globalData.authState,
        countries: state.countries.countries,
        fetch: state.main.fetch,
        pathologies: state.pathologies.pathologies,
        classifications: state.classifications.classifications
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(mainAppActions.resetFetch())
    dispatch(classificationsActions.fetchClassifications())
    return {
        onResetFetch: () => {dispatch(mainAppActions.resetFetch())},
        onHandleAuthStateChange: (text) => {dispatch(mainAppActions.handleAuthStateChange(text))},
        onHandleUserInputChange: (e) =>{dispatch(mainAppActions.handleUserInputChange(e))},
        onHandleUserProfileInputChange: (event,id) => {dispatch(mainAppActions.handleUserProfileInputChange(event))},
        fetchCreateUser: (user) => {dispatch(mainAppActions.fetchCreateUser(user))},
        onFetchAuthUser: (user) => {dispatch(mainAppActions.fetchAuthUser(user))},
        onResetFetch: () => {dispatch(mainAppActions.resetFetch())},
        onResetUserData: () => {dispatch(mainAppActions.resetUserData())},
        onHandleUserMultiSelect: (array,state) =>{dispatch(mainAppActions.handleUserMultiSelect(array,state))}
    }
}
const authFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(authForm)

export default authFormContainer