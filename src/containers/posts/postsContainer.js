import { connect } from 'react-redux'
import posts from '../../components/Posts/Posts'
import * as postActions from '../../actions/postAction'
import * as userActions from '../../actions/userAction'

const mapStateToProps = state => {
    return {
      userData: state.main.userData,
      posts: state.post.posts,
      my_liked_posts: state.post.my_liked_posts
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchPosts:(path) =>{ dispatch(postActions.fetchPosts(path))},
        onLikePost:(id) => {},
        onRemoveLikePost:(id) => {},
        onShowPost:(post) => {dispatch(postActions.showPost(post))},
        onShowPublicUserProfile: (user) => {}
    }
}

const timeLineContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(posts)

export default timeLineContainer