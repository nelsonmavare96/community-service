import { connect } from 'react-redux'
import NewPost from '../../components/Posts/NewPost'
import * as postActions from '../../actions/postAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      post: state.post.post,
      my_liked_posts: state.post.my_liked_posts,
      categories: [],
      fetch: state.post.fetch_post
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(postActions.cancelPostPost())
    dispatch(postActions.resetFetchPost())
    return {
      onSaveNewPost: (post) => {dispatch(postActions.fetchSaveNewPost(post))},
      onPostInputChange: (e) => {dispatch(postActions.onPostHandleInputChange(e));},
      onDeletePostPicture: (index) => {dispatch(postActions.deletePostPictures(index))},
      onCancelPostPost: () => {dispatch(postActions.cancelPostPost())}
    }
}

const DetailPostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPost)

export default DetailPostContainer