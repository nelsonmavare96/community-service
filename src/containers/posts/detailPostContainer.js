import { connect } from 'react-redux'
import DetailPost from '../../components/Posts/DetailPost'
import * as postActions from '../../actions/postAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      post: state.post.post,
    }
}
const mapDispatchToProps = dispatch => {
    return {
      onDownloadFile: (fileName) =>{dispatch(postActions.downloadFile(fileName))}
    }
}

const DetailPostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailPost)

export default DetailPostContainer