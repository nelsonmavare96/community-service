import { connect } from 'react-redux'
import classificationsList from '../../components/Classifications/ClassificationList'
import * as classificationsAction from '../../actions/classificationsAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      classifications: state.classifications.classifications
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(classificationsAction.fetchClassifications())
    return {
        onHandleInputChange: (event,id) => {dispatch(classificationsAction.handleInputChange(event,id))},
        onAddNewClassification: () => {dispatch(classificationsAction.addNewClassification())},
        onCancelNewClassification: (id) => {dispatch(classificationsAction.deleteClassification(id,true))},
        onDeleteClassification: (id) => {dispatch(classificationsAction.fetchDeleteClassification(id))},
        onEditClassification: (id) => {dispatch(classificationsAction.editClassification(id))},
        onCancelEditClassification: (text,id) => {dispatch(classificationsAction.cancelEditClassification(text,id))},
        onHandleStatusInputChange: (event,id) => {dispatch(classificationsAction.handleStatusInputChange(event,id))},
        onUpdateClassification: (classification) => {dispatch(classificationsAction.fetchUpdateClassification(classification))},
        onSaveNewClassification: (classification) => {dispatch(classificationsAction.fetchCreateClassification(classification))},
    }
}

const classificationsListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(classificationsList)

export default classificationsListContainer