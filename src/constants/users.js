export default [
    {
        id: 1,
        username: 'jose',
        email: 'j@gmail.com',
        status: 1,
        role_id: 1,
        user_type:"person",
        clasification:"-",
        role:{
            id: 2,
            name: "Administrador",
            description: "Usuario encargado del manejo y control de la apliación y de la data", 
            status: 1
        },

        social_reason:"456789",
        name: 'Jose',
        lastname: "Camacaro",
        instruction_degree:"instruction degree",
        job_type:"job type",
        job:"job",
        current_study:"current study",
        birthdate: "1996-07-15",
        telephone: "1548878",
        country_id:1,
        country: {id:1},
        facebook: "notengo",
        twitter: "tampoctengo",
        instagram: "menostengo",

        pathologies:[
            {
                id: 1,
                value: 1,
                name: "Patología 1",
                label: "Patología 1",
                status: 1
            },
            {
                id: 3,
                value:3,
                name: "Patología 3",
                label: "Patología 3",
                status: 1
            }
        ]
    },
    {
        id: 2,
        username: 'KidneyClick',
        email: 'kidneyclick@gmail.com',
        status: 1,
        role_id: 1,
        user_type:"institution",
        clasification:"-",
        role:{
            id: 1,
            name: "Administrador",
            description: "Usuario encargado del manejo y control de la apliación y de la data", 
            status: 1
        },

        social_reason: "159789",
        name: 'KidneyClick',
        telephone: "1548878123",
        country_id:1,
        country: {id:1},
        website:"nada por aqui",
        facebook: "notengo",
        twitter: "tampoctengo",
        instagram: "menostengo",
        address:"no tengo",
        pathologies:[
            {
                id: 2,
                value: 2,
                name: "Patología 2",
                label: "Patología 2",
                status: 1
            },

        ]
    }

];