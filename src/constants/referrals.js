export default [
    {
        id: 1,
        username: 'jose',
        email: 'j@gmail.com',
        status: 1,
        role_id: 1,
        user_type:1,
        clasification:"-",
        role:{
            id: 1,
            name: "Administrador",
            description: "Usuario encargado del manejo y control de la apliación y de la data", 
            status: 1
        },
        id: 1,
        social_reason:"123456",
        name: 'Jose',
        lastname: "Camacaro",
        birthdate: "15/07/1996",
        telephone: "1548878",
        country: {id:1},
        facebook: "notengo",
        twitter: "tampoctengo",
        instagram: "menostengo",
        pathologies: [
            {
                id: 2,
                value:2,
                name: "Patología 2",
                label: "Patología 2",
                status: 2
            },
        ]
    },
    {
        id: 1,
        username: 'KidneyClick',
        email: 'kidneyclick@gmail.com',
        status: 1,
        role_id: 1,
        user_type:"institution",
        clasification:"-",
        role:{
            id: 1,
            name: "Administrador",
            description: "Usuario encargado del manejo y control de la apliación y de la data", 
            status: 1
        },
        social_reason: "456789",
        name: 'KidneyClick',
        birthdate: "15/07/1996",
        telephone: "1548878",
        country: {id:1},
        website:"nada por aqui",
        facebook: "notengo",
        twitter: "tampoctengo",
        instagram: "menostengo",
        address:"no tengo",
        
    }

];