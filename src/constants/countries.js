export default [
    {
        id: 1,
        value:1,//igual que el id
        name: "Venezuela", 
        label: "Venezuela",
        status: 1
    },
    {
        id: 2,
        value:2,
        name: "Colombia", 
        label: "Colombia",
        status: 2
    },
    {
        id: 3,
        value:3,
        name: "Argentina", 
        label: "Argentina",
        status: 1
    }
];