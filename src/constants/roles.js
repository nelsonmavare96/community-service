export default [
    {
        id: 1,
        name: "Persona",
        description: "", 
        status: 1
    },
    {
        id: 2,
        name: "Institución",
        description: "",
        status: 1
    },
    {
        id: 3,
        name: "Administrador",
        description: "Otro rol aun no definido", 
        status: 2
    }
];