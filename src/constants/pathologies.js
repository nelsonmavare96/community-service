export default [
    {
        id: 1,
        value: 1,
        name: "Patología 1",
        label: "Patología 1",
        status: 1
    },
    {
        id: 2,
        value:2,
        name: "Patología 2",
        label: "Patología 2",
        status: 1
    },
    {
        id: 3,
        value:3,
        name: "Patología 3", 
        label: "Patología 3",
        status: 1
    }
];