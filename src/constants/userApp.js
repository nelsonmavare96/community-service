export default 
{
    id: 1,
    username: "nema123",
    email: "nema@gmail.com",
    password: 12345,
    confirmPassword: 12345,
    status: 1,
    user_type:"person",
    role:{
        id: 1,
        name: "Administrador",
        description: "Usuario encargado del manejo y control de la apliación y de la data", 
        status: 1
    },

    name: "Nelson",
    lastname: "Mavare",
    birthdate: "1996-07-30",
    sex: "M",
    phone: "04245558208",
    image: "https://vignette.wikia.nocookie.net/avatar/images/f/f0/Iroh.png/revision/latest?cb=20170628053044&path-prefix=es",
    country: {
        id: 1,
        name: "Venezuela", 
        status: 1
    },
    facebook: "Jeferson96",
    twitter: "@Jefer96",
    instagram: "JefersonJAC96"
    
}