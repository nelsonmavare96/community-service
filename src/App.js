import { STATE_LOGIN, STATE_SIGNUP, RECOVER_PASSWORD } from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout, MainLayoutAdmin } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
//import AuthPage from './containers/main/authFormContainer';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './styles/reduction.scss';
import {connect} from 'react-redux'

import Hammer from 'hammerjs'
import * as $ from 'jquery'

const UserProfile = React.lazy(() => import('containers/main/userProfileContainer'));
const CountryList = React.lazy(() => import('containers/countries/countryContainer'));
const ClassificationsList = React.lazy(() => import('containers/classifications/classificationContainer'));
const RoleList = React.lazy(() => import('containers/roles/rolContainer'));

const UserList = React.lazy(() => import('containers/users/userContainer'));
const AuthForm = React.lazy(() => import('containers/main/authFormContainer'));
const ReferralsList = React.lazy(() => import('containers/referrals/referredContainer'));
const PathologiesList = React.lazy(() => import('containers/pathologies/pathologiesContainer'));
const Login = React.lazy(() => import('containers/auth/loginContainer'));
const Register = React.lazy(() => import('containers/auth/registerContainer'));
const RecoverPassword = React.lazy(() => import('containers/auth/recoverPasswordContainer'));
const EmailList = React.lazy(() => import('containers/main/emailContainer'));
const EmailSingle = React.lazy(() => import('containers/main/emailSingleContainer'));
const Posts = React.lazy(() => import('containers/posts/postsContainer'));
const NewPost = React.lazy(() => import('containers/posts/newPostContainer'));
const DetailPost = React.lazy(() => import('containers/posts/detailPostContainer'));

/* const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
}; */

class App extends React.Component {

  detRole = (roles,string) =>{
		let role_ = null
		roles.map((role)=>{
			if(role.name == string)
				role_ = role
    })
    return role_
    
    }

  render() {
    const {globalData,user,roles} = this.props
    return (
      <BrowserRouter>
        <GAListener>
          {!globalData.isLogged?
          <Switch>
              <EmptyLayout>
                <React.Suspense fallback={<PageSpinner />}>
                  <Route exact path="/" component={Login} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/recover-password" component={RecoverPassword} />
                </React.Suspense>
              </EmptyLayout>
        </Switch>
:
      <Switch>
            {user.user_type== 3?
            <MainLayoutAdmin user={user} breakpoint={this.props.breakpoint}>
              <React.Suspense fallback={<PageSpinner />}>
                <Route exact path="/userProfile" component={UserProfile} />
                <Route exact path="/" render={() => <UserList />} />
                <Route exact path="/users" render={() => <UserList />} />
                <Route exact path="/roles" render={() => <RoleList />} />
                {/* <Route exact path="/countries" render={() => <CountryList />} /> */}
                <Route exact path="/pathologies" render={() => <PathologiesList/>} />
                <Route exact path="/classifications" render={() => <ClassificationsList/>} />
                <Route exact path="/email" render={() => <EmailList/>} />
                <Route exact path="/email/single" render={() => <EmailSingle/>} />
                <Route exact path="/posts" render={() => <Posts/>} />
                <Route exact path="/myReferral" render={() => <Posts/>} />
                <Route exact path="/newPost" render={() => <NewPost/>} />
                <Route exact path="/detail-post" render={() => <DetailPost/>} />
              </React.Suspense>
            </MainLayoutAdmin>
            :     
            (
            <Switch>
              <MainLayout user={user} breakpoint={this.props.breakpoint}>   
                <React.Suspense fallback={<PageSpinner />}>
                  <Route exact path="/" render={() => <ReferralsList />} />
                  <Route exact path="/referrals" render={() => <ReferralsList />} />
                  <Route exact path="/userProfile" component={UserProfile} />
                  <Route exact path="/posts" render={() => <Posts/>} />
                  <Route exact path="/myReferral" render={() => <Posts/>} />
                  <Route exact path="/detail-post" render={() => <DetailPost/>} />
                </React.Suspense>
              </MainLayout>
            </Switch>)
            }
          </Switch>
          }
        </GAListener>
      </BrowserRouter>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

const mapStateToProps = state => {
  return {
    globalData: state.main.globalData,
    user: state.main.userData,
    userLogged: state.main.userLogged,
    roles: state.roles.roles
  }
}

const mapDispatchToProps = dispatch => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
