//import moment from "moment";

export function transformUserType(user_type){
    if(user_type==2)
        return "Institución"
    else if(user_type==1)
        return "Persona"
    return "Administrador"
}
/************************************************************************************************* */
export function filterUsers(users,pathologies){
    let filtered_users = []
    if(users && pathologies && pathologies.length>0){
            for(let user of users){
                let hasOnePathology = false
                if(user.pathologies && user.pathologies.length!=0)
                    for(let pathology_user of user.pathologies){
                        for(let pathology of pathologies){
                            if(pathology_user.id==pathology.id && !hasOnePathology){
                                filtered_users.push(user)
                                hasOnePathology = true
                            } 
                            if (hasOnePathology) break;                         
                        }
                        if (hasOnePathology) break;  
                    }
            }
            return filtered_users
    }
    return users
}

/************************************************************************************************* */
export function validateEmail(text){
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      //this.setState({ email: text })
      return false;
    }
    return true
}
/************************************************************************************************* */
//ordenar arreglo segun palabra clave y tipo
export function orderArray(array_json,key,type) {
  if(type=="ASC")
      array_json.sort(function (a, b) {
      return a[key] > b[key];
      });
  else if(type=="DESC")
    array_json.sort(function (a, b) {
      return a[key] < b[key];
      });
  return array_json
}
/************************************************************************************************* */
//filtrar arreglo segun nombre
export function filterArrayByName(array_json,key, value){
  var filteredArrayAux = []
  if(array_json.length>0 && value.length>0){
      for(var i = 0; i < array_json.length; i++){
      if(array_json[i][key].substr(0,value.length).toLowerCase() == value.toLowerCase())
          filteredArrayAux.push(array_json[i])
      }
  }
  else
      return array_json
  return filteredArrayAux
}
/************************************************************************************************* */
export function transformStringGender(gender){
    switch(gender[0]){
        case "M":
            return "Masculino"
        case "F":
            return "Femenino"
    }
}
/************************************************************************************************* */
export async function fetchTimeOut (url, options, timeout) {
    return await Promise.race([
        fetch(url, options),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('timeout')), timeout)
        )
    ]);
}
/************************************************************************************************* */
export function validateEmptyFields(fields){
    if(fields.length>0){
        for (var i=0; i<fields.length; i++) {
            if(fields[i]=="" || fields[i]=={} || fields[i]==null || fields[i].length==0 )
                return false
        }
        return true
    }
    return false
}

/************************************************************************************************* */
/*export function diffDates(date1,date2){
	var a = date1//moment(date1);
	var b = date2//moment(date2);

	var years = a.diff(b, 'year');
	//b.add(years, 'years');
	var months = a.diff(b, 'months');
	//b.add(months, 'months');
	var days = a.diff(b, 'days');

    return {"days":Math.abs(days),"months":Math.abs(months),"years":Math.abs(years)}
}*/
/************************************************************************************************* */
export function findValue(key,value,data){
    if(data.length>0){
        for (var i=0; i<data.length; i++) {
            if(data[i][key]==value)
                return data[i]
        }
    }
    return null
}
/************************************************************************************************* */
export function detResponseStatusErr(status){
    if(status === 400 || status === 404 || status === 409 || status === 422 || status === 500 || status === 503)
        return true
    return false
}
