import * as postsActions from '../actions/postAction'

const initialState = {
    fetch_post: {
      is_fetching:false,
      message:"",
      status:0, //0 null, 1 succeed, 2 failed
      _fetch:""
    },
    posts:[],
    post:{
      files:[],
      //prev_files:[]
    },

  }
  
  const postsReducer = (state = initialState, action) => {
  
    switch(action.type) {


      case postsActions.HANDLE_POST_INPUT_CHANGE:
        if(action.event.target.name == "files"){
          //let prev_files = state.post.prev_files
          let files = state.post.files

          for (var i in action.event.target.files){
            if(action.event.target.files[i].name != null && action.event.target.files[i].name != undefined && action.event.target.files[i].name != "item"){
              //prev_files.push(URL.createObjectURL(action.event.target.files[i]))
              //files.push(action.event.target.files[i])
              //prev_files[0] = URL.createObjectURL(action.event.target.files[i])
              files[0] = action.event.target.files[i]
            }
          }

          if(files.length < 6){
            return Object.assign({}, state, {
              post: Object.assign({}, state.post,{
                files: files,
                //prev_files: prev_files
              })
            }) 
          }else{
            alert("Numero máximo de imagénes excedido")
          }
        }else{
          return Object.assign({}, state, {
            post: Object.assign({}, state.post,{
              [action.event.target.name]: action.event.target.value
            })
          }) 
        }
      
      case postsActions.SHOW_POST:
        return Object.assign({}, state, {
          post: action.post
        }) 

      case postsActions.CANCEL_POST_POST:
        return Object.assign({}, state, {
          post: Object.assign({}, state.post, {
            id: "",
            title: "",
            content:"",
            files: [],
          })
        })

      case postsActions.DELETE_POST_PICTURES:
        return Object.assign({}, state, {
          post: Object.assign({}, state.post,{
            files: state.post.files.filter((image, index) => index != action.index),
            //prev_files: state.post.prev_files.filter((image, index) => index != action.index)
          }) 
        }) 

      case postsActions.IS_FETCHING:
        return Object.assign({}, state, {
          fetch_post: Object.assign({}, state.fetch,{
            is_fetching:true,
            message:"",
            status:0, //0 null, 1 succeed, 2 failed
            _fetch:action.fetch
          })
        })

      case postsActions.RESET_FETCH:
        return Object.assign({}, state, {
          fetch_post: Object.assign({}, state.fetch,{
            is_fetching:false,
            message:"",
            status:0, //0 null, 1 succeed, 2 failed
            _fetch:""
          })
        })
        
      case postsActions.FETCH_POST_FAILED:
        return Object.assign({}, state, {
          fetch_post: Object.assign({},{
            is_fetching:false,
            message:action.message,
            status:2, //0 null, 1 succeed, 2 failed
            _fetch:action.fetch
          })
        })

      case postsActions.FETCH_SAVE_NEW_POST_SUCCEEDED:
        return Object.assign({}, state, {
          fetch_post: Object.assign({},{
            is_fetching:false,
            message:"",
            status:1, //0 null, 1 succeed, 2 failed
            _fetch:action.fetch
          }),
          post: action.post
        })

      case postsActions.FETCH_POSTS_SUCCEEDED:
        return Object.assign({}, state, {
          posts: action.posts,
          fetch_post: Object.assign({},{
            is_fetching:false,
            message:"",
            status:1, //0 null, 1 succeed, 2 failed
            _fetch:action.fetch
          }),
        })
        
      default:
        return state
    }
  }
  
  export default postsReducer