import * as referralsActions from '../actions/referralsActions'

const initialState = {
  isFetching: false,
  referredLogged: false,
  referrals:[],
  referred_original_values:null,
  referred:{
    id: "",
    referredname: "",
    email: "",
    password: "",
    confirmPassword: "",
    status: "",
    role_id: "",
    name: "",
    lastname: "",
    birthdate: "",
    sex: "",
    phone: "",
    country: {},
    facebook: "",
    twitter: "",
    instagram: "",
    
  }
}

const mainReducer = (state = initialState, action) => {

  switch(action.type) {

    case referralsActions.HANDLE_REFERRED_INPUT_CHANGE:
      return Object.assign({}, state, {
        referred: Object.assign({}, state.referred,{
          [action.event.target.name]: action.event.target.value
        })
      })
    case referralsActions.HANDLE_REFERRED_COUNTRY_CHANGE:
      return Object.assign({}, state, {
        referred: Object.assign({}, state.referred,{

          country: Object.assign({}, state.referred.country,{
            [action.event.target.name]: action.event.target.value
          })
       
        })
      })
    case referralsActions.HANDLE_REFERRED_ROLE_CHANGE:
      return Object.assign({}, state, {
        referred: Object.assign({}, state.referred,{
          role: Object.assign({}, state.referred.role,{
            [action.event.target.name]: action.event.target.value
          })          
        })
      })

    case referralsActions.FETCH_REFERRALS_SUCCEEDED:
      return Object.assign({},state,{
        referrals: action.referrals
      })
      
    case referralsActions.SHOW_REFERRED:
      return Object.assign({},state,{
          referred: action.referred
          
      })

    case referralsActions.EDIT_REFERRED:
      return Object.assign({}, state, {
          referred: action.referred,
          referred_original_values: action.referred
      })

    case referralsActions.RESTORE_REFERRED_DATA:
      return Object.assign({}, state, {
          referred: action.referred
      })

    case referralsActions.UPDATE_REFERRED:
    console.log(state.referrals)
      return Object.assign({},state,{
        referrals: state.referrals.map((referred) => {
          if(referred.id == action.referred.id){
            return Object.assign({}, action.referred)
          }
          return referred
        }),
      })

    case referralsActions.SAVE_NEW_REFERRED:
      return Object.assign({},state,{
        referrals: state.referrals.map((referred) => {
          if(referred.id == action.new_referred_id){
            return Object.assign({}, action.referred, {
              isNew: false
            })
          }
          return referred
        }),
      })
      
    case referralsActions.RESTORE_REFERRED:
      return Object.assign({}, state, {
          referred: Object.assign({},{
            id: "",
            referredname: "",
            email: "",
            password: "",
            confirmPassword: "",
            status: "",
            role_id: "",
            name: "",
            lastname: "",
            birthdate: "",
            sex: "",
            phone: "",
            country: {
            },
            facebook: "",
            twitter: "",
            instagram: "",

          })
      })
    
    case referralsActions.DELETE_REFERRED:
        return Object.assign({}, state, {
          referrals: state.referrals.map((referred) => {
            if(referred.id == action.id){
              return Object.assign({}, referred, {
                status: 2
                })
            }
            return referred
          }),
        })
      

      case referralsActions.FETCH_REQUEST_REFERRALS_SUCCEEDED:
        return Object.assign({}, state, {
          referrals: action.referrals,
        })

      case referralsActions.SHOW_PUBLIC_REFERRED_PROFILE_SUCCEEDED:
        return Object.assign({},state,{
          referred: action.referred
        })

    default:
      return state
  }
}

export default mainReducer