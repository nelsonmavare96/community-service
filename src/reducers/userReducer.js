import * as userActions from '../actions/userAction'

const initialState = {
  isFetching: false,
  userLogged: false,
  users:[],
  user_original_values:null,
  user:{
    id: "",
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    status: "",
    role_id: "",
    name: "",
    lastname: "",
    birthdate: "",
    sex: "",
    phone: "",
    country: {},
    facebook: "",
    twitter: "",
    instagram: ""
  }
}

const mainReducer = (state = initialState, action) => {

  switch(action.type) {

    case userActions.SET_FILTERED_USERS:
      return Object.assign({}, state, {
        filtered_users: action.users
      })

    case userActions.HANDLE_USER_INPUT_CHANGE:
      return Object.assign({}, state, {
        user: Object.assign({}, state.user,{
          [action.event.target.name]: action.event.target.value
        })
      })
    case userActions.HANDLE_USER_COUNTRY_CHANGE:
      return Object.assign({}, state, {
        user: Object.assign({}, state.user,{
            country: Object.assign({}, state.user.country,{
              [action.event.target.name]: action.event.target.value
            })
        })
      })
    case userActions.HANDLE_USER_ROLE_CHANGE:
      return Object.assign({}, state, {
        user: Object.assign({}, state.user,{
          role: Object.assign({}, state.user.role,{
            [action.event.target.name]: action.event.target.value
          })          
        })
      })

    case userActions.HANDLE_USER_MULTI_SELECT_INPUT:
      return Object.assign({}, state, {
        user: Object.assign({}, state.user,{
          [action.state]: action.array,
        })
      })

    case userActions.FETCH_USERS_SUCCEEDED:
      return Object.assign({},state,{
        users: action.users,
        filtered_users: action.users
      })
      
    case userActions.SHOW_USER:
      return Object.assign({},state,{
          user: action.user
          
      })

    case userActions.EDIT_USER:
      return Object.assign({}, state, {
          user: action.user,
          user_original_values: action.user
      })

    case userActions.RESTORE_USER_DATA:
      return Object.assign({}, state, {
          user: action.user
      })

    case userActions.UPDATE_USER:
      return Object.assign({},state,{
        users: state.users.map((user) => {
          if(user.id === action.user.id){
            return Object.assign({}, action.user)
          }
          return user
        }),
        filtered_users: state.users.map((user) => {
          if(user.id === action.user.id){
            return Object.assign({}, action.user)
          }
          return user
        }),
      })

    case userActions.SAVE_NEW_USER:
      return Object.assign({},state,{
        users: state.users.map((user) => {
          if(user.id === action.new_user_id){
            return Object.assign({}, action.user, {
              isNew: false
            })
          }
          return user
        }),
        filtered_users: state.users.map((user) => {
          if(user.id === action.new_user_id){
            return Object.assign({}, action.user, {
              isNew: false
            })
          }
          return user
        }),
      })
      
    case userActions.RESTORE_USER:
      return Object.assign({}, state, {
          user: Object.assign({},{
            username: "",
            email: "",
            password: "",
            confirmPassword: "",
            status: "",
            role_id: "",
              name: "",
              lastname: "",
              birthdate: "",
              sex: "",
              phone: "",
              country: {
              },
              facebook: "",
              twitter: "",
              instagram: ""
            })
      })
    
    case userActions.DELETE_USER:
        return Object.assign({}, state, {
          users: state.users.map((user) => {
            if(user.id === action.id){
              return Object.assign({}, user, {
                status: 2
                })
            }
            return user
          }),
          filtered_users: state.filtered_users.map((user) => {
            if(user.id === action.id){
              return Object.assign({}, user, {
                status: 2
                })
            }
            return user
          }),
        })
      

      case userActions.FETCH_REQUEST_USERS_SUCCEEDED:
        return Object.assign({}, state, {
          users: action.users,
          filtered_users: action.users
        })

      case userActions.SHOW_PUBLIC_USER_PROFILE_SUCCEEDED:
        return Object.assign({},state,{
          user: action.user
        })

    default:
      return state
  }
}

export default mainReducer