import * as classificationsActions from '../actions/classificationsAction'

const initialState = {
    fetch: {
      is_fetching:"",
      message:"",
      result_succeeded:null
    },
    classifications:[],
    classification:{
        name:"",
        status:"",
    },
  }
  
  const classificationsReducer = (state = initialState, action) => {
  
    switch(action.type) {
  
      case classificationsActions.HANDLE_CLASSIFICATION_INPUT_CHANGE:
        return Object.assign({}, state, {
          classifications: state.classifications.map((classification) => {
            if(classification.id == action.id){
              return Object.assign({}, classification, {
                [action.event.target.name]: action.event.target.value
              })
            }
            return classification
          }),
        })
       
      

      case classificationsActions.HANDLE_STATUS_INPUT_CHANGE:
        return Object.assign({}, state, {
          classifications: state.classifications.map((classification) => {
            if(classification.id == action.id){
              return Object.assign({}, classification, {
                status:action.text
              })
            }
            return classification
          }),
        })

      case classificationsActions.UPDATE_CLASSIFICATION:
        return Object.assign({},state,{
          classifications: state.classifications.map((classification) => {
            if(classification.id == action.classification.id){
              return Object.assign({}, action.classification, {
                isEditing: false
              })
            }
            return classification
          }),
        })

      case classificationsActions.EDIT_CLASSIFICATION:
        return Object.assign({}, state, {
          classifications: state.classifications.map((classification) => {
            if(classification.id == action.id){
              return Object.assign({}, classification, {
                originalName:classification.name,
                originalStatus: classification.status,
                isEditing:true
              })
            }
            return classification
          }),
        })

      case classificationsActions.SAVE_NEW_CLASSIFICATION:
        return Object.assign({},state,{
          classifications: state.classifications.map((classification) => {
            if(classification.id == action.new_classification_id){
              return Object.assign({}, action.classification, {
                isNew: false,
                status: 1
              })
            }
            return classification
          }),
        })
      case classificationsActions.CANCEL_EDIT_CLASSIFICATION:
        return Object.assign({}, state, {
          classifications: state.classifications.map((classification) => {
            if(classification.id == action.id){
              return Object.assign({}, classification, {
                name: action.text,
                status: classification.originalStatus,
                isEditing:false
              })
            }
            return classification
          }),
        })

      case classificationsActions.ADD_NEW_CLASSIFICATION:
         let newClassification = {
          id: Math.random(),
          name: "",
          status: 1, 
          isNew: true
        }
        return Object.assign({}, state, {
          classifications: [...state.classifications,newClassification],
        })

      case classificationsActions.DELETE_CLASSIFICATION:
        if(action.cancelNew)
          return Object.assign({}, state, {
            classifications: state.classifications.filter(classification => classification.id !== action.id)
          })
        else
          return Object.assign({}, state, {
            classifications: state.classifications.map((classification) => {
              if(classification.id == action.id){
                return Object.assign({}, classification, {
                  status: 2
                  })
              }
              return classification
            }),
          })
      
      case classificationsActions.FETCH_REQUEST_CLASSIFICATIONS_SUCCEEDED:
        return Object.assign({}, state, {
          classifications: action.classifications,
        })
        
      default:
        return state
    }
  }
  
  export default classificationsReducer