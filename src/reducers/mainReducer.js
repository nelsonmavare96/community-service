import * as mainAppActions from '../actions/mainAppAction'
import {dateParse} from '../components/Utils/dateParse'
import moment from 'moment'

const initialState = {
  fetch: {
    is_fetching:false,
    message:"",
    status:0, //0 null, 1 succeed, 2 failed
    _fetch:""
  },
  userLogged: false,
  emails:[],
  email: {},
  userData: {
    email: "",
    password:"",
    passwordConfirm:"",
    country_id:"",//caso login
      country_id:"",
      username:"",
      name:"",
      lastname: "",
      image: "",
      twitter: "",
      instagram: "",
      facebook: "",
      phone: "",
      sex: "",
      role: "",
      status: "",
      description: "",
    role:{
      id:""
    }
  },
  globalData: {
    isLogged:false,
    roleUser:"",
    authState:"STATE_LOGIN", //STATE_LOGIN, STATE_SIGNUP , RECOVER_PASSWORD
    token:""
  },
  order: {
    prictures: [],
    prev_pictures: []
  },
  change_password:{
    current_password:"",
    new_password:"",
    new_password_confirm:""
  },
}

const mainReducer = (state = initialState, action) => {

  switch(action.type) {

    case mainAppActions.RESET_MAIN_FETCH:
      return Object.assign({}, state, {
        fetch: Object.assign({}, state.fetch,{
          is_fetching:false,
          message:"",
          status:0
        })
      })

    case mainAppActions.HANDLE_MAIN_USER_INPUT_CHANGE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          [action.event.target.name]: action.event.target.value,
        })
      })
    
    case mainAppActions.HANDLE_MAIN_USER_MULTI_SELECT:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          [action.state]: action.array,
        })
      })

    case mainAppActions.HANDLE_CHANGE_PASSWORD_INPUT_CHANGE:
      return Object.assign({}, state, {
        change_password: Object.assign({}, state.change_password,{
          [action.event.target.name]: action.event.target.value,
        })
      })

    case mainAppActions.HANDLE_USER_PROFILE_INPUT_CHANGE:
      return Object.assign({}, state, {
        userData_changed_values: Object.assign({}, state.userData_changed_values,{
            [action.event.target.name]: action.event.target.value
        })
      })
    case mainAppActions.UPDATE_USER_PROFILE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData_changed_values),
      })
    
    case mainAppActions.HANDLE_AUTH_STATE_CHANGE:
      return Object.assign({},state, {
        globalData: Object.assign({}, state.globalData,{
          authState: action.text
        })  
      })

    case mainAppActions.REQUEST_IN_PROCESS:
      return Object.assign({},state,{
        fetch: Object.assign({},{
          is_fetching:true,
          message:"",
          status:0, //0 null, 1 succeed, 2 failed
          _fetch:action.fetch
        })
      })
    
    case mainAppActions.FETCH_EMAIL_SINGLE_SUCCEEDED:
      return Object.assign({}, state, {
        email: Object.assign({},state.email,{
          content: action.content
        }),
        fetch: Object.assign({},state.fetch,{
          is_fetching: false,
          message:action.message,
          status:1
        })
      })

    case mainAppActions.AUTH_USER_SUCCEEDED:
      return Object.assign({}, state, {
        userData: action.user,
        userData_changed_values: action.user,
        globalData: Object.assign({}, state.globalData,{
          isLogged:true,
          token: action.token
        }),
        fetch: Object.assign({},state.fetch,{
          is_fetching: false,
          message:action.message,
          status:1
        })
      })

    case mainAppActions.REGISTER_USER_SUCCEEDED:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.user,{
          email: "",
          password:"",
          passwordConfirm:"",
        }),
        fetch: Object.assign({},state.fetch,{
          is_fetching: false,
          message:action.message,
          status:1,
          _fetch:"fetchCreateUser"
        })
      })

    case mainAppActions.REQUEST_FAILED:
      return Object.assign({}, state, {
        fetch: Object.assign({},state.fetch,{
          is_fetching: false,
          message:action.message,
          status:2,
          fetch: action.fetch
        })
      })

    case mainAppActions.RESET_CHANGE_PASSWORD:
      return Object.assign({}, state, {
        change_password:{
          current_password:"",
          new_password:"",
          new_password_confirm:""
        }
      })
    
    case mainAppActions.RESET_USER_DATA:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.user,{
          email: "",
          password:"",
          passwordConfirm:"",
          country_id:"",//caso login
          country_id:"",
          username:"",
          name:"",
          lastname: "",
          image: "",
          twitter: "",
          instagram: "",
          facebook: "",
          phone: "",
          sex: "",
          role: "",
          status: "",
          description: ""
        }),
        userData_changed_values: {},
        globalData: Object.assign({}, state.globalData,{
          isLogged:false,
          roleUser:"",
        }),
        change_password:{
          current_password:"",
          new_password:"",
          new_password_confirm:""
        }
      })
    
    case mainAppActions.SAVE_USER_PROFILE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData_changed_values),
      })

    case mainAppActions.CANCEL_EDIT_PROFILE:
      return Object.assign({}, state, {
        userData_changed_values: Object.assign({}, state.userData),
      })

    case mainAppActions.HANDLE_INPUT_FILE_CHANGE:
      if(action.event.target.name == "pictures"){
        let prev_pictures = state.order.prev_pictures
        let pictures = state.order.pictures

        for (var i in action.event.target.files){
          if(action.event.target.files[i].name != null && action.event.target.files[i].name != undefined && action.event.target.files[i].name != "item"){
            prev_pictures.push(URL.createObjectURL(action.event.target.files[i]))
            pictures.push(action.event.target.files[i])
          }
        }

        if(prev_pictures.length < 6){
          return Object.assign({}, state, {
            order: Object.assign({}, state.order,{
              pictures: pictures,
              prev_pictures: prev_pictures
            })
          }) 
        }else{
          alert("Numero máximo de imagénes excedido")
        }
      }

    case mainAppActions.REQUEST_FINISH_PURCHASE_SUCCEDED:
      return Object.assign({}, state, {
        order: action.purchase
      })
      
    case mainAppActions.REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData, {
            image: action.image
        })
      })

    case mainAppActions.FETCH_GET_EMAIL_SUCCEEDED:
      return Object.assign({}, state, {
        emails: action.emails,
        fetch: Object.assign({},state.fetch,{
          is_fetching: false,
          message:'',
          status:1
        })
      })

    case mainAppActions.SET_EMAIL:
      return Object.assign({}, state, {
        email: action.email,
      })

    default:
      return state
  }
}

export default mainReducer