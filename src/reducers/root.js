import { combineReducers } from 'redux'
import mainReducer from './mainReducer'
import userReducer from './userReducer'
import countriesReducer from './countriesReducer'
import rolesReducer from './rolesReducer'
import referredReducer from './referredReducer'
import pathologiesReduction from './pathologiesReducer'
import classificationsReduction from './classificationsReducer'
import postReducer from './postReducer'

const rootReducer = combineReducers({
  
	  user: userReducer,
	  countries: countriesReducer,
	  roles: rolesReducer,
	  main: mainReducer,
	  referrals: referredReducer,
	  pathologies: pathologiesReduction,
	  classifications: classificationsReduction,
	  post: postReducer
  
})

export default rootReducer;