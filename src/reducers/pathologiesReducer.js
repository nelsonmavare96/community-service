import * as pathologiesActions from '../actions/pathologiesAction'

const initialState = {
    fetch: {
      is_fetching:"",
      message:"",
      result_succeeded:null
    },
    pathologies:[],
    pathology:{
        name:"",
        status:"",
    },
  }
  
  const pathologiesReducer = (state = initialState, action) => {
  
    switch(action.type) {
  
      case pathologiesActions.HANDLE_PATHOLOGY_INPUT_CHANGE:
        return Object.assign({}, state, {
          pathologies: state.pathologies.map((pathology) => {
            if(pathology.id == action.id){
              return Object.assign({}, pathology, {
                [action.event.target.name]: action.event.target.value
              })
            }
            return pathology
          }),
        })
       
      

      case pathologiesActions.HANDLE_STATUS_INPUT_CHANGE:
        return Object.assign({}, state, {
          pathologies: state.pathologies.map((pathology) => {
            if(pathology.id == action.id){
              return Object.assign({}, pathology, {
                status:action.text
              })
            }
            return pathology
          }),
        })

      case pathologiesActions.UPDATE_PATHOLOGY:
        return Object.assign({},state,{
          pathologies: state.pathologies.map((pathology) => {
            if(pathology.id == action.pathology.id){
              return Object.assign({}, action.pathology, {
                isEditing: false
              })
            }
            return pathology
          }),
        })

      case pathologiesActions.EDIT_PATHOLOGY:
        return Object.assign({}, state, {
          pathologies: state.pathologies.map((pathology) => {
            if(pathology.id == action.id){
              return Object.assign({}, pathology, {
                originalName:pathology.name,
                originalStatus: pathology.status,
                isEditing:true
              })
            }
            return pathology
          }),
        })

      case pathologiesActions.SAVE_NEW_PATHOLOGY:
        return Object.assign({},state,{
          pathologies: state.pathologies.map((pathology) => {
            if(pathology.id == action.new_pathology_id){
              return Object.assign({}, action.pathology, {
                isNew: false,
                status: 1
              })
            }
            return pathology
          }),
        })
      case pathologiesActions.CANCEL_EDIT_PATHOLOGY:
        return Object.assign({}, state, {
          pathologies: state.pathologies.map((pathology) => {
            if(pathology.id == action.id){
              return Object.assign({}, pathology, {
                name: action.text,
                status: pathology.originalStatus,
                isEditing:false
              })
            }
            return pathology
          }),
        })

      case pathologiesActions.ADD_NEW_PATHOLOGY:
         let newPathology = {
          id: Math.random(),
          name: "",
          status: 1, 
          isNew: true
        }
        return Object.assign({}, state, {
          pathologies: [...state.pathologies,newPathology],
        })

      case pathologiesActions.DELETE_PATHOLOGY:
        if(action.cancelNew)
          return Object.assign({}, state, {
            pathologies: state.pathologies.filter(pathology => pathology.id !== action.id)
          })
        else
          return Object.assign({}, state, {
            pathologies: state.pathologies.map((pathology) => {
              if(pathology.id == action.id){
                return Object.assign({}, pathology, {
                  status: 2
                  })
              }
              return pathology
            }),
          })
      
      case pathologiesActions.FETCH_REQUEST_PATHOLOGIES_SUCCEEDED:
        return Object.assign({}, state, {
          pathologies: action.pathologies,
        })
        
      default:
        return state
    }
  }
  
  export default pathologiesReducer