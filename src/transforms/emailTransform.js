
import moment from "moment";
var transform

const format = (data) => {
    let message = {}
    data.map((m) => {
        message = Object.assign({},message,{
            "id": m.id?m.id:message.id,
            "date": m.name=='Date'?m.value:message.date,
            "to": m.name=='To'?m.value:message.to,
            "subject": m.name=='Subject'?m.value:message.subject,
            "from": m.name=='From'?m.value:message.from,
        })
    });
    return message
};

export default transform = (data) => {

    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};