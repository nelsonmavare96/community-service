
import moment from "moment";
var transform

function exist(field){
    if(field){
        return field;
    }
    return "";
}

const format = (data) => {
    return {
        "id": exist(data.Id),
        "email": exist(data.Email),
        "status": exist(data.Status),
        "social_reason": exist(data.SocialReason),
        "name": exist(data.Name),
        "telephone": exist(data.Phone),
        "user_type":data.UserType,
        "classification":data.clasification&&{
            "id": exist(data.clasification.Id),
            "name" :exist(data.clasification.Name),
            "description": exist(data.clasification.description),
            "userType": exist(data.clasification.UserType),
            "sector": exist(data.clasification.sector)
        },
        "country": {"id":data.person?exist(data.person.Country):""},
        "lastname": data.person?exist(data.person.Lastname):"",
        "instruction_degree":data.person?exist(data.person.InstructionDegree):"",
        "job_type":data.person?exist(data.person.JobType):"",
        "job":data.person?exist(data.person.JobName):"",
        "sex":data.person?exist(data.person.Sex):"",
        "current_study":data.person?exist(data.person.CurrentStudy):"",
        "birthdate": data.person?exist(data.person.Birthdate):"",
        "pathologies":(data.person && data.person.pathologies && data.person.pathologies.length>0)?
                data.person.pathologies.map(pathology=>{
                    return {
                        "id":pathology.Id?pathology.Id:Math.random(),
                        "name": exist(pathology.Name),
                        "description": exist(pathology.description),
                        "value": pathology.Id?pathology.Id:Math.random(),
                        "label": exist(pathology.Name),
                        "status" : exist(pathology.status)
                    }
                })
            :
                [],

        "website":data.organization?exist(data.organization.WebSite):"",
        "facebook":data.organization? exist(data.organization.Facebook):"",
        "twitter": data.organization?exist(data.organization.Twitter):"",
        "instagram": data.organization?exist(data.organization.Instagram):"",
        "address":data.organization?exist(data.organization.Address):"",
    };
};

export default transform = (data) => {

    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};