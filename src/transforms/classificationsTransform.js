
import moment from "moment";
var transform

const format = (data) => {
    return {
        "id": data.Id?data.Id:Math.random(),
        "name": data.Name,
        "description": data.Description,
        "sector": data.Sector?data.Sector:"-",
        "userType": data.UserType?data.UserType:"-",
        "status": data.Status
      };
};

export default transform = (data) => {

    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};